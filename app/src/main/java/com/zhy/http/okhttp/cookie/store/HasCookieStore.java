package com.zhy.http.okhttp.cookie.store;

import android.support.annotation.NonNull;

/**
 * Created by zhy on 16/3/10.
 */
public interface HasCookieStore
{
    @NonNull
    CookieStore getCookieStore();
}
