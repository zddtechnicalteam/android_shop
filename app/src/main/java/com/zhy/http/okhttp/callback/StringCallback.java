package com.zhy.http.okhttp.callback;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by zhy on 15/12/14.
 */
public abstract class StringCallback extends Callback<String>
{
    @Override
    public String parseNetworkResponse(@NonNull Response response, int id) throws IOException
    {
        return response.body().string();
    }
}
