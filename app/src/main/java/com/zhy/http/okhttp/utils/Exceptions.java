package com.zhy.http.okhttp.utils;

import android.support.annotation.NonNull;

/**
 * Created by zhy on 15/12/14.
 */
public class Exceptions
{
    public static void illegalArgument(@NonNull String msg, Object... params)
    {
        throw new IllegalArgumentException(String.format(msg, params));
    }
}
