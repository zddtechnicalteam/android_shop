package com.zhy.http.okhttp.callback;

import android.support.annotation.NonNull;

/**
 * Created by JimGong on 2016/6/23.
 */
public interface IGenericsSerializator {
    @NonNull
    <T> T transform(String response, Class<T> classOfT);
}
