package com.zhaodiandao.shopkeeper.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.zhaodiandao.shopkeeper.R;

/**
 * 左边有固定文字EditText
 */
public class FixedEditText extends AppCompatEditText {
    private View.OnClickListener mListener;
    private int originLeft;
    @Nullable
    private String mFixedText;
    private float mTextPadding;
    private int mTextColor;
    private int mTextColorDefault;
    private int mTextColorFocus;

    public FixedEditText(@NonNull Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.FixedEditText,
                0, 0);

        try {
            mFixedText = a.getString(R.styleable.FixedEditText_fixedText);
            mTextColorDefault = a.getColor(R.styleable.FixedEditText_fixedTextColor, getResources().getColor(R.color.color_edit_bg));
            mTextColorFocus = getResources().getColor(R.color.colorPrimary);
            mTextColor = mTextColorDefault;
            mTextPadding = a.getDimension(R.styleable.FixedEditText_fixedTextPadding, 3);
            originLeft = getPaddingLeft();
            int left = (int) getPaint().measureText(mFixedText)+ getPaddingLeft() + (int)mTextPadding;
            setPadding(left, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused) {
            mTextColor = mTextColorFocus;
        } else {
            mTextColor = mTextColorDefault;
        }
        invalidate();
    }

    public void setDrawableClk(View.OnClickListener listener) {
        mListener = listener;
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);
        if (!TextUtils.isEmpty(mFixedText)) {
            Paint paint = getPaint();
            paint.setColor(mTextColor);
            canvas.drawText(mFixedText, originLeft, getMeasuredHeight() - getPaddingBottom() - paint.getFontMetrics().descent, getPaint());
        }
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (mListener != null && getCompoundDrawables()[2] != null) {
            switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                int i = getMeasuredWidth() - getCompoundDrawables()[2].getIntrinsicWidth();
                if (event.getX() > i) {
                    mListener.onClick(this);
                    return true;
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
            default:
                break;
            }
        }
        return super.onTouchEvent(event);
    }
}