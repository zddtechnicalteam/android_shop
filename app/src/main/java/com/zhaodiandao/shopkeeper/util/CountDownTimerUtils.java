package com.zhaodiandao.shopkeeper.util;

import android.os.CountDownTimer;
import android.widget.Button;
  
/** 
 * Created by Jackie on 2015/11/30. 
 */  
public class CountDownTimerUtils extends CountDownTimer {  
    private Button mButton;
  
    public CountDownTimerUtils(Button button, long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);  
        this.mButton = button;
    }  
  
    @Override  
    public void onTick(long millisUntilFinished) {  
        mButton.setEnabled(false); //设置不可点击
        mButton.setText(millisUntilFinished / 1000 + "s");  //设置倒计时时间
    }
  
    @Override  
    public void onFinish() {  
        mButton.setText("获取");
        mButton.setEnabled(true);//重新获得点击
    }
}  