package com.zhaodiandao.shopkeeper.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;

import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.view.CustomDialog;


public class DialogHelper {
    @Nullable
    private static CustomDialog customDimDialog;

    public static void showDimDialog(Context context, String text){
        if (customDimDialog == null){
            customDimDialog = new CustomDialog(context, R.style.CustomDialogDim, text);
        } else {
            customDimDialog.setText(text);
        }
        customDimDialog.setCancelable(false);
        customDimDialog.setCanceledOnTouchOutside(false);
        customDimDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                customDimDialog = null;
            }
        });
        customDimDialog.show();
    }

    public static void dismissDialog(){
        if (customDimDialog != null) {
            customDimDialog.dismiss();
            customDimDialog = null;
        }
    }
}
