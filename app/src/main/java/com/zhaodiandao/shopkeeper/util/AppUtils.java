package com.zhaodiandao.shopkeeper.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.zhaodiandao.shopkeeper.R;

/**
 * Created by yanix on 2016/12/6.
 * 应用版本信息工具类
 */
public class AppUtils {
    @Nullable
    private static AppUtils instance = null;

    private AppUtils() {}

    @Nullable
    public static AppUtils getInstance() {
        if (instance == null)
            instance = new AppUtils();
        return instance;
    }

    /**
     * 获取版本名
     */
    public String getVersion(@NonNull Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return context.getString(R.string.error_unknown_version);
        }
    }

    /**
     * 获取版本号
     */
    @NonNull
    public String getVersionCode(@NonNull Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String versionCode = info.versionCode + "";
            return versionCode ;
        } catch (Exception e) {
            e.printStackTrace();
            return context.getString(R.string.error_unknown_version_code);
        }
    }
}
