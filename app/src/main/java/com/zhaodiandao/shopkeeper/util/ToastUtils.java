package com.zhaodiandao.shopkeeper.util;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

/**
 * Created by yanix on 2016/11/22.
 * Toast工具类 
 */
public class ToastUtils {  
    @NonNull
    private static Handler handler = new Handler(Looper.getMainLooper());
    @Nullable
    private static Toast toast = null;
    @NonNull
    private static Object synObj = new Object();
   
    public static void showMessage(final Context act, final String msg) {
        showMessage(act, msg, Toast.LENGTH_SHORT);  
    }  
       
    public static void showMessage(final Context act, final int msg) {
        showMessage(act, msg, Toast.LENGTH_SHORT);  
    }  
       
    public static void showMessage(final Context act, final int msg,
            final int len) {  
        new Thread(new Runnable() {  
            public void run() {  
                handler.post(new Runnable() {  
   
                    @Override 
                    public void run() {  
                        synchronized (synObj) {  
                            if (toast != null) {  
                                toast.setText(msg);
                                toast.setDuration(len);
                            } else {  
                                toast = Toast.makeText(act, msg, len);
                            }
                            toast.show();  
                        }  
                    }  
                });  
            }  
        }).start();  
    }  
       
    public static void showMessage(final Context act, final String msg,
            final int len) {  
        new Thread(new Runnable() {  
            public void run() {  
                handler.post(new Runnable() {  
   
                    @Override 
                    public void run() {  
                        synchronized (synObj) {  
                            if (toast != null) {  
                                toast.setText(msg);
                                toast.setDuration(len);
                            } else {  
                                toast = Toast.makeText(act, msg, len);
                            }
                            toast.show();  
                        }  
                    }  
                });  
            }  
        }).start();  
    }  
}