/*
 * Copyright (c) 2017.
 * 早点到餐饮管理有限公司 版权所有
 */

package com.zhaodiandao.shopkeeper.model;

import java.io.Serializable;

/**
 * Created by yanix on 2017/8/22.
 */

public class AbsentProductInfo implements Serializable {
    private String sop_id;
    private String pname;
    private int amount;
    private int number;

    public String getSop_id() {
        return sop_id;
    }

    public void setSop_id(String sop_id) {
        this.sop_id = sop_id;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
