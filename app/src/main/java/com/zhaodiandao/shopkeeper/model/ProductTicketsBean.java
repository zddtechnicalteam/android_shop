package com.zhaodiandao.shopkeeper.model;

import java.util.List;

/**
 * Created by yanix on 2017/3/27.
 */
public class ProductTicketsBean {
    private Pickfoodticket pickfoodtickets;
    private List<ProductClassBean> sellproducts;
    private List<StkTicket> stktickets;

    public Pickfoodticket getPickfoodtickets() {
        return pickfoodtickets;
    }

    public void setPickfoodtickets(Pickfoodticket pickfoodtickets) {
        this.pickfoodtickets = pickfoodtickets;
    }

    public List<ProductClassBean> getSellproducts() {
        return sellproducts;
    }

    public void setSellproducts(List<ProductClassBean> sellproducts) {
        this.sellproducts = sellproducts;
    }

    public List<StkTicket> getStktickets() {
        return stktickets;
    }

    public void setStktickets(List<StkTicket> stktickets) {
        this.stktickets = stktickets;
    }

    public static class Pickfoodticket {
        private String header;
        private int stimeleft;
        private List<TicketBean> body;

        public String getHeader() {
            return header;
        }

        public void setHeader(String header) {
            this.header = header;
        }

        public int getStimeleft() {
            return stimeleft;
        }

        public void setStimeleft(int stimeleft) {
            this.stimeleft = stimeleft;
        }

        public List<TicketBean> getBody() {
            return body;
        }

        public void setBody(List<TicketBean> body) {
            this.body = body;
        }
    }

    public static class StkTicket {
        private String name;
        private double org_price;
        private double price;
        private String image;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getOrg_price() {
            return org_price;
        }

        public void setOrg_price(double org_price) {
            this.org_price = org_price;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
