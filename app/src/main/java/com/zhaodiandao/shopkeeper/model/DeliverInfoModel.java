package com.zhaodiandao.shopkeeper.model;

import java.util.List;

/**
 * Created by yanix on 2017/1/12.
 */
public class DeliverInfoModel {
    private String linkman;
    private String mobile;
    private String address;
    private List<DeliverType> deliver;
    private List<String> delivertime;

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<DeliverType> getDeliver() {
        return deliver;
    }

    public void setDeliver(List<DeliverType> deliver) {
        this.deliver = deliver;
    }

    public List<String> getDelivertime() {
        return delivertime;
    }

    public void setDelivertime(List<String> delivertime) {
        this.delivertime = delivertime;
    }

    public static class DeliverType {
        private String id;
        private String type;
        private String remark;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }
}
