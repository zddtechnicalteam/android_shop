package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/1/13.
 */
public class MessageEvent {
    public final int code;
    public final String message;

    public MessageEvent(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
