package com.zhaodiandao.shopkeeper.model;

import java.util.List;

/**
 * Created by yanix on 2017/1/12.
 */
public class OrderDetailModel {
    private String order_id;
    private String ordernumber;
    private int status;  // 5=未支付 10未审核 20=已审核 50=已配送 60=已完成 80=已取消
    private String linkman;
    private String mobile;
    private String address;
    private String delivertime;
    private String vehiclenumber;
    private String drivername;
    private String drivermobile;
    private List<BrandBean> productdata;
    private String productmoney;
    private String delivermoney;
    private String discountmoney;
    private String summoney;
    private String delivertype_id;
    private String delivertype_label;
    private String pft_money;
    private List<TicketItemBean> foodtickets;
    private String stk_money;
    private List<TicketItemBean> stktickets;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDelivertime() {
        return delivertime;
    }

    public void setDelivertime(String delivertime) {
        this.delivertime = delivertime;
    }

    public List<BrandBean> getProductdata() {
        return productdata;
    }

    public void setProductdata(List<BrandBean> productdata) {
        this.productdata = productdata;
    }

    public String getProductmoney() {
        return productmoney;
    }

    public void setProductmoney(String productmoney) {
        this.productmoney = productmoney;
    }

    public String getDelivermoney() {
        return delivermoney;
    }

    public void setDelivermoney(String delivermoney) {
        this.delivermoney = delivermoney;
    }

    public String getDiscountmoney() {
        return discountmoney;
    }

    public void setDiscountmoney(String discountmoney) {
        this.discountmoney = discountmoney;
    }

    public String getSummoney() {
        return summoney;
    }

    public void setSummoney(String summoney) {
        this.summoney = summoney;
    }

    public String getVehiclenumber() {
        return vehiclenumber;
    }

    public void setVehiclenumber(String vehiclenumber) {
        this.vehiclenumber = vehiclenumber;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getDrivermobile() {
        return drivermobile;
    }

    public void setDrivermobile(String drivermobile) {
        this.drivermobile = drivermobile;
    }

    public String getDelivertype_id() {
        return delivertype_id;
    }

    public void setDelivertype_id(String delivertype_id) {
        this.delivertype_id = delivertype_id;
    }

    public String getDelivertype_label() {
        return delivertype_label;
    }

    public void setDelivertype_label(String delivertype_label) {
        this.delivertype_label = delivertype_label;
    }

    public String getStk_money() {
        return stk_money;
    }

    public void setStk_money(String stk_money) {
        this.stk_money = stk_money;
    }

    public List<TicketItemBean> getStktickets() {
        return stktickets;
    }

    public void setStktickets(List<TicketItemBean> stktickets) {
        this.stktickets = stktickets;
    }

    public static class BrandBean {
        private String brandname;
        private String delivermoney;
        private List<ProductBean> products;

        public List<ProductBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductBean> products) {
            this.products = products;
        }

        public String getBrandname() {
            return brandname;
        }

        public void setBrandname(String brandname) {
            this.brandname = brandname;
        }

        public String getDelivermoney() {
            return delivermoney;
        }

        public void setDelivermoney(String delivermoney) {
            this.delivermoney = delivermoney;
        }

        public static class ProductBean {
            private String name;
            private String unit;
            private String amount;
            private String money;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUnit() {
                return unit;
            }

            public void setUnit(String unit) {
                this.unit = unit;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getMoney() {
                return money;
            }

            public void setMoney(String money) {
                this.money = money;
            }
        }
    }

    public String getPft_money() {
        return pft_money;
    }

    public void setPft_money(String pft_money) {
        this.pft_money = pft_money;
    }

    public List<TicketItemBean> getFoodtickets() {
        return foodtickets;
    }

    public void setFoodtickets(List<TicketItemBean> foodtickets) {
        this.foodtickets = foodtickets;
    }
}
