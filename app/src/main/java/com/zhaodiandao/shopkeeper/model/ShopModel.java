package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/1/5.
 * 商户信息
 */
public class ShopModel {
    private String shop_id;
    private String name;
    private String address;

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
