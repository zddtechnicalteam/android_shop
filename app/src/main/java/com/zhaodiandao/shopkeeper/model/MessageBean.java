package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/5/5.
 */

public class MessageBean {
    private String nickName;
    private String content;

    public MessageBean() {
    }

    public MessageBean(String nickName, String content) {
        this.nickName = nickName;
        this.content = content;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
