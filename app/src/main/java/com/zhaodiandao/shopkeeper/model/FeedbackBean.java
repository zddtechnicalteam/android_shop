package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/2/20.
 * 质量反馈实体类
 */
public class FeedbackBean {
    private String fd_id;
    private String back_date;
    private String status_label;
    private int status_id;
    private String ordernumber;
    private String product;

    public String getFd_id() {
        return fd_id;
    }

    public void setFd_id(String fd_id) {
        this.fd_id = fd_id;
    }

    public String getBack_date() {
        return back_date;
    }

    public void setBack_date(String back_date) {
        this.back_date = back_date;
    }

    public String getStatus_label() {
        return status_label;
    }

    public void setStatus_label(String status_label) {
        this.status_label = status_label;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }
}
