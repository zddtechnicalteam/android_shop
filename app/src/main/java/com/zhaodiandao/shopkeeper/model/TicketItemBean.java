package com.zhaodiandao.shopkeeper.model;

import java.io.Serializable;

/**
 * Created by yanix on 2017/3/29.
 */
public class TicketItemBean implements Serializable {
    private String name;
    private String amount;
    private String money;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
