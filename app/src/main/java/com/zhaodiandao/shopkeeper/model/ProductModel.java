package com.zhaodiandao.shopkeeper.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yanix on 2017/2/17.
 * 投诉产品实体
 */
public class ProductModel implements Serializable{
    private List<ProductBean> products;

    public List<ProductBean> getProducts() {
        return products;
    }

    public void setProducts(List<ProductBean> products) {
        this.products = products;
    }

    public static class ProductBean implements Serializable{
        private int product_id;
        private String product_name;

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

    }
}
