package com.zhaodiandao.shopkeeper.model;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by yanix on 2017/1/12.
 */
public class PayModel implements Serializable{
    private String order_id;
    private String ordernumber;
    private Double money;
    private Double amount;
    private Double remoney;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    @NonNull
    public Double getMoney() {
        return money == null ? 0.0 : money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    @NonNull
    public Double getAmount() {
        return amount == null ? 0.0 : amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @NonNull
    public Double getRemoney() {
        return remoney == null ? 0.0 : remoney;
    }

    public void setRemoney(Double remoney) {
        this.remoney = remoney;
    }
}
