package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/3/28.
 * 我的提货券
 */
public class MyTicket {
    private String pft_id;
    private String pft_name;
    private String amount;
    private String use_amount;
    private String rest_amount;

    public String getPft_id() {
        return pft_id;
    }

    public void setPft_id(String pft_id) {
        this.pft_id = pft_id;
    }

    public String getPft_name() {
        return pft_name;
    }

    public void setPft_name(String pft_name) {
        this.pft_name = pft_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUse_amount() {
        return use_amount;
    }

    public void setUse_amount(String use_amount) {
        this.use_amount = use_amount;
    }

    public String getRest_amount() {
        return rest_amount;
    }

    public void setRest_amount(String rest_amount) {
        this.rest_amount = rest_amount;
    }
}
