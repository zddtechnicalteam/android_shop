package com.zhaodiandao.shopkeeper.model;

import android.support.annotation.NonNull;

/**
 * Created by yanix on 2017/1/12.
 */
public class PayResultModel {
    private String ordernumber;
    private Float pay_money;

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    @NonNull
    public Float getPay_money() {
        return pay_money == null ? 0.0f : pay_money;
    }

    public void setPay_money(Float pay_money) {
        this.pay_money = pay_money;
    }
}
