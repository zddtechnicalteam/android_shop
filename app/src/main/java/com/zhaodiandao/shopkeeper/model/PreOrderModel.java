package com.zhaodiandao.shopkeeper.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yanix on 2017/1/12.
 */
public class PreOrderModel implements Serializable{
    private String linkman;
    private String mobile;
    private String address;
    private String delivertype;
    private String delivertype_label;
    private String delivertime;
    private List<BrandBean> productdata;
    private String delivermoneyexplain;
    private String productmoney;
    private String delivermoney;
    private String discountmoney;
    private String summoney;
    private String remark;
    private String pft_money;
    private List<TicketItemBean> foodticket;
    private String stk_money;
    private List<TicketItemBean> stkticket;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDelivertype() {
        return delivertype;
    }

    public void setDelivertype(String delivertype) {
        this.delivertype = delivertype;
    }

    public String getDelivertype_label() {
        return delivertype_label;
    }

    public void setDelivertype_label(String delivertype_label) {
        this.delivertype_label = delivertype_label;
    }

    public String getDelivertime() {
        return delivertime;
    }

    public void setDelivertime(String delivertime) {
        this.delivertime = delivertime;
    }

    public List<BrandBean> getProductdata() {
        return productdata;
    }

    public void setProductdata(List<BrandBean> productdata) {
        this.productdata = productdata;
    }

    public String getDelivermoneyexplain() {
        return delivermoneyexplain;
    }

    public void setDelivermoneyexplain(String delivermoneyexplain) {
        this.delivermoneyexplain = delivermoneyexplain;
    }

    public String getProductmoney() {
        return productmoney;
    }

    public void setProductmoney(String productmoney) {
        this.productmoney = productmoney;
    }

    public String getDelivermoney() {
        return delivermoney;
    }

    public void setDelivermoney(String delivermoney) {
        this.delivermoney = delivermoney;
    }

    public String getDiscountmoney() {
        return discountmoney;
    }

    public void setDiscountmoney(String discountmoney) {
        this.discountmoney = discountmoney;
    }

    public String getSummoney() {
        return summoney;
    }

    public void setSummoney(String summoney) {
        this.summoney = summoney;
    }

    public static class BrandBean implements Serializable {
        private String brandname;
        private String delivermoney;
        private List<ProductBean> products;

        public List<ProductBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductBean> products) {
            this.products = products;
        }

        public String getBrandname() {
            return brandname;
        }

        public void setBrandname(String brandname) {
            this.brandname = brandname;
        }

        public String getDelivermoney() {
            return delivermoney;
        }

        public void setDelivermoney(String delivermoney) {
            this.delivermoney = delivermoney;
        }

        public static class ProductBean implements Serializable{
            private String name;
            private String unit;
            private String amount;
            private String money;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUnit() {
                return unit;
            }

            public void setUnit(String unit) {
                this.unit = unit;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getMoney() {
                return money;
            }

            public void setMoney(String money) {
                this.money = money;
            }
        }
    }

    public String getPft_money() {
        return pft_money;
    }

    public void setPft_money(String pft_money) {
        this.pft_money = pft_money;
    }

    public List<TicketItemBean> getFoodticket() {
        return foodticket;
    }

    public void setFoodticket(List<TicketItemBean> foodticket) {
        this.foodticket = foodticket;
    }

    public String getStk_money() {
        return stk_money;
    }

    public void setStk_money(String stk_money) {
        this.stk_money = stk_money;
    }

    public List<TicketItemBean> getStkticket() {
        return stkticket;
    }

    public void setStkticket(List<TicketItemBean> stkticket) {
        this.stkticket = stkticket;
    }
}
