package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/1/11.
 * 历史订单实体类
 */
public class OrderListBean {
    private String order_id;
    private String ordernumber;
    private int status;     // 5=未支付 10=未审核 20=已审核 50=已配送 60=已完成 80=已取消
    private String delivertime;
    private String money;
    private String delivertype_id;
    private String delivertype_label;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDelivertime() {
        return delivertime;
    }

    public void setDelivertime(String delivertime) {
        this.delivertime = delivertime;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getDelivertype_id() {
        return delivertype_id;
    }

    public void setDelivertype_id(String delivertype_id) {
        this.delivertype_id = delivertype_id;
    }

    public String getDelivertype_label() {
        return delivertype_label;
    }

    public void setDelivertype_label(String delivertype_label) {
        this.delivertype_label = delivertype_label;
    }
}
