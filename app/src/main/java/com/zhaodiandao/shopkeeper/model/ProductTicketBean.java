package com.zhaodiandao.shopkeeper.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yanix on 2017/1/9.
 * 产品类别
 */
public class ProductTicketBean implements Serializable{
    private String saledate;
    private String menutype;
    private String daysleft;
    private String stimeleft;
    private List<ProductBean> products;

    public String getDaysleft() {
        return daysleft;
    }

    public void setDaysleft(String daysleft) {
        this.daysleft = daysleft;
    }

    public String getStimeleft() {
        return stimeleft;
    }

    public void setStimeleft(String stimeleft) {
        this.stimeleft = stimeleft;
    }

    public String getSaledate() {
        return saledate;
    }

    public void setSaledate(String saledate) {
        this.saledate = saledate;
    }

    public String getMenutype() {
        return menutype;
    }

    public void setMenutype(String menutype) {
        this.menutype = menutype;
    }

    public List<ProductBean> getProducts() {
        return products;
    }

    public void setProducts(List<ProductBean> products) {
        this.products = products;
    }

    public static class ProductBean implements Serializable{
        private String pid;     // 商品ID
        private String name;    // 商品名
        private float price;   // 单价
        private String format;  // 规格
        private String unit;     // 单位
        private String image;           // 产品图片
        private String detail;          // 产品描述
        private int status_id;         // 1-可以抢购  其他值-不能抢购
        private float org_price;   // 原价
        private int sold_percentage;     // 已售比例
        private String status_label;
        private int number;
        private int position;

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public int getStatus_id() {
            return status_id;
        }

        public void setStatus_id(int status_id) {
            this.status_id = status_id;
        }

        public String getStatus_label() {
            return status_label;
        }

        public void setStatus_label(String status_label) {
            this.status_label = status_label;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public float getOrg_price() {
            return org_price;
        }

        public void setOrg_price(float org_price) {
            this.org_price = org_price;
        }

        public int getSold_percentage() {
            return sold_percentage;
        }

        public void setSold_percentage(int sold_percentage) {
            this.sold_percentage = sold_percentage;
        }
    }
}
