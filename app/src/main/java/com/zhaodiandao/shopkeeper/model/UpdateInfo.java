package com.zhaodiandao.shopkeeper.model;

public class UpdateInfo {
    private String update;      // 是否更新
    private String update_mark; // 更新说明
    private String update_is_mandatory;     // 是否强制更新
    private String downloadUrl;     // app下载地址

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getUpdate_mark() {
        return update_mark;
    }

    public void setUpdate_mark(String update_mark) {
        this.update_mark = update_mark;
    }

    public String getUpdate_is_mandatory() {
        return update_is_mandatory;
    }

    public void setUpdate_is_mandatory(String update_is_mandatory) {
        this.update_is_mandatory = update_is_mandatory;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
}
