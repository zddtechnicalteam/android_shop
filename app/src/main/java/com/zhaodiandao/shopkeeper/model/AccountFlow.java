package com.zhaodiandao.shopkeeper.model;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by yanix on 2017/1/11.
 * 账户流水
 */
public class AccountFlow {
    private String balance;
    private String remoney;
    private List<AccountBean> accounts;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getRemoney() {
        return remoney;
    }

    public void setRemoney(String remoney) {
        this.remoney = remoney;
    }

    public List<AccountBean> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountBean> accounts) {
        this.accounts = accounts;
    }

    public static class AccountBean {
        private int type;   //1=收入 2=支出 3=退款 4=提现 41=申请提现 42=取消提现
        private String mark;
        private Double amount;
        private String created;
        private String balance;

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getMark() {
            return mark;
        }

        public void setMark(String mark) {
            this.mark = mark;
        }

        @NonNull
        public Double getAmount() {
            return amount == null ? 0.0 : amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }
    }
}
