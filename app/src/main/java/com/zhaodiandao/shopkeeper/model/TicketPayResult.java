package com.zhaodiandao.shopkeeper.model;

import android.support.annotation.NonNull;

/**
 * Created by yanix on 2017/3/28.
 */
public class TicketPayResult {
    private String ordernumber;
    private Float pay_money;

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    @NonNull
    public Float getPay_money() {
        return pay_money == null ? 0 : pay_money;
    }

    public void setPay_money(Float pay_money) {
        this.pay_money = pay_money;
    }
}
