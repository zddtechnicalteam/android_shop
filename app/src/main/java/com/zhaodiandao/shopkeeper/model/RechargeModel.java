package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/1/11.
 * 充值实体类
 */
public class RechargeModel {
    private String ordernumber;
    private String money;
    private String mark;

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
