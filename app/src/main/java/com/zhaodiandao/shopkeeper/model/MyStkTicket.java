/*
 * Copyright (c) 2017.
 * 早点到餐饮管理有限公司 版权所有
 */

package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/3/28.
 * 我的提货券
 */
public class MyStkTicket {
    private String stk_id;
    private String stk_name;
    private String amount;
    private String use_amount;
    private String rest_amount;

    public String getStk_id() {
        return stk_id;
    }

    public void setStk_id(String stk_id) {
        this.stk_id = stk_id;
    }

    public String getStk_name() {
        return stk_name;
    }

    public void setStk_name(String stk_name) {
        this.stk_name = stk_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUse_amount() {
        return use_amount;
    }

    public void setUse_amount(String use_amount) {
        this.use_amount = use_amount;
    }

    public String getRest_amount() {
        return rest_amount;
    }

    public void setRest_amount(String rest_amount) {
        this.rest_amount = rest_amount;
    }
}
