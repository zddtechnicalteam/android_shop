package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/5/3.
 * 视频直播
 */
public class LiveBean {
    private String title;
    private String live_addr;
    private String live_id;
    private String room_id;
    private String status;  // 2=直播中 3=直播结束

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLive_addr() {
        return live_addr;
    }

    public void setLive_addr(String live_addr) {
        this.live_addr = live_addr;
    }

    public String getLive_id() {
        return live_id;
    }

    public void setLive_id(String live_id) {
        this.live_id = live_id;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
