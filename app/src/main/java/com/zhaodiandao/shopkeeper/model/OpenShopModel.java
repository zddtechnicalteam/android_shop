package com.zhaodiandao.shopkeeper.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yanix on 2017/1/5.
 * 门店申请状态
 */
public class OpenShopModel implements Serializable {
    private String shopapply_id;
    private String mobile;
    private String name;
    private String linkman;
    private String address;
    private String city_id;
    private String city_name;
    private String brand_id;
    private String brand_name;
    private String applytype;
    private String applytype_id;
    private String business_img;
    private String food_img;
    private String facade_img;
    private String IDcard_front;
    private String IDcard_back;
    private String IDcard_body_photo;
    private String IDcard_body_photo_back;
    private String IDcard_bl_body;
    private String bank_card_photo;

    private int status;      // 申请状态：1-提交申请，2-已审核，3-指派业务员，4-开店成功，0-审核失败
    private int shop_id;     // 0-未成功，非0-商户ID
    private List<String> logs;
    private String fail_log;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getBusiness_img() {
        return business_img;
    }

    public void setBusiness_img(String business_img) {
        this.business_img = business_img;
    }

    public String getFood_img() {
        return food_img;
    }

    public void setFood_img(String food_img) {
        this.food_img = food_img;
    }

    public String getFacade_img() {
        return facade_img;
    }

    public void setFacade_img(String facade_img) {
        this.facade_img = facade_img;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<String> getLogs() {
        return logs;
    }

    public void setLogs(List<String> logs) {
        this.logs = logs;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getShopapply_id() {
        return shopapply_id;
    }

    public void setShopapply_id(String shopapply_id) {
        this.shopapply_id = shopapply_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getFail_log() {
        return fail_log;
    }

    public void setFail_log(String fail_log) {
        this.fail_log = fail_log;
    }

    public String getApplytype() {
        return applytype;
    }

    public void setApplytype(String applytype) {
        this.applytype = applytype;
    }

    public String getIDcard_front() {
        return IDcard_front;
    }

    public void setIDcard_front(String IDcard_front) {
        this.IDcard_front = IDcard_front;
    }

    public String getIDcard_back() {
        return IDcard_back;
    }

    public void setIDcard_back(String IDcard_back) {
        this.IDcard_back = IDcard_back;
    }

    public String getIDcard_body_photo() {
        return IDcard_body_photo;
    }

    public void setIDcard_body_photo(String IDcard_body_photo) {
        this.IDcard_body_photo = IDcard_body_photo;
    }

    public String getIDcard_body_photo_back() {
        return IDcard_body_photo_back;
    }

    public void setIDcard_body_photo_back(String IDcard_body_photo_back) {
        this.IDcard_body_photo_back = IDcard_body_photo_back;
    }

    public String getIDcard_bl_body() {
        return IDcard_bl_body;
    }

    public void setIDcard_bl_body(String IDcard_bl_body) {
        this.IDcard_bl_body = IDcard_bl_body;
    }

    public String getBank_card_photo() {
        return bank_card_photo;
    }

    public void setBank_card_photo(String bank_card_photo) {
        this.bank_card_photo = bank_card_photo;
    }

    public String getApplytype_id() {
        return applytype_id;
    }

    public void setApplytype_id(String applytype_id) {
        this.applytype_id = applytype_id;
    }
}
