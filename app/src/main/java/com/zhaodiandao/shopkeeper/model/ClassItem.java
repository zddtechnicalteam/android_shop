package com.zhaodiandao.shopkeeper.model;

public class ClassItem {
    private String className;       // 类别名称
    private String saledate;        // 销售日期
    private int number;             // 该类别下点菜的数量

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSaledate() {
        return saledate;
    }

    public void setSaledate(String saledate) {
        this.saledate = saledate;
    }

    public int getNumber() {
        return number < 0 ? 0 : number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}