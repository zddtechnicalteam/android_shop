package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/1/16.
 */
public class BrandBean {
    private String brand_id;
    private String brand_name;
    private String brand_logo;
    private int status;      // 状态(0=未过审 1=已加盟 2=审核中 4=不可加盟 5=可加盟)
    private String label;

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_logo() {
        return brand_logo;
    }

    public void setBrand_logo(String brand_logo) {
        this.brand_logo = brand_logo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
