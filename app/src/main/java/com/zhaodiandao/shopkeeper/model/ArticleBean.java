package com.zhaodiandao.shopkeeper.model;

/**
 * Created by yanix on 2017/1/9.
 * 文章实体类
 */
public class ArticleBean {
    private String id;
    private String title;
    private String describe;
    private String img_url;
    private String art_url;
    private String click_status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getArt_url() {
        return art_url;
    }

    public void setArt_url(String art_url) {
        this.art_url = art_url;
    }

    public String getClick_status() {
        return click_status;
    }

    public void setClick_status(String click_status) {
        this.click_status = click_status;
    }
}
