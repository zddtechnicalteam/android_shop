/*
 * Copyright (c) 2017.
 * 早点到餐饮管理有限公司 版权所有
 */

package com.zhaodiandao.shopkeeper.model;

import java.util.List;

/**
 * Created by yanix on 2017/5/17.
 */
public class CityModel {
    private List<ApplyTypeBean> applytype;
    private List<CityBean> citybrand;
    private String notes;

    public List<ApplyTypeBean> getApplytype() {
        return applytype;
    }

    public void setApplytype(List<ApplyTypeBean> applytype) {
        this.applytype = applytype;
    }

    public List<CityBean> getCitybrand() {
        return citybrand;
    }

    public void setCitybrand(List<CityBean> citybrand) {
        this.citybrand = citybrand;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public static class ApplyTypeBean {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
