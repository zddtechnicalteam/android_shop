package com.zhaodiandao.shopkeeper.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.AccountFlow;

import java.util.List;

public class AccountListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<AccountFlow.AccountBean> mAccounts;

    public AccountListAdapter(Context context, List<AccountFlow.AccountBean> accounts) {
        mContext = context;
        mAccounts = accounts;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new AccountListAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_account_list, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    @Nullable
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, AccountFlow.AccountBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            AccountListAdapter.EmptyViewViewHolder emptyViewViewHolder = (AccountListAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无订单");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            AccountFlow.AccountBean data = mAccounts.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvMark.setText(data.getMark());
            double amount = data.getAmount();
            if (amount > 0) {
                myViewHolder.tvAmount.setText("+" + amount);
                myViewHolder.tvAmount.setTextColor(Color.parseColor("#DA5623"));
            } else {
                myViewHolder.tvAmount.setText(amount + "");
                myViewHolder.tvAmount.setTextColor(Color.parseColor("#13A800"));
            }
            myViewHolder.tvTime.setText(data.getCreated());
            myViewHolder.tvBalance.setText("余额：" + data.getBalance());
        }
    }

    @Override
    public int getItemCount() {
        return (mAccounts != null && mAccounts.size()>0) ? mAccounts.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mAccounts == null || mAccounts.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(@NonNull View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (AccountFlow.AccountBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvMark, tvAmount, tvTime, tvBalance;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvMark = (TextView) view.findViewById(R.id.tv_mark);
            tvAmount = (TextView) view.findViewById(R.id.tv_amount);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            tvBalance = (TextView) view.findViewById(R.id.tv_balance);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}