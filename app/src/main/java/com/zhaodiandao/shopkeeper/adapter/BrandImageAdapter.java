package com.zhaodiandao.shopkeeper.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.BrandDetailModel;
import com.zhaodiandao.shopkeeper.util.BitmapUtils;

import java.util.List;

public class BrandImageAdapter extends RecyclerView.Adapter<BrandImageAdapter.MyViewHolder> implements View.OnClickListener {
    private Context mContext;
    private List<BrandDetailModel.ImageBean> mImages;

    public BrandImageAdapter(Context context, List<BrandDetailModel.ImageBean> images) {
        mContext = context;
        mImages = images;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_shop_image, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        view.setOnClickListener(this);
        return holder;
    }

    @Nullable
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    //define interface
    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, BrandDetailModel.ImageBean data);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        BrandDetailModel.ImageBean data = mImages.get(position);
        data.setPos(position);
        String imgUrl = data.getImg_url();
        if (!TextUtils.isEmpty(imgUrl)) {
            if (imgUrl.startsWith("/storage")) {
                // 显示本地图片
                Bitmap bitmap = BitmapUtils.createImageThumbnail(mContext, imgUrl);
                holder.ivShop.setImageBitmap(bitmap);
            } else {
                // 显示网络图片
                Picasso.with(mContext).load(imgUrl)
                        .placeholder(R.mipmap.img_default)
                        .config(Bitmap.Config.RGB_565)
                        .into(holder.ivShop);
            }
        } else {
            holder.ivShop.setImageResource(R.mipmap.add_image);
        }
        holder.tvMark.setText(data.getImg_mark());
        holder.itemView.setTag(data);
    }

    @Override
    public int getItemCount() {
        return mImages == null ? 0 : mImages.size();
    }

    @Override
    public void onClick(@NonNull View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (BrandDetailModel.ImageBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivShop;
        TextView tvMark;

        public MyViewHolder(@NonNull View view) {
            super(view);
            ivShop = (ImageView) view.findViewById(R.id.iv_shop);
            tvMark = (TextView) view.findViewById(R.id.tv_mark);
        }
    }
}