package com.zhaodiandao.shopkeeper.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.TicketBean;
import com.zhaodiandao.shopkeeper.util.DensityUtils;

import java.math.BigDecimal;
import java.util.List;

public class TicketBannerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private Context mContext;
    private List<TicketBean> mDatas;

    public TicketBannerAdapter(Context context, List<TicketBean> datas) {
        mContext = context;
        mDatas = datas;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_ticket_banner, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        view.setOnClickListener(this);
        return holder;

    }

    @Nullable
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, TicketBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        TicketBean data = mDatas.get(position);
        myViewHolder.itemView.setTag(data);
        double price = data.getPrice();
        if (price > 999) {
            myViewHolder.tvPrice.setTextSize(18);
        } else if (price > 99) {
            myViewHolder.tvPrice.setTextSize(22);
        } else if (price > 9) {
            myViewHolder.tvPrice.setTextSize(26);
        }
        BigDecimal bigDecimal = new BigDecimal(price);
        myViewHolder.tvPrice.setText(bigDecimal.setScale(1, BigDecimal.ROUND_HALF_UP).floatValue() + "");
        myViewHolder.tvName.setText(data.getName());
        myViewHolder.tvDate.setText(data.getStatus_label());

        String imgUrl = data.getImage();
        if (!TextUtils.isEmpty(imgUrl)) {
            Picasso.with(mContext).load(imgUrl)
                    .resize(DensityUtils.dp2px(mContext, 65), DensityUtils.dp2px(mContext, 65)).centerCrop().config(Bitmap.Config.RGB_565).placeholder(R.mipmap.ticket_img).into(myViewHolder.ivTicket);
        } else {
            myViewHolder.ivTicket.setImageResource(R.mipmap.ticket_img);
        }
    }

    @Override
    public int getItemCount() {
        return mDatas != null ? mDatas.size() : 0;
    }

    @Override
    public void onClick(@NonNull View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (TicketBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvPrice, tvName, tvDate;
        ImageView ivTicket;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvPrice = (TextView) view.findViewById(R.id.tv_price);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            ivTicket = (ImageView) view.findViewById(R.id.iv_ticket);
        }
    }
}