package com.zhaodiandao.shopkeeper.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.OrderListBean;

import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<OrderListBean> mOrders;

    public OrderListAdapter(Context context, List<OrderListBean> orders) {
        mContext = context;
        mOrders = orders;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new OrderListAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_order_list, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    @Nullable
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, OrderListBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            OrderListAdapter.EmptyViewViewHolder emptyViewViewHolder = (OrderListAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无订单");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            OrderListBean data = mOrders.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvOrderNumber.setText("订单号：" + data.getOrdernumber());
            myViewHolder.tvDeliverType.setText(data.getDelivertype_label());
            if ("1".equals(data.getDelivertype_id())) {
                myViewHolder.line.setVisibility(View.VISIBLE);
                myViewHolder.llDeliverTime.setVisibility(View.VISIBLE);
                myViewHolder.tvDeliverTime.setText(data.getDelivertime());
            } else {
                myViewHolder.line.setVisibility(View.GONE);
                myViewHolder.llDeliverTime.setVisibility(View.GONE);
            }
            myViewHolder.tvMoney.setText("￥" + data.getMoney());
            if (data.getStatus() == 5) {
                myViewHolder.tvOrderStatus.setText("未支付");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#F3A536"));
            } else if (data.getStatus() == 10) {
                myViewHolder.tvOrderStatus.setText("未审核");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (data.getStatus() == 20) {
                myViewHolder.tvOrderStatus.setText("已审核");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (data.getStatus() == 50) {
                myViewHolder.tvOrderStatus.setText("已配送");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (data.getStatus() == 60) {
                myViewHolder.tvOrderStatus.setText("已完成");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (data.getStatus() == 80) {
                myViewHolder.tvOrderStatus.setText("已取消");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#13A800"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return (mOrders != null && mOrders.size()>0) ? mOrders.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mOrders == null || mOrders.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(@NonNull View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (OrderListBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvOrderNumber, tvOrderStatus, tvDeliverTime, tvMoney, tvDeliverType;
        View line;
        LinearLayout llDeliverTime;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvOrderNumber = (TextView) view.findViewById(R.id.tv_order_number);
            tvOrderStatus = (TextView) view.findViewById(R.id.tv_order_status);
            tvDeliverTime = (TextView) view.findViewById(R.id.tv_deliver_time);
            tvMoney = (TextView) view.findViewById(R.id.tv_money);
            line = view.findViewById(R.id.line);
            llDeliverTime = (LinearLayout) view.findViewById(R.id.ll_deliver_time);
            tvDeliverType = (TextView) view.findViewById(R.id.tv_deliver_type);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}