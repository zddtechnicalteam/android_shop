package com.zhaodiandao.shopkeeper.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.BrandBean;
import com.zhaodiandao.shopkeeper.util.DensityUtils;

import java.util.List;

public class BrandAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<BrandBean> mBrands;

    public BrandAdapter(Context context, List<BrandBean> brands) {
        mContext = context;
        mBrands = brands;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new BrandAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_fragment_brand, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    @Nullable
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, BrandBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            BrandAdapter.EmptyViewViewHolder emptyViewViewHolder = (BrandAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无品牌");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            BrandBean data = mBrands.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvTitle.setText(data.getBrand_name());

            int status = data.getStatus();
            myViewHolder.tvStatus.setText(data.getLabel());
            if (status == 0) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (status == 1 || status == 3) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#13A800"));
            } else if (status == 2) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#F3A536"));
            } else if (status == 4) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#999999"));
            } else if (status == 5) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#13A800"));
            }

            String imgUrl = data.getBrand_logo();
            if (!TextUtils.isEmpty(imgUrl)) {
                Picasso.with(mContext)
                        .load(imgUrl)
                        .resize(DensityUtils.dp2px(mContext, 300),DensityUtils.dp2px(mContext, 160))
                        .centerCrop().config(Bitmap.Config.RGB_565)
                        .placeholder(R.drawable.default_src)
                        .into(myViewHolder.image);
            } else {
                myViewHolder.image.setImageResource(R.drawable.default_src);
            }
        }
    }

    @Override
    public int getItemCount() {
        return (mBrands != null && mBrands.size()>0) ? mBrands.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mBrands == null || mBrands.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(@NonNull View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (BrandBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvStatus;
        ImageView image;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            image = (ImageView) view.findViewById(R.id.image);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}