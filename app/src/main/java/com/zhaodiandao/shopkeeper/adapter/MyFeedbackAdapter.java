package com.zhaodiandao.shopkeeper.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.FeedbackBean;

import java.util.List;

public class MyFeedbackAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<FeedbackBean> mDatas;

    public MyFeedbackAdapter(Context context, List<FeedbackBean> datas) {
        mContext = context;
        mDatas = datas;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new MyFeedbackAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_my_feedback, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    @Nullable
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, FeedbackBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            MyFeedbackAdapter.EmptyViewViewHolder emptyViewViewHolder = (MyFeedbackAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无质量反馈");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            FeedbackBean data = mDatas.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvOrderNumber.setText("关联订单：" + data.getOrdernumber());
            myViewHolder.tvProduct.setText(data.getProduct());
            myViewHolder.tvDate.setText(data.getBack_date());

            myViewHolder.tvStatus.setText(data.getStatus_label());
            int status = data.getStatus_id();
            // 1=已申请 2=已受理 3=已处理
            if (status == 1) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#FEC16A"));
            } else if (status == 2) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#E70000"));
            } else if (status == 3) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#009D3E"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return (mDatas != null && mDatas.size()>0) ? mDatas.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas == null || mDatas.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(@NonNull View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (FeedbackBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvOrderNumber, tvStatus, tvProduct, tvDate;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvOrderNumber = (TextView) view.findViewById(R.id.tv_order_number);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvProduct = (TextView) view.findViewById(R.id.tv_product);
            tvDate = (TextView) view.findViewById(R.id.tv_feedback_date);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}