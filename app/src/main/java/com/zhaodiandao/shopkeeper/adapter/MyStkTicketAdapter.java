/*
 * Copyright (c) 2017.
 * 早点到餐饮管理有限公司 版权所有
 */

package com.zhaodiandao.shopkeeper.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.MyStkTicket;

import java.util.List;

public class MyStkTicketAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<MyStkTicket> mDatas;

    public MyStkTicketAdapter(Context context, List<MyStkTicket> datas) {
        mContext = context;
        mDatas = datas;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new MyStkTicketAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_my_ticket, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    @Nullable
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, MyStkTicket data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            MyStkTicketAdapter.EmptyViewViewHolder emptyViewViewHolder = (MyStkTicketAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无数据");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            MyStkTicket data = mDatas.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvName.setText(data.getStk_name());
            myViewHolder.tvBuy.setText(data.getAmount());
            myViewHolder.tvLeft.setText(data.getRest_amount());
            myViewHolder.tvUse.setText(data.getUse_amount());
        }
    }

    @Override
    public int getItemCount() {
        return (mDatas != null && mDatas.size()>0) ? mDatas.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas == null || mDatas.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(@NonNull View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (MyStkTicket) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvBuy, tvLeft, tvUse;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvBuy = (TextView) view.findViewById(R.id.tv_buy_number);
            tvLeft = (TextView) view.findViewById(R.id.tv_left_number);
            tvUse = (TextView) view.findViewById(R.id.tv_use_number);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}