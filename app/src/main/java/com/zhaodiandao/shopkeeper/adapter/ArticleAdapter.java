package com.zhaodiandao.shopkeeper.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.ArticleBean;
import com.zhaodiandao.shopkeeper.util.DensityUtils;

import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<ArticleBean> mArticles;

    public ArticleAdapter(Context context, List<ArticleBean> articles) {
        mContext = context;
        mArticles = articles;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new ArticleAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_article, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    @Nullable
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, ArticleBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            ArticleAdapter.EmptyViewViewHolder emptyViewViewHolder = (ArticleAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无文章");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            ArticleBean data = mArticles.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvTitle.setText(data.getTitle());
            if (TextUtils.isEmpty(data.getDescribe())) {
                myViewHolder.tvDescribe.setVisibility(View.GONE);
            } else {
                myViewHolder.tvDescribe.setVisibility(View.VISIBLE);
                myViewHolder.tvDescribe.setText(data.getDescribe());
            }
            String imgUrl = data.getImg_url();
            if (!TextUtils.isEmpty(imgUrl)) {
                if (!imgUrl.startsWith("http:")) {
                    imgUrl = "http://" + imgUrl;
                }
                Picasso.with(mContext).load(imgUrl).
                resize(DensityUtils.dp2px(mContext, 300),DensityUtils.dp2px(mContext, 180)).centerCrop().config(Bitmap.Config.RGB_565).placeholder(R.drawable.default_src).into(myViewHolder.image);
            } else {
                myViewHolder.image.setImageResource(R.drawable.default_src);
            }
        }
    }

    @Override
    public int getItemCount() {
        return (mArticles != null && mArticles.size()>0) ? mArticles.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mArticles == null || mArticles.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(@NonNull View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (ArticleBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDescribe;
        ImageView image;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            image = (ImageView) view.findViewById(R.id.image);
            tvDescribe = (TextView) view.findViewById(R.id.tv_describe);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}