package com.zhaodiandao.shopkeeper.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.TicketDetailBean;

import java.util.List;

public class TicketUseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<TicketDetailBean> mDatas;

    public TicketUseAdapter(Context context, List<TicketDetailBean> datas) {
        mContext = context;
        mDatas = datas;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new TicketUseAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_ticket_use, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    @Nullable
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, TicketDetailBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            TicketUseAdapter.EmptyViewViewHolder emptyViewViewHolder = (TicketUseAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无数据");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            TicketDetailBean data = mDatas.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvOrderNumber.setText("订单号：" + data.getOrdernumber());
            myViewHolder.tvDate.setText(data.getDatetime());
            myViewHolder.tvNumber.setText(data.getNums() + data.getUnit());
            myViewHolder.tvDesc.setText("消费预购券");
        }
    }

    @Override
    public int getItemCount() {
        return (mDatas != null && mDatas.size()>0) ? mDatas.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas == null || mDatas.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(@NonNull View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (TicketDetailBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvOrderNumber, tvDate, tvNumber, tvDesc;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvOrderNumber = (TextView) view.findViewById(R.id.tv_order_number);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvNumber = (TextView) view.findViewById(R.id.tv_number);
            tvDesc = (TextView) view.findViewById(R.id.tvDesc);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}