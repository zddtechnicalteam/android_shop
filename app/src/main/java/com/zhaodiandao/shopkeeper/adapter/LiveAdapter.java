package com.zhaodiandao.shopkeeper.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.LiveBean;

import java.util.List;

public class LiveAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<LiveBean> mlives;

    public LiveAdapter(Context context, List<LiveBean> lives) {
        mContext = context;
        mlives = lives;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new LiveAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_live, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    @Nullable
    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, LiveBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            LiveAdapter.EmptyViewViewHolder emptyViewViewHolder = (LiveAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无视频");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            LiveBean data = mlives.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvTitle.setText(data.getTitle());
            if ("2".equals(data.getStatus())) {
                myViewHolder.ivStatus.setImageResource(R.mipmap.living);
            } else {
                myViewHolder.ivStatus.setImageResource(R.mipmap.living_end);
            }
        }
    }

    @Override
    public int getItemCount() {
        return (mlives != null && mlives.size()>0) ? mlives.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mlives == null || mlives.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(@NonNull View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (LiveBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        ImageView ivStatus;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            ivStatus = (ImageView) view.findViewById(R.id.iv_status);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}