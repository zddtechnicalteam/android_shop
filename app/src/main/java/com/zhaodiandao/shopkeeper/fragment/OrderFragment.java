package com.zhaodiandao.shopkeeper.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zhaodiandao.shopkeeper.BaseFragment;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.adapter.StkTicketAdapter;
import com.zhaodiandao.shopkeeper.adapter.TicketBannerAdapter;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.ClassItem;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.ProductClassBean;
import com.zhaodiandao.shopkeeper.model.ProductTicketsBean;
import com.zhaodiandao.shopkeeper.model.TicketBean;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.net.ZddStringCallback;
import com.zhaodiandao.shopkeeper.order.DeliverInfoActivity;
import com.zhaodiandao.shopkeeper.order.OrderListActivity;
import com.zhaodiandao.shopkeeper.product.ProductDetailActivity;
import com.zhaodiandao.shopkeeper.ticket.StkTicketActivity;
import com.zhaodiandao.shopkeeper.ticket.TicketActivity;
import com.zhaodiandao.shopkeeper.util.AnimUtils;
import com.zhaodiandao.shopkeeper.util.DensityUtils;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.util.simpleadpter.CommonAdapter;
import com.zhaodiandao.shopkeeper.util.simpleadpter.ViewHolder;
import com.zhaodiandao.shopkeeper.view.CustemConfirmDialog;
import com.zhy.http.okhttp.OkHttpUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by yanix on 2017/1/6.
 * 订货页面
 */
public class OrderFragment extends BaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    @Nullable
    @BindView(R.id.ll_content)
    RelativeLayout llContent;
    @Nullable
    @BindView(R.id.pb_loading)
    View pbloading;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView tvFail;
    @Nullable
    @BindView(R.id.lv_class)
    ListView lvClass;
    @Nullable
    @BindView(R.id.lv_product)
    ListView lvProduct;
    @Nullable
    @BindView(R.id.tv_ok)
    TextView tvOk;
    @Nullable
    @BindView(R.id.tv_sum)
    TextView tvSum;
    @Nullable
    @BindView(R.id.tv_count)
    TextView tvCount;
    @Nullable
    @BindView(R.id.ll_cart)
    LinearLayout llCart;
    @Nullable
    @BindView(R.id.iv_cart)
    ImageView ivCart;
    @Nullable
    @BindView(R.id.tv_reserve_not_pay_number)
    TextView tvReserveNotPayNumber;
    @Nullable
    @BindView(R.id.ll_to_pay)
    LinearLayout llToPay;
    @Nullable
    @BindView(R.id.refresh)
    SwipeRefreshLayout refreshLayout;
    // 辅助购物车显示的view
    @Nullable
    @BindView(R.id.black_view)
    View blackView;
    @Nullable
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.recyclerViewStk)
    RecyclerView mRecyclerViewStk;
    @BindView(R.id.llStkLabel)
    View mLlStkLabel;
    @Nullable
    @BindView(R.id.tv_header)
    TextView tvHeader;
    @Nullable
    @BindView(R.id.tv_count_down)
    TextView tvCountDown;
    @Nullable
    @BindView(R.id.rl_header)
    LinearLayout rlHeader;

    ImageView imageView;

    // 类别、菜品、类别菜品、临时购物车、最终购物车集合
    @NonNull
    public static List<ClassItem> classes = new ArrayList<ClassItem>();
    @NonNull
    public static List<ProductClassBean.ProductBean> productInfos = new ArrayList<>();
    @NonNull
    public static List<ProductClassBean> productClassInfos = new ArrayList<>();
    @NonNull
    public static List<ProductClassBean.ProductBean> cart = new ArrayList<>();
    @NonNull
    public static List<ProductClassBean.ProductBean> realCart = new ArrayList<>();
    public static List<ProductTicketsBean.StkTicket> stkTickets = new ArrayList<>();

    // 类别列表、菜品列表、购物车列表适配器
    @Nullable
    public static CommonAdapter<ClassItem> classAdapter;
    private CommonAdapter<ProductClassBean.ProductBean> productAdapter;
    @Nullable
    public static CommonAdapter<ProductClassBean.ProductBean> cartAdapter;

    @NonNull
    private List<TicketBean> mTickets = new ArrayList<>();
    private TicketBannerAdapter adapter;
    private StkTicketAdapter stkAdapter;

    private String selectedClassName = "";
    private boolean isShowCart = true;
    private PopupWindow popWindow;
    private CustemConfirmDialog dialog;

    @NonNull
    private String requestNotPayCountTag = "ReserveFragment_not_pay_count";

    private NumberReceiver numberReceiver;
    @Nullable
    private AnimUtils animUtils;
    private int second = 0;

    @NonNull
    Handler handler = new Handler();
    @NonNull
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            second--;
            if (second <= 0) {
                tvCountDown.setVisibility(View.GONE);
                tvHeader.setVisibility(View.VISIBLE);
                tvHeader.setText("活动进行中");
                return;
            }
            tvCountDown.setText("还剩" + formatTime(second) + "活动开始");
            handler.postDelayed(this, 1000);
        }
    };

    @Override
    public View getView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_order, null);
        ButterKnife.bind(this, root);
        EventBus.getDefault().register(this);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(this);

        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mRecyclerViewStk.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        tvFail.setOnClickListener(this);
        llToPay.setOnClickListener(this);
        tvOk.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        llCart.setOnClickListener(this);

        blackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
            }
        });

        animUtils = new AnimUtils(getActivity(), ivCart);

        adapter = new TicketBannerAdapter(getActivity(), mTickets);
        mRecyclerView.setAdapter(adapter);

        stkAdapter = new StkTicketAdapter(getActivity(), stkTickets);
        mRecyclerViewStk.setAdapter(stkAdapter);

        adapter.setOnItemClickListener(new TicketBannerAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, TicketBean data) {
                startActivity(new Intent(getActivity(), TicketActivity.class));
            }
        });

        stkAdapter.setOnItemClickListener(new StkTicketAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, ProductTicketsBean.StkTicket data) {
                startActivity(new Intent(getActivity(), StkTicketActivity.class));
            }
        });

        classAdapter = new CommonAdapter<ClassItem>(getActivity(), classes, R.layout.listitem_class) {

            @Override
            public void convert(@NonNull ViewHolder helper, @NonNull final ClassItem item) {
                TextView tvClassName = helper.getView(R.id.tv_class_name);
                tvClassName.setText(item.getClassName());

                TextView tvSaleDate = helper.getView(R.id.tv_sale_date);
                if (TextUtils.isEmpty(item.getSaledate())){
                    tvSaleDate.setVisibility(View.GONE);
                } else {
                    tvSaleDate.setText(item.getSaledate());
                    tvSaleDate.setVisibility(View.VISIBLE);
                }

                View indicator = helper.getView(R.id.indicator);

                if (helper.getPosition() == selectPosition) {
                    indicator.setVisibility(View.VISIBLE);
                    tvClassName.setTextColor(getResources().getColor(R.color.colorPrimary));
                } else {
                    indicator.setVisibility(View.INVISIBLE);
                    tvClassName.setTextColor(getResources().getColor(R.color.text_middle));
                }

                TextView tvCount = helper.getView(R.id.tv_count);
                int totalCount = item.getNumber();
                if (totalCount > 0) {
                    if (totalCount > 999) {
                        tvCount.setTextSize(6);
                    } else if (totalCount > 99) {
                        tvCount.setTextSize(8);
                    } else if (totalCount > 9) {
                        tvCount.setTextSize(10);
                    }
                    tvCount.setText(totalCount + "");
                    tvCount.setVisibility(View.VISIBLE);
                } else {
                    tvCount.setVisibility(View.GONE);
                }
            }
        };
        lvClass.setAdapter(classAdapter);
        lvClass.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedClassName = classes.get(position).getClassName();

                classAdapter.setSelectPosition(position);
                classAdapter.notifyDataSetInvalidated();

                productInfos.clear();
                productInfos.addAll(productClassInfos.get(position).getProducts());
                productAdapter.notifyDataSetChanged();
            }
        });

        productAdapter = new CommonAdapter<ProductClassBean.ProductBean>(getActivity(), productInfos, R.layout.listitem_product) {

            @Override
            public void convert(@NonNull final ViewHolder helper, @NonNull final ProductClassBean.ProductBean item) {
                ImageView ivProduct = helper.getView(R.id.iv_product);
                String path = item.getImage();
                if (!TextUtils.isEmpty(path)) {
                    Picasso.with(getActivity()).load(item.getImage())
                            .resize(DensityUtils.dp2px(getActivity().getApplicationContext(), 70), DensityUtils.dp2px(getActivity().getApplicationContext(), 70))
                            .placeholder(R.mipmap.img_default).into(ivProduct);
                } else {
                    ivProduct.setImageResource(R.mipmap.img_default);
                }

                RelativeLayout rlAdded = helper.getView(R.id.rl_added);
                TextView tvReplenishment = helper.getView(R.id.tv_replenishment);
                TextView tvDayLeft = helper.getView(R.id.tv_day_left);
                final int buyAmount = item.getBuy_amont();

                if (item.getDaysleft() == 0) {
                    tvDayLeft.setVisibility(View.GONE);
                    int status = item.getStatus();
                    if (status == 1) {
                        if (buyAmount != 0) {
                            rlAdded.setVisibility(View.VISIBLE);
                            tvReplenishment.setVisibility(View.GONE);
                        } else {
                            rlAdded.setVisibility(View.GONE);
                            tvReplenishment.setVisibility(View.VISIBLE);
                            tvReplenishment.setText("需使用预购券购买");
                        }
                    } else if (status == 3) {
                        rlAdded.setVisibility(View.GONE);
                        tvReplenishment.setVisibility(View.VISIBLE);
                        tvReplenishment.setText("补货中");
                    }
                } else {
                    rlAdded.setVisibility(View.GONE);
                    tvReplenishment.setVisibility(View.GONE);
                    tvDayLeft.setVisibility(View.VISIBLE);
                    tvDayLeft.setText("还有" + item.getDaysleft() + "天开抢");
                }

                helper.setText(R.id.tv_name, item.getName());
                helper.setText(R.id.tv_format, item.getFormat());
                helper.setText(R.id.tv_price, "￥" + item.getPrice() + "/" + item.getUnit());

                final TextView tvCount = helper.getView(R.id.tv_count);
                final ImageView ivAnimReduce = helper.getView(R.id.iv_anim_reduce);
                final ImageView ivAdd = helper.getView(R.id.iv_add);
                final ImageView ivReduce = helper.getView(R.id.iv_reduce);

                ivReduce.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.setNumber(item.getNumber() - 1);

                        classes.get(item.getPosition()).setNumber(classes.get(item.getPosition()).getNumber() - 1);
                        classAdapter.notifyDataSetChanged();

                        if (item.getNumber() == 0) {
                            ivReduce.setEnabled(false);
                            hide(ivReduce, ivAnimReduce);
                        }

                        Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                        intent.putExtra("info", item);
                        intent.putExtra("type", KeyConstants.REDUCE);
                        getActivity().sendBroadcast(intent);
                    }
                });

                if (item.getNumber() > 0) {
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(item.getNumber() + "");
                    ivReduce.setVisibility(View.VISIBLE);
                } else {
                    tvCount.setVisibility(View.INVISIBLE);
                    ivReduce.setVisibility(View.INVISIBLE);
                }

                ivAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(@NonNull View v) {
                        if (item.getNumber() >= buyAmount && buyAmount != -1) {
                            ToastUtils.showMessage(getActivity().getApplicationContext(), "已达到最大购买量");
                            return;
                        }

                        item.setNumber(item.getNumber() + 1);

                        classes.get(item.getPosition()).setNumber(classes.get(item.getPosition()).getNumber() + 1);
                        classAdapter.notifyDataSetChanged();

                        if (item.getNumber() == 1) {
                            ivAdd.setEnabled(false);
                            show(ivAnimReduce, ivAdd, ivReduce, item);
                        } else {
                            Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                            intent.putExtra("info", item);
                            intent.putExtra("type", KeyConstants.ADD);
                            getActivity().sendBroadcast(intent);
                        }

                        int[] start_location = new int[2];// 一个整型数组，用来存储按钮的在屏幕的X、Y坐标
                        v.getLocationInWindow(start_location);// 这是获取购买按钮的在屏幕的X、Y坐标（这也是动画开始的坐标）
                        imageView = new ImageView(getActivity());
                        imageView.setImageBitmap(animUtils.getAddDrawBitMap());// 设置buyImg的图片
                        animUtils.setAnim(imageView, start_location, null);// 开始执行动画
                    }
                });
            }
        };
        lvProduct.setAdapter(productAdapter);
        lvProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProductClassBean.ProductBean productInfo = productInfos.get(position);
                Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
                intent.putExtra("product", productInfo);
                intent.putExtra("pos", position);
                startActivity(intent);
            }
        });

        registerReceiver();
        getProduct();
        return root;
    }

    @Override
    public void getData() {

    }

    private void setText(@NonNull String number) {
        if ("0".equals(number)) {
            llToPay.setVisibility(View.GONE);
            return;
        }
        llToPay.setVisibility(View.VISIBLE);
        String text = String.format(getResources().getString(R.string.reserve_not_pay_hint), number);
        SpannableString ss = new SpannableString(text);
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 2, number.length() + 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvReserveNotPayNumber.setText(ss);
    }

    @Override
    public void onResume() {
        super.onResume();
        getNotPayCount();
    }

    private void show(@NonNull View v, @NonNull final View clickView, @NonNull final View endView, final ProductClassBean.ProductBean item) {
        int[] startPos = new int[2];
        v.getLocationOnScreen(startPos);

        final int[] endPos = new int[2];
        endView.getLocationOnScreen(endPos);

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setDuration(300);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, endPos[0] - startPos[0], 0, 0);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        RotateAnimation rotateAnimation = new RotateAnimation(0, -720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(300);
        //animationSet.addAnimation(rotateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        v.startAnimation(animationSet);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                endView.setVisibility(View.VISIBLE);

                Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                intent.putExtra("info", item);
                intent.putExtra("type", KeyConstants.ADD);
                getActivity().sendBroadcast(intent);

                if (cartAdapter != null) {
                    cartAdapter.notifyDataSetChanged();
                }

                clickView.setEnabled(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void hide(@NonNull final View v, @NonNull View endView) {
        final int[] startPos = new int[2];
        v.getLocationOnScreen(startPos);

        final int[] endPos = new int[2];
        endView.getLocationOnScreen(endPos);

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setDuration(300);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, endPos[0] - startPos[0], 0, 0);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        RotateAnimation rotateAnimation = new RotateAnimation(0, 720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(300);
        //animationSet.addAnimation(rotateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        v.startAnimation(animationSet);

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setEnabled(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void registerReceiver() {
        // 注册广播接收
        numberReceiver = new NumberReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(KeyConstants.ACTION_CHANGE);
        filter.addAction(KeyConstants.ACTION_CART_CLEAR);
        filter.addAction(KeyConstants.ACTION_REFRESH);
        getActivity().registerReceiver(numberReceiver, filter);
    }

    @Override
    public void onRefresh() {
        getProduct();
        clearCart();
    }

    private static int totalCount = 0;
    private static float sumMoney = 0.0f;

    private class NumberReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, @NonNull Intent intent) {
            String action = intent.getAction();
            if (action.equals(KeyConstants.ACTION_CHANGE)) { // 改变数量
                productAdapter.notifyDataSetChanged();
                ProductClassBean.ProductBean info = (ProductClassBean.ProductBean) intent.getSerializableExtra("info");
                int type = intent.getIntExtra("type", 0);

                boolean flag = true;
                for (ProductClassBean.ProductBean cartInfo : cart) {
                    if (cartInfo.getPid().equals(info.getPid())) {
                        cartInfo.setNumber(info.getNumber());
                        flag = false;
                        break;
                    }
                }

                if (flag) {
                    cart.add(info);
                }

                totalCount = Integer.parseInt(tvCount.getText().toString());
                sumMoney = Float.parseFloat(tvSum.getText().toString());

                if (type == KeyConstants.ADD) {
                    totalCount++;
                    sumMoney += info.getPrice();
                } else {
                    totalCount--;
                    sumMoney -= info.getPrice();
                }

                tvCount.setTextSize(12);
                if (totalCount > 0) {
                    if (totalCount > 999) {
                        tvCount.setTextSize(6);
                    } else if (totalCount > 99) {
                        tvCount.setTextSize(8);
                    } else if (totalCount > 9) {
                        tvCount.setTextSize(10);
                    }
                    tvOk.setEnabled(true);
                    tvCount.setText(totalCount + "");
                    tvCount.setVisibility(View.VISIBLE);
                    BigDecimal bigDecimal = new BigDecimal(sumMoney);
                    tvSum.setText("" + bigDecimal.setScale(KeyConstants.SCALE, BigDecimal.ROUND_HALF_UP).floatValue());
                } else {
                    tvOk.setEnabled(false);
                    tvCount.setText("0");
                    tvCount.setVisibility(View.GONE);
                    tvSum.setText("0");
                }
            } else if (action.equals(KeyConstants.ACTION_CART_CLEAR)) {     // 清空
                tvSum.setText("0");
                tvCount.setText("0");
                tvCount.setVisibility(View.GONE);
                tvOk.setEnabled(false);
                totalCount = 0;
                sumMoney = 0.0f;
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
                cart.clear();

                for (ProductClassBean productClassInfo : productClassInfos) {
                    for (ProductClassBean.ProductBean info : productClassInfo.getProducts()) {
                        info.setNumber(0);
                    }
                }

                for (ClassItem classItem : classes) {
                    classItem.setNumber(0);
                }
                productAdapter.notifyDataSetChanged();
                classAdapter.notifyDataSetChanged();
            } else if (action.equals(KeyConstants.ACTION_REFRESH)) {
                getProduct();
                clearCart();
            }
        }
    }


    /**
     * 获取菜品数据
     */
    private void getProduct() {
        mApiService.getProductList(requestTag, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericCallback<ProductTicketsBean>(getActivity().getApplicationContext(), ProductTicketsBean.class) {

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() == View.GONE) {
                            AnimUtils.crossfade(tvFail, pbloading);
                        }
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() == View.GONE) {
                            AnimUtils.crossfade(tvFail, pbloading);
                        }
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (refreshLayout.isRefreshing()) {
                            refreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void success(@NonNull ProductTicketsBean data) {
                        productClassInfos.clear();
                        productClassInfos.addAll(data.getSellproducts());

                        if (productClassInfos.size() <= 0) {
                            ToastUtils.showMessage(getActivity(), "沒有数据");
                            classes.clear();
                            productInfos.clear();
                            classAdapter.notifyDataSetChanged();
                            productAdapter.notifyDataSetChanged();
                            if (pbloading.getVisibility() == View.VISIBLE || tvFail.getVisibility() == View.VISIBLE) {
                                AnimUtils.visible(llContent);
                            } else {
                                llContent.setVisibility(View.VISIBLE);
                            }
                            AnimUtils.gone(pbloading);
                            AnimUtils.gone(tvFail);
                            return;
                        }

                        second = data.getPickfoodtickets().getStimeleft();
                        handler.removeCallbacks(runnable);
                        if (second == 0) {
                            tvHeader.setVisibility(View.VISIBLE);
                            tvCountDown.setVisibility(View.GONE);
                            tvHeader.setText(data.getPickfoodtickets().getHeader());
                        } else {
                            tvHeader.setVisibility(View.GONE);
                            tvCountDown.setVisibility(View.VISIBLE);
                            tvCountDown.setText("还剩" + formatTime(second) + "活动开始");
                            handler.postDelayed(runnable, 1000);
                        }

                        if (data.getPickfoodtickets().getBody() == null || data.getPickfoodtickets().getBody().size() <= 0) {
                            rlHeader.setVisibility(View.GONE);
                        } else {
                            rlHeader.setVisibility(View.VISIBLE);
                        }

                        mTickets.clear();
                        mTickets.addAll(data.getPickfoodtickets().getBody());
                        adapter.notifyDataSetChanged();

                        stkTickets.clear();
                        stkTickets.addAll(data.getStktickets());
                        if (stkTickets.size() <= 0) {
                            mLlStkLabel.setVisibility(View.GONE);
                        } else {
                            mLlStkLabel.setVisibility(View.VISIBLE);
                            stkAdapter.notifyDataSetChanged();
                        }

                        classes.clear();
                        for (int i = 0; i < productClassInfos.size(); i++) {
                            ProductClassBean productClassInfo = productClassInfos.get(i);
                            ClassItem classItem = new ClassItem();
                            classItem.setNumber(0);
                            classItem.setClassName(productClassInfo.getMenutype());
                            classItem.setSaledate(productClassInfo.getSaledate());
                            classes.add(classItem);

                            for (ProductClassBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                productInfo.setPosition(i);
                            }
                        }

                        int selectedClassIndex = getClassIndex(selectedClassName);
                        classAdapter.setSelectPosition(selectedClassIndex);
                        classAdapter.notifyDataSetChanged();

                        productInfos.clear();
                        productInfos.addAll(productClassInfos.get(selectedClassIndex).getProducts());

                        productAdapter.notifyDataSetChanged();

                        if (pbloading.getVisibility() == View.VISIBLE || tvFail.getVisibility() == View.VISIBLE) {
                            AnimUtils.visible(llContent);
                        } else {
                            llContent.setVisibility(View.VISIBLE);
                        }
                        AnimUtils.gone(pbloading);
                        AnimUtils.gone(tvFail);
                    }
                });
    }

    @NonNull
    private String formatTime(int second) {
        String result = "";
        if (second < 60) {
            result = second + "秒";
        } else if (second < 3600) {
            int minute = second/60;
            int sec = second%60;
            result = minute + "分" + sec + "秒";
        } else {
            int hour = second/3600;
            int minute = (second%3600) / 60;
            int sec = (second%3600) % 60;
            result = hour + "时" + minute + "分" + sec + "秒";
        }
        return result;
    }

    private int getClassIndex(String selectedClassName) {
        for (int i = 0; i < classes.size(); i++) {
            if (classes.get(i).getClassName().equals(selectedClassName)) {
                return i;
            }
        }
        return 0;
    }

    private void getNotPayCount() {
        mApiService.getNotPayOrderCount(requestNotPayCountTag, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new ZddStringCallback(getActivity().getApplicationContext(), "num") {

                    @Override
                    public void success(@NonNull String response) {
                        setText(response);
                    }
                });
    }

    @Override
    public void onStop() {
        super.onStop();
        OkHttpUtils.getInstance().cancelTag(requestNotPayCountTag);
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        // 取消广播
        try {
            getActivity().unregisterReceiver(numberReceiver);
        } catch (Exception e) {

        }
        super.onDestroyView();
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.tv_retry:
                pbloading.setVisibility(View.VISIBLE);
                AnimUtils.gone(tvFail);
                getProduct();
                break;
            case R.id.iv_cart:
            case R.id.ll_cart:
                getCart();
                if (isShowCart) {
                    // 判断购物车是否为空
                    if (isCartEmpty()) {
                        ToastUtils.showMessage(getActivity(), getString(R.string.cart_empty));
                    } else {
                        showPop(ivCart);
                        isShowCart = false;
                    }
                } else {
                    if (popWindow != null && popWindow.isShowing()) {
                        popWindow.dismiss();
                        blackView.setVisibility(View.GONE);
                        isShowCart = true;
                    }
                }
                break;
            case R.id.tv_ok:  // 选好了
                getCart();
                Intent intent = new Intent(getActivity(), DeliverInfoActivity.class);
                intent.putExtra("infos", (Serializable) realCart);
                startActivity(intent);
                break;
            case R.id.ll_to_pay:
                startActivity(new Intent(getActivity(), OrderListActivity.class));
                break;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (!isVisibleToUser) {
            if (popWindow != null && popWindow.isShowing()) {
                popWindow.dismiss();
                blackView.setVisibility(View.GONE);
                isShowCart = true;
            }
        } else {
            if (getActivity() != null)
                getNotPayCount();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    /**
     * 显示购物车的详情
     */
    private void showPop(@NonNull View temp) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.cart_popwindow, null);
        LinearLayout llCart = (LinearLayout) v.findViewById(R.id.ll_cart);
        int[] location = new int[2];
        temp.getLocationOnScreen(location);
        popWindow = new PopupWindow(v, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ListView menuList = (ListView) v.findViewById(R.id.card_food_list);
        cartAdapter = new CommonAdapter<ProductClassBean.ProductBean>(getActivity(), realCart, R.layout.listitem_cart) {

            @Override
            public void convert(@NonNull ViewHolder helper, @NonNull final ProductClassBean.ProductBean item) {
                helper.setText(R.id.tv_format, item.getFormat());
                helper.setText(R.id.tv_price, "￥" + item.getPrice() + "/" + item.getUnit());
                helper.setText(R.id.tv_name, item.getName());

                final TextView tvCount = helper.getView(R.id.tv_count);
                final ImageView ivAnimReduce = helper.getView(R.id.iv_anim_reduce);
                final ImageView ivAdd = helper.getView(R.id.iv_add);

                final ImageView ivReduce = helper.getView(R.id.iv_reduce);
                ivReduce.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (ProductClassBean productClassInfo : productClassInfos) {
                            for (ProductClassBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                if (item.getPid().equals(productInfo.getPid())) {
                                    productInfo.setNumber(productInfo.getNumber() - 1);
                                }
                            }
                        }

                        item.setNumber(item.getNumber() - 1);

                        classes.get(item.getPosition()).setNumber(classes.get(item.getPosition()).getNumber() - 1);
                        classAdapter.notifyDataSetChanged();

                        if (item.getNumber() == 0) {
                            ivReduce.setEnabled(false);
                            hide(ivReduce, ivAnimReduce);
                        }

                        Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                        intent.putExtra("info", item);
                        intent.putExtra("type", KeyConstants.REDUCE);
                        getActivity().sendBroadcast(intent);

                        cartAdapter.notifyDataSetChanged();
                    }
                });

                if (item.getNumber() > 0) {
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(item.getNumber() + "");
                    ivReduce.setVisibility(View.VISIBLE);
                } else {
                    tvCount.setVisibility(View.INVISIBLE);
                    ivReduce.setVisibility(View.INVISIBLE);
                }

                ivAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (ProductClassBean productClassInfo : productClassInfos) {
                            for (ProductClassBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                if (item.getPid().equals(productInfo.getPid())) {
                                    productInfo.setNumber(productInfo.getNumber() + 1);
                                }
                            }
                        }

                        item.setNumber(item.getNumber() + 1);

                        classes.get(item.getPosition()).setNumber(classes.get(item.getPosition()).getNumber() + 1);
                        classAdapter.notifyDataSetChanged();

                        if (item.getNumber() == 1) {
                            ivAdd.setEnabled(false);
                            show(ivAnimReduce, ivAdd, ivReduce, item);
                        } else {
                            Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                            intent.putExtra("info", item);
                            intent.putExtra("type", KeyConstants.ADD);
                            getActivity().sendBroadcast(intent);
                            cartAdapter.notifyDataSetInvalidated();
                        }
                    }
                });
            }
        };
        menuList.setAdapter(cartAdapter);
        popWindow.showAtLocation(temp, Gravity.BOTTOM, 0, DensityUtils.dp2px(getActivity(), 90));
        blackView.setVisibility(View.VISIBLE);

        llCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
            }
        });

        v.findViewById(R.id.tv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog == null) {
                    dialog = new CustemConfirmDialog(getActivity(), R.style.QQStyle, "确定清空购物车吗？"
                            , "清空",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    clearCart();
                                    dialog.dismiss();
                                }
                            });
                }
                dialog.show();
            }
        });
    }

    /**
     * 获取购物车的内容
     */
    public static void getCart() {
        realCart.clear();
        for (ProductClassBean.ProductBean info : cart) {
            if (info.getNumber() > 0) {
                realCart.add(info);
            }
        }
    }

    public static int getCartAmount() {
        return totalCount;
    }

    public static float getCartMoney() {
        return sumMoney;
    }

    /**
     * 判断购物车是否为空
     */
    public static boolean isCartEmpty() {
        return realCart.size() <= 0;
    }

    /**
     * 清空购物车
     */
    private void clearCart() {
        // 清空列表
        Intent intent = new Intent();
        intent.setAction(KeyConstants.ACTION_CART_CLEAR);
        getActivity().sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        classAdapter = null;
        cartAdapter = null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.REFRESH_PAY) {
            getProduct();
            clearCart();
        }
    }
}
