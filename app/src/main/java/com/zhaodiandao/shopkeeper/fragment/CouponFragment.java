package com.zhaodiandao.shopkeeper.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zhaodiandao.shopkeeper.BaseFragment;
import com.zhaodiandao.shopkeeper.R;

/**
 * Created by yanix on 2017/1/10.
 */
public class CouponFragment extends BaseFragment {
    private int status;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        status = getArguments().getInt("status", -1);
        requestTag = "CouponFragment_" + status;
    }

    @Override
    public View getView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_coupon, container, false);
        return root;
    }

    @Override
    public void getData() {

    }
}
