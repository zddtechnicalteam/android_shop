package com.zhaodiandao.shopkeeper.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseFragment;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.ZDDWebViewActivity;
import com.zhaodiandao.shopkeeper.account.AccountListActivity;
import com.zhaodiandao.shopkeeper.brand.MyBrandActivity;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.config.URLConstants;
import com.zhaodiandao.shopkeeper.coupon.CouponActivity;
import com.zhaodiandao.shopkeeper.me.MyBuyTicketActivity;
import com.zhaodiandao.shopkeeper.me.MyFeedbackActivity;
import com.zhaodiandao.shopkeeper.me.SettingActivity;
import com.zhaodiandao.shopkeeper.order.OrderListActivity;
import com.zhaodiandao.shopkeeper.util.SPUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/1/6.
 * 个人中心页面
 */
public class MeFragment extends BaseFragment {
    @Nullable
    @BindView(R.id.tv_shop_name)
    TextView tvShopName;
    @Nullable
    @BindView(R.id.tv_address)
    TextView tvAddress;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View getView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_me, container, false);
        ButterKnife.bind(this, root);
        tvShopName.setText("商户名称：" + SPUtils.get(getActivity(), KeyConstants.SHOP_NAME, ""));
        tvAddress.setText("地址：" + SPUtils.get(getActivity(), KeyConstants.SHOP_ADDRESS, ""));
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_setting, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.setting:
                startActivity(new Intent(getActivity(), SettingActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void getData() {
    }

    @OnClick(R.id.tv_coupon)
    public void toCoupon(View v) {
        startActivity(new Intent(getActivity(), CouponActivity.class));
    }

    @OnClick(R.id.tv_order_list)
    public void toOrderList(View v) {
        startActivity(new Intent(getActivity(), OrderListActivity.class));
    }

    @OnClick(R.id.tv_finance)
    public void toFinance(View v) {
        startActivity(new Intent(getActivity(), AccountListActivity.class));
    }

    @OnClick(R.id.tv_brand)
    public void toMyBrand(View v) {
        startActivity(new Intent(getActivity(), MyBrandActivity.class));
    }

    @OnClick(R.id.tv_ticket)
    public void toTicket(View v) {
        startActivity(new Intent(getActivity(), MyBuyTicketActivity.class));
    }

    @OnClick(R.id.tv_feedback)
    public void toMyFeedback(View v) {
        startActivity(new Intent(getActivity(), MyFeedbackActivity.class));
        // TbsVideo.openVideo(getActivity().getApplicationContext(), "http://zdd-player.ufile.ucloud.com.cn/ChickenOperation.mp4");
    }

    @OnClick(R.id.tv_problem)
    public void toProblem(View v) {
        Intent intent = new Intent(getActivity(), ZDDWebViewActivity.class);
        intent.putExtra(ZDDWebViewActivity.TITLE, "常见问题");
        intent.putExtra(ZDDWebViewActivity.URL, URLConstants.URL_PROBLEM);
        startActivity(intent);
    }

}
