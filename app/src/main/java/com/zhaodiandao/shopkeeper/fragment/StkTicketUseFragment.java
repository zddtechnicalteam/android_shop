/*
 * Copyright (c) 2017.
 * 早点到餐饮管理有限公司 版权所有
 */

package com.zhaodiandao.shopkeeper.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseFragment;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.adapter.TicketUseAdapter;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.TicketDetailBean;
import com.zhaodiandao.shopkeeper.net.GenericArrayCallback;
import com.zhaodiandao.shopkeeper.order.OrderDetailActivity;
import com.zhaodiandao.shopkeeper.util.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2017/1/10.
 */
public class StkTicketUseFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private int status;
    @Nullable
    private String ticketId;
    @Nullable
    @BindView(R.id.pb_loading)
    View mProgressBar;
    @Nullable
    @BindView(R.id.ll_content)
    SwipeRefreshLayout mLayoutContent;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @Nullable
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private TicketUseAdapter mAdapter;
    @NonNull
    private List<TicketDetailBean> datas = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        status = getArguments().getInt("status", -1);
        ticketId = getArguments().getString("ticket_id");
        requestTag = "TicketUseFragment" + status;
    }

    @Override
    public View getView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_ticket, null);
        ButterKnife.bind(this, root);
        EventBus.getDefault().register(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mLayoutContent.setColorSchemeResources(R.color.colorPrimary);
        mLayoutContent.setOnRefreshListener(this);
        mAdapter = new TicketUseAdapter(getActivity().getApplicationContext(), datas);
        mAdapter.setOnItemClickListener(new TicketUseAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, @NonNull TicketDetailBean data) {
                Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
                intent.putExtra(OrderDetailActivity.ORDER_ID, data.getOrder_id());
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        fetchData();
        return root;
    }

    @Override
    public void getData() {

    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    private void fetchData() {
        // 获取数据
        mApiService.getStkTicketDetail(requestTag, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""), ticketId, status + "",
                new GenericArrayCallback<TicketDetailBean>(getActivity().getApplicationContext(), TicketDetailBean.class) {

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(@NonNull List<TicketDetailBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    private void refresh() {
        // 获取数据
        mApiService.getStkTicketDetail(requestTag, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""), ticketId, status + "",
                new GenericArrayCallback<TicketDetailBean>(getActivity().getApplicationContext(), TicketDetailBean.class) {

                    @Override
                    public void success(@NonNull List<TicketDetailBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        fetchData();
    }

    @Override
    public void onRefresh() {
        refresh();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.REFRESH_ORDER)
            refresh();
    }
}
