package com.zhaodiandao.shopkeeper.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseFragment;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.account.AccountListActivity;
import com.zhaodiandao.shopkeeper.adapter.AccountListAdapter;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.AccountFlow;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.util.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2017/1/10.
 */
public class AccountStatusFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{
    private int status;
    @Nullable
    @BindView(R.id.pb_loading)
    View mProgressBar;
    @Nullable
    @BindView(R.id.ll_content)
    SwipeRefreshLayout mLayoutContent;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @Nullable
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private AccountListAdapter mAdapter;
    @NonNull
    private List<AccountFlow.AccountBean> datas = new ArrayList<>();
    private AccountListActivity mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AccountListActivity) activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        status = getArguments().getInt("status", -1);
        requestTag = "AccountStatusFragment" + status;
    }

    @Override
    public View getView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_account_list, container, false);
        ButterKnife.bind(this, root);
        EventBus.getDefault().register(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mLayoutContent.setColorSchemeResources(R.color.colorPrimary);
        mLayoutContent.setOnRefreshListener(this);
        mAdapter = new AccountListAdapter(getActivity().getApplicationContext(), datas);
        mAdapter.setOnItemClickListener(new AccountListAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, AccountFlow.AccountBean data) {

            }
        });
        mRecyclerView.setAdapter(mAdapter);
        if (status == 1)
            fetchData();
        return root;
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void getData() {
        if (status != 0)
            fetchData();
    }

    private void fetchData() {
        // 获取数据
        mApiService.getAccountList(requestTag, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""), status + "",
                new GenericCallback<AccountFlow>(getActivity().getApplicationContext(), AccountFlow.class) {

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(@NonNull AccountFlow data) {
                        datas.clear();
                        datas.addAll(data.getAccounts());
                        mAdapter.notifyDataSetChanged();
                        mLayoutContent.setVisibility(View.VISIBLE);
                        mActivity.setBalance(data.getBalance(), data.getRemoney());
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    private void refresh() {
        // 获取数据
        mApiService.getAccountList(requestTag, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""), status + "",
                new GenericCallback<AccountFlow>(getActivity().getApplicationContext(), AccountFlow.class) {

                    @Override
                    public void success(@NonNull AccountFlow data) {
                        datas.clear();
                        datas.addAll(data.getAccounts());
                        mAdapter.notifyDataSetChanged();
                        mActivity.setBalance(data.getBalance(), data.getRemoney());
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    @Override
    public void onRefresh() {
        refresh();
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (status == 1) {
            if (event.code == EventCode.REFRESH_PAY)
                refresh();
        }
    }
}
