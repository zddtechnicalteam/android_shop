package com.zhaodiandao.shopkeeper.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseFragment;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.ZDDWebViewActivity;
import com.zhaodiandao.shopkeeper.adapter.ArticleAdapter;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.ArticleBean;
import com.zhaodiandao.shopkeeper.net.GenericArrayCallback;
import com.zhaodiandao.shopkeeper.net.ZddStringCallback;
import com.zhaodiandao.shopkeeper.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2017/1/6.
 * 商学院页面
 */
public class ArticleFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    @Nullable
    @BindView(R.id.pb_loading)
    View mProgressBar;
    @Nullable
    @BindView(R.id.ll_content)
    SwipeRefreshLayout mLayoutContent;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @Nullable
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private ArticleAdapter mAdapter;
    @NonNull
    private List<ArticleBean> datas = new ArrayList<>();

    @Override
    public View getView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_article, container, false);
        ButterKnife.bind(this, root);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mLayoutContent.setColorSchemeResources(R.color.colorPrimary);
        mLayoutContent.setOnRefreshListener(this);
        mAdapter = new ArticleAdapter(getActivity().getApplicationContext(), datas);
        mAdapter.setOnItemClickListener(new ArticleAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, @NonNull ArticleBean data) {
                if ("1".equals(data.getClick_status())) {
                    read(data.getId());
                }
                Intent intent = new Intent(getActivity(), ZDDWebViewActivity.class);
                intent.putExtra(ZDDWebViewActivity.TITLE, data.getTitle());
                intent.putExtra(ZDDWebViewActivity.URL, data.getArt_url());
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        fetchData();
        return root;
    }

    private void read(String id){
        mApiService.readArticle(requestTag, id, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new ZddStringCallback(getActivity().getApplicationContext(), "message") {
                    @Override
                    public void success(String response) {

                    }
                });
    }

    private void fetchData() {
        // 获取数据
        mApiService.getArticleList(requestTag, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericArrayCallback<ArticleBean>(getActivity().getApplicationContext(), ArticleBean.class) {

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(@NonNull List<ArticleBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    @Override
    public void getData() {

    }

    public void refresh() {
        // 获取数据
        mApiService.getArticleList(requestTag, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericArrayCallback<ArticleBean>(getActivity().getApplicationContext(), ArticleBean.class) {

                    @Override
                    public void success(@NonNull List<ArticleBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        fetchData();
    }

    @Override
    public void onRefresh() {
        refresh();
    }
}
