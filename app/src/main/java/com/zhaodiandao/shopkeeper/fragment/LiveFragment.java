package com.zhaodiandao.shopkeeper.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hyphenate.EMValueCallBack;
import com.hyphenate.chat.EMChatRoom;
import com.hyphenate.chat.EMClient;
import com.tencent.smtt.sdk.TbsVideo;
import com.zhaodiandao.shopkeeper.BaseFragment;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.adapter.LiveAdapter;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.LiveBean;
import com.zhaodiandao.shopkeeper.net.GenericArrayCallback;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.video.PlayerLiveActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2017/1/6.
 * 商学院页面
 */
public class LiveFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    @Nullable
    @BindView(R.id.pb_loading)
    View mProgressBar;
    @Nullable
    @BindView(R.id.ll_content)
    SwipeRefreshLayout mLayoutContent;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @Nullable
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private LiveAdapter mAdapter;
    @NonNull
    private List<LiveBean> datas = new ArrayList<>();

    @Override
    public View getView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_article, container, false);
        ButterKnife.bind(this, root);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mLayoutContent.setColorSchemeResources(R.color.colorPrimary);
        mLayoutContent.setOnRefreshListener(this);
        mAdapter = new LiveAdapter(getActivity().getApplicationContext(), datas);
        mAdapter.setOnItemClickListener(new LiveAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, @NonNull final LiveBean data) {
                if ("2".equals(data.getStatus())) {
                    DialogHelper.showDimDialog(getActivity(), "正在加入聊天室...");
                    //roomId为聊天室ID
                    EMClient.getInstance().chatroomManager().joinChatRoom(data.getRoom_id(), new EMValueCallBack<EMChatRoom>() {

                        @Override
                        public void onSuccess(final EMChatRoom value) {
                            //加入聊天室成功
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    DialogHelper.dismissDialog();
                                    Intent intent = new Intent(getActivity(), PlayerLiveActivity.class);
                                    intent.putExtra(PlayerLiveActivity.KEY_URL, data.getLive_addr());
                                    intent.putExtra(PlayerLiveActivity.KEY_TITLE, data.getTitle());
                                    intent.putExtra(PlayerLiveActivity.KEY_ROOM_ID, data.getRoom_id());
                                    intent.putExtra(PlayerLiveActivity.KEY_LIVE_ID, data.getLive_id());
                                    startActivity(intent);
                                }
                            });
                        }

                        @Override
                        public void onError(final int error, final String errorMsg) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //加入聊天室失败
                                    DialogHelper.dismissDialog();
                                    ToastUtils.showMessage(getActivity().getApplicationContext(), "加入聊天室失败: " + errorMsg);
                                    Intent intent = new Intent(getActivity(), PlayerLiveActivity.class);
                                    intent.putExtra(PlayerLiveActivity.KEY_URL, data.getLive_addr());
                                    intent.putExtra(PlayerLiveActivity.KEY_TITLE, data.getTitle());
                                    intent.putExtra(PlayerLiveActivity.KEY_ROOM_ID, data.getRoom_id());
                                    intent.putExtra(PlayerLiveActivity.KEY_LIVE_ID, data.getLive_id());
                                    startActivity(intent);
                                }
                            });
                        }
                    });
                } else {
                    TbsVideo.openVideo(getActivity().getApplicationContext(), data.getLive_addr());
                    getActivity().overridePendingTransition(0, 0);
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        return root;
    }

    @Override
    public void getData() {
        // 获取数据
        mApiService.getLiveList(requestTag, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericArrayCallback<LiveBean>(getActivity().getApplicationContext(), LiveBean.class) {

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(@NonNull List<LiveBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    public void refresh() {
        // 获取数据
        mApiService.getLiveList(requestTag, (String) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericArrayCallback<LiveBean>(getActivity().getApplicationContext(), LiveBean.class) {

                    @Override
                    public void success(@NonNull List<LiveBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void onRefresh() {
        refresh();
    }
}
