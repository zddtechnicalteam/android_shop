package com.zhaodiandao.shopkeeper.coupon;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.fragment.CouponFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yangxu on 2017/1/10.
 * 优惠券
 */
public class CouponActivity extends BaseActivity {
    private static final int STATUS_NOT_USE = 1;
    private static final int STATUS_USED = 2;
    private static final int STATUS_INVALID = 3;
    @Nullable
    @BindView(R.id.viewpager)
    ViewPager mPager;
    @Nullable
    @BindView(R.id.cursor)
    View cursor;
    @Nullable
    @BindView(R.id.rb_not_use)
    RadioButton rbNotUse;
    @Nullable
    @BindView(R.id.rb_used)
    RadioButton rbUsed;
    @Nullable
    @BindView(R.id.rb_invalid)
    RadioButton rbInvalid;
    private RelativeLayout.LayoutParams layoutParams;
    private int bmpW;
    private int screenW;
    private int wholeWidth;
    private List<Fragment> fragmentList;
    private static final int TAB_COUNT = 3;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_coupon);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initCursor();
        initTab();
        initViewPager();
    }

    private void initTab() {
        rbNotUse.setOnClickListener(new MyClickListener(0));
        rbUsed.setOnClickListener(new MyClickListener(1));
        rbInvalid.setOnClickListener(new MyClickListener(2));
        rbNotUse.setChecked(true);
    }

    public class MyClickListener implements View.OnClickListener {
        private int index = 0;

        public MyClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            mPager.setCurrentItem(index, true);
        }
    }

    private void initCursor() {
        layoutParams = (RelativeLayout.LayoutParams) cursor.getLayoutParams();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        wholeWidth = dm.widthPixels;
        screenW = dm.widthPixels;
        bmpW = screenW / TAB_COUNT;
        layoutParams.width = bmpW;
        cursor.setLayoutParams(layoutParams);
        cursor.setVisibility(View.VISIBLE);
    }

    private void initViewPager() {
        fragmentList = new ArrayList<>();
        CouponFragment notStartFragment = new CouponFragment();
        Bundle bundleNotUse= new Bundle();
        bundleNotUse.putInt("status", STATUS_NOT_USE);
        notStartFragment.setArguments(bundleNotUse);

        CouponFragment processFragment = new CouponFragment();
        Bundle bundleUsed= new Bundle();
        bundleUsed.putInt("status", STATUS_USED);
        processFragment.setArguments(bundleUsed);

        CouponFragment finishFragment = new CouponFragment();
        Bundle bundleInvalid = new Bundle();
        bundleInvalid.putInt("status", STATUS_INVALID);
        finishFragment.setArguments(bundleInvalid);

        fragmentList.add(notStartFragment);
        fragmentList.add(processFragment);
        fragmentList.add(finishFragment);

        //给ViewPager设置适配器
        mPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager()));
        mPager.setOffscreenPageLimit(TAB_COUNT-1);
        mPager.setCurrentItem(0);//设置当前显示标签页为第一页
        mPager.addOnPageChangeListener(new MyOnPageChangeListener());//页面变化时的监听器
    }

    /**
     * 视图加载完成之后执行：值得注意的是PopupWindow每次显示和销毁都会执行一次
     */
    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            tabMove(position, positionOffsetPixels);
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageSelected(int position) {
            tabChange(position);
        }

        private void tabMove(int position, int positionOffsetPixels) {
            int moveI = (int) (bmpW * position + (((double) positionOffsetPixels / wholeWidth) * bmpW));
            layoutParams.leftMargin = moveI;
            cursor.setLayoutParams(layoutParams);
        }

        private void tabChange(int position) {
            if (position == 0) {
                rbNotUse.setChecked(true);
            } else if (position == 1) {
                rbUsed.setChecked(true);
            } else if (position == 2) {
                rbInvalid.setChecked(true);
            }
        }
    }

    class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
