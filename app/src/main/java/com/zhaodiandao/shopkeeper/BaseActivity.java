package com.zhaodiandao.shopkeeper;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.umeng.analytics.MobclickAgent;
import com.zhaodiandao.shopkeeper.net.ApiService;
import com.zhy.http.okhttp.OkHttpUtils;

/**
 * Created by yanix on 2016/11/21.
 * 基础Activity
 * 封装通用Activity的UI效果和逻辑，简化代码
 */
public abstract class BaseActivity extends AppCompatActivity {
    @Nullable
    protected ApiService mApiService = null;
    protected String requestTag = this.getClass().getSimpleName();

    protected abstract void getView(Bundle savedInstanceState);
    protected void beforeContent(Bundle savedInstanceState) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        beforeContent(savedInstanceState);
        super.onCreate(savedInstanceState);
        mApiService = ApiService.getInstance();
        getView(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0.0f);
        }
    }

    @Override
    protected void onStop() {
        // 取消请求
        OkHttpUtils.getInstance().cancelTag(requestTag);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // 友盟统计信息
        if (!BuildConfig.LOG_DEBUG)
            MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!BuildConfig.LOG_DEBUG)
            MobclickAgent.onPause(this);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // 监听返回键，统一处理返回逻辑
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
