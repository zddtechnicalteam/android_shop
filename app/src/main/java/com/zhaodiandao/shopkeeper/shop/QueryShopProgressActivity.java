package com.zhaodiandao.shopkeeper.shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.OpenShopModel;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.net.ZddStringCallback;
import com.zhaodiandao.shopkeeper.util.CountDownTimerUtils;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.ToastUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Request;

/**
 * Created by yanix on 2017/1/6.
 * 查询开店进度
 */
public class QueryShopProgressActivity extends BaseActivity {
    @Nullable
    @BindView(R.id.et_mobile)
    EditText etMobile;
    @Nullable
    @BindView(R.id.et_msg_code)
    EditText etMsgCode;
    @Nullable
    @BindView(R.id.btn_get_code)
    Button btnGetCode;
    @Nullable
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    private CountDownTimerUtils countDownTimerUtils;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_query_shop_progress);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        countDownTimerUtils = new CountDownTimerUtils(btnGetCode, 60000, 1000);
    }

    @Override
    protected void onDestroy() {
        countDownTimerUtils.cancel();
        super.onDestroy();
    }

    private boolean isMobileNO(@NonNull String mobile) {
        Pattern p = Pattern.compile("^((13[0-9])|(15[0-9])|(18[0-9])|(17[0-9])|(14[0-9]))\\d{8}$");
        Matcher m = p.matcher(mobile);
        return m.find();
    }

    private void getMsgCode(String mobile) {
        mApiService.getMsgCode(requestTag, mobile, new ZddStringCallback(getApplicationContext(), "success") {
            @Override
            public void success(String response) {
                ToastUtils.showMessage(getApplicationContext(), response);
            }
        });
    }

    @OnClick(R.id.btn_get_code)
    public void getCode(View v) {
        String mobile = etMobile.getText().toString();
        if (TextUtils.isEmpty(mobile)) {
            ToastUtils.showMessage(getApplicationContext(), "请填写手机号");
            return;
        } else if (!isMobileNO(mobile)) {
            ToastUtils.showMessage(getApplicationContext(), "手机号格式不正确");
            return;
        }
        getMsgCode(mobile);
        countDownTimerUtils.start();
    }

    @OnClick(R.id.btn_submit)
    public void submit(View v) {
        String mobile = etMobile.getText().toString();
        if (TextUtils.isEmpty(mobile)) {
            ToastUtils.showMessage(getApplicationContext(), "请填写手机号");
            return;
        }
        String msgCode = etMsgCode.getText().toString();
        if (TextUtils.isEmpty(msgCode)) {
            ToastUtils.showMessage(getApplicationContext(), "请输入验证码");
            return;
        }

        queryProgress(mobile, msgCode);
    }

    private void queryProgress(String mobile, String code) {
        mApiService.queryProgress(requestTag, mobile, code, new GenericCallback<OpenShopModel>(getApplicationContext(), OpenShopModel.class) {

            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                DialogHelper.showDimDialog(QueryShopProgressActivity.this, "正在查询...");
            }

            @Override
            public void onAfter(int id) {
                super.onAfter(id);
                DialogHelper.dismissDialog();
            }

            @Override
            public void success(OpenShopModel data) {
                Intent intent = new Intent(QueryShopProgressActivity.this, ShopProgressActivity.class);
                intent.putExtra(ShopProgressActivity.KEY_MODEL, data);
                startActivity(intent);
                finish();
            }
        });
    }
}
