package com.zhaodiandao.shopkeeper.shop;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.squareup.picasso.Picasso;
import com.umeng.analytics.MobclickAgent;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.BigImageActivity;
import com.zhaodiandao.shopkeeper.BuildConfig;
import com.zhaodiandao.shopkeeper.MainActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.config.URLConstants;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.OpenShopModel;
import com.zhaodiandao.shopkeeper.model.ShopModel;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.util.BitmapUtils;
import com.zhaodiandao.shopkeeper.util.DensityUtils;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.PictureUtil;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.util.Util;
import com.zhaodiandao.shopkeeper.view.CustemConfirmDialog;
import com.zhaodiandao.shopkeeper.view.FixedEditText;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zhaodiandao.shopkeeper.account.BankRemarkActivity.REQUEST_CODE_GALLERY;

/**
 * Created by yanix on 2017/1/5.
 * 查询开店进度
 */
public class ShopProgressActivity extends BaseActivity {
    public static final String KEY_MODEL = "key_model";

    @Nullable
    @BindView(R.id.bottomsheet)
    BottomSheetLayout mSheetLayout;
    @Nullable
    @BindView(R.id.tv_reason)
    TextView tvReason;
    @Nullable
    @BindView(R.id.et_mobile)
    FixedEditText etMobile;
    @Nullable
    @BindView(R.id.et_shop_name)
    FixedEditText etShopName;
    @Nullable
    @BindView(R.id.et_shop_linkman)
    FixedEditText etShopLinkman;
    @Nullable
    @BindView(R.id.et_address)
    FixedEditText etAddress;
    @Nullable
    @BindView(R.id.et_city)
    FixedEditText etCity;
    @Nullable
    @BindView(R.id.et_brand)
    FixedEditText etBrand;
    @BindView(R.id.et_applytype)
    FixedEditText etApplyType;
    @Nullable
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    @Nullable
    @BindView(R.id.iv_business_licence)
    ImageView ivBusinessLicence;
    @Nullable
    @BindView(R.id.iv_food_licence)
    ImageView ivFoodLicence;
    @Nullable
    @BindView(R.id.iv_shop_photo)
    ImageView ivShopPhoto;
    @Nullable
    @BindView(R.id.iv_idcard_front)
    ImageView ivIdcardFront;
    @Nullable
    @BindView(R.id.iv_idcard_back)
    ImageView ivIdcardBack;
    @Nullable
    @BindView(R.id.iv_idcard_body_photo)
    ImageView ivIdcardBodyPhoto;
    @Nullable
    @BindView(R.id.iv_idcard_body_photo_back)
    ImageView ivIdcardBodyPhotoBack;
    @Nullable
    @BindView(R.id.iv_idcard_bl_body)
    ImageView ivIdcardBlBody;
    @Nullable
    @BindView(R.id.iv_bank_card_photo)
    ImageView ivBankCardPhoto;

    @Nullable
    @BindView(R.id.iv_step1)
    ImageView ivStep1;
    @Nullable
    @BindView(R.id.iv_step1_line)
    ImageView ivStepLine1;
    @Nullable
    @BindView(R.id.iv_step2)
    ImageView ivStep2;
    @Nullable
    @BindView(R.id.iv_step2_line)
    ImageView ivStepLine2;
    @Nullable
    @BindView(R.id.iv_step3)
    ImageView ivStep3;
    @Nullable
    @BindView(R.id.iv_step3_line)
    ImageView ivStepLine3;
    @Nullable
    @BindView(R.id.iv_step4)
    ImageView ivStep4;

    @Nullable
    @BindView(R.id.tv_time1)
    TextView tvTime1;
    @Nullable
    @BindView(R.id.tv_time2)
    TextView tvTime2;
    @Nullable
    @BindView(R.id.tv_time3)
    TextView tvTime3;
    @Nullable
    @BindView(R.id.tv_time4)
    TextView tvTime4;

    private CustemConfirmDialog dialog;
    private boolean canEdit = false;
    private String businessLicencePath;
    private String foodLicencePath;
    private String shopPhotoPath;
    @Nullable
    private String idcardFrontPath;
    @Nullable
    private String idcardBackPath;
    @Nullable
    private String idcardBodyPhotoPath;
    @Nullable
    private String idcardBodyPhotoBackPath;
    @Nullable
    private String idcardBlBodyPath;
    @Nullable
    private String bankCardPhotoPath;

    private OpenShopModel openShopModel;
    // 指示当前图片放大动画是否打开，防止重复点击
    boolean isOpening;

    @Override
    protected void getView(Bundle savedInstanceState) {
        // 这里的作用是让状态栏从隐藏到显示的过程中,布局不受影响
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        );
        setContentView(R.layout.activity_shop_progress);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        openShopModel = (OpenShopModel) intent.getSerializableExtra(KEY_MODEL);
        etMobile.setText(openShopModel.getMobile());
        etShopName.setText(openShopModel.getName());
        etShopLinkman.setText(openShopModel.getLinkman());
        etAddress.setText(openShopModel.getAddress());
        etCity.setText(openShopModel.getCity_name());
        etBrand.setText(openShopModel.getBrand_name());
        etApplyType.setText(openShopModel.getApplytype());

        if (!TextUtils.isEmpty(openShopModel.getBusiness_img()))
            Picasso.with(this).load(Uri.parse(openShopModel.getBusiness_img())).into(ivBusinessLicence);
        if (!TextUtils.isEmpty(openShopModel.getFood_img()))
            Picasso.with(this).load(Uri.parse(openShopModel.getFood_img())).into(ivFoodLicence);
        if (!TextUtils.isEmpty(openShopModel.getFacade_img()))
            Picasso.with(this).load(Uri.parse(openShopModel.getFacade_img())).into(ivShopPhoto);
        if (!TextUtils.isEmpty(openShopModel.getIDcard_front()))
            Picasso.with(this).load(Uri.parse(openShopModel.getIDcard_front())).into(ivIdcardFront);
        if (!TextUtils.isEmpty(openShopModel.getIDcard_back()))
            Picasso.with(this).load(Uri.parse(openShopModel.getIDcard_back())).into(ivIdcardBack);
        if (!TextUtils.isEmpty(openShopModel.getIDcard_body_photo()))
            Picasso.with(this).load(Uri.parse(openShopModel.getIDcard_body_photo())).into(ivIdcardBodyPhoto);
        if (!TextUtils.isEmpty(openShopModel.getIDcard_body_photo_back()))
            Picasso.with(this).load(Uri.parse(openShopModel.getIDcard_body_photo_back())).into(ivIdcardBodyPhotoBack);
        if (!TextUtils.isEmpty(openShopModel.getIDcard_bl_body()))
            Picasso.with(this).load(Uri.parse(openShopModel.getIDcard_bl_body())).into(ivIdcardBlBody);
        if (!TextUtils.isEmpty(openShopModel.getBank_card_photo()))
            Picasso.with(this).load(Uri.parse(openShopModel.getBank_card_photo())).into(ivBankCardPhoto);


        List<String> logs = openShopModel.getLogs();
        tvTime1.setText(logs.get(0));
        tvTime2.setText(logs.get(1));
        tvTime3.setText(logs.get(2));
        tvTime4.setText(logs.get(3));

        int status = openShopModel.getStatus();
        if (status == 0) {
            ivStep1.getDrawable().setLevel(1);
            ivStepLine1.getDrawable().setLevel(1);
            ivStep2.getDrawable().setLevel(1);
            ivStepLine2.getDrawable().setLevel(1);
            ivStep3.getDrawable().setLevel(1);
            ivStepLine3.getDrawable().setLevel(1);
            ivStep4.getDrawable().setLevel(1);
            tvTime1.setText("审核失败");
            etShopName.setEnabled(true);
            etShopLinkman.setEnabled(true);
            etAddress.setEnabled(true);
            btnSubmit.setVisibility(View.VISIBLE);
            canEdit = true;
            tvReason.setText(openShopModel.getFail_log());
        } else if (status == 1) {
            ivStep1.getDrawable().setLevel(2);
            ivStepLine1.getDrawable().setLevel(1);
            ivStep2.getDrawable().setLevel(1);
            ivStepLine2.getDrawable().setLevel(1);
            ivStep3.getDrawable().setLevel(1);
            ivStepLine3.getDrawable().setLevel(1);
            ivStep4.getDrawable().setLevel(1);
        } else if (status == 2) {
            ivStep1.getDrawable().setLevel(2);
            ivStepLine1.getDrawable().setLevel(2);
            ivStep2.getDrawable().setLevel(2);
            ivStepLine2.getDrawable().setLevel(1);
            ivStep3.getDrawable().setLevel(1);
            ivStepLine3.getDrawable().setLevel(1);
            ivStep4.getDrawable().setLevel(1);
        } else if (status == 3) {
            ivStep1.getDrawable().setLevel(2);
            ivStepLine1.getDrawable().setLevel(2);
            ivStep2.getDrawable().setLevel(2);
            ivStepLine2.getDrawable().setLevel(2);
            ivStep3.getDrawable().setLevel(2);
            ivStepLine3.getDrawable().setLevel(1);
            ivStep4.getDrawable().setLevel(1);
        } else if (status == 4) {
            ivStep1.getDrawable().setLevel(2);
            ivStepLine1.getDrawable().setLevel(2);
            ivStep2.getDrawable().setLevel(2);
            ivStepLine2.getDrawable().setLevel(2);
            ivStep3.getDrawable().setLevel(2);
            ivStepLine3.getDrawable().setLevel(2);
            ivStep4.getDrawable().setLevel(2);
        }

        if (openShopModel.getShop_id() != 0) {
            dialog = new CustemConfirmDialog(ShopProgressActivity.this, R.style.QQStyle, "审核通过，去订货？", "确定",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            login(openShopModel.getShop_id() + "");
                        }
                    });
            dialog.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpening = false;
    }

    public void login(String shopId) {
        mApiService.login(requestTag, shopId, new GenericCallback<ShopModel>(getApplicationContext(), ShopModel.class) {

            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                DialogHelper.showDimDialog(ShopProgressActivity.this, "登录中...");
            }

            @Override
            public void onAfter(int id) {
                DialogHelper.dismissDialog();
                super.onAfter(id);
            }

            @Override
            public void success(@NonNull ShopModel shopModel) {
                // 登录成功，保存业务员ID的值，下次就不用登录了
                SPUtils.put(getApplicationContext(), KeyConstants.SHOP_ID, shopModel.getShop_id());
                SPUtils.put(getApplicationContext(), KeyConstants.SHOP_NAME, shopModel.getName());
                SPUtils.put(getApplicationContext(), KeyConstants.SHOP_ADDRESS, shopModel.getAddress());
                SPUtils.remove(getApplicationContext(), KeyConstants.SHOP_MOBILE);
                if (!BuildConfig.LOG_DEBUG) {
                    // 统计账户信息
                    MobclickAgent.onProfileSignIn(shopModel.getShop_id());
                }

                EMClient.getInstance().login("shop_" + shopModel.getShop_id(), "123456", new EMCallBack() {     //回调
                    @Override
                    public void onSuccess() {
                        // 进入首页
                        startActivity(new Intent(ShopProgressActivity.this, MainActivity.class));
                        EventBus.getDefault().post(new MessageEvent(EventCode.CLOSE, "关闭!"));
                        // 销毁界面
                        finish();
                    }

                    @Override
                    public void onProgress(int progress, String status) {

                    }

                    @Override
                    public void onError(int code, String message) {
                        ToastUtils.showMessage(getApplicationContext(), "登录聊天服务器失败！" + message);
                    }
                });
            }
        });
    }

    /**
     * 图片放大
     */
    private void open(@NonNull View view, String url) {
        if (!isOpening) {
            isOpening = true;
            Intent i = new Intent(getApplicationContext(), BigImageActivity.class);
            i.putExtra(KeyConstants.ORIG_X, Util.getXY(view)[0]);
            i.putExtra(KeyConstants.ORIG_Y, Util.getXY(view)[1]);
            i.putExtra(KeyConstants.ORIG_WIDTH, view.getWidth());
            i.putExtra(KeyConstants.ORIG_HEIGHT, view.getHeight());
            i.putExtra(KeyConstants.IMG_URL, url);

            // Start activity disable animations
            startActivity(i);
            ShopProgressActivity.this.overridePendingTransition(0, 0);
        }
    }

    @OnClick(R.id.iv_business_licence)
    public void addBusinessLicence(final View v) {
        if (!canEdit) {
            if (!TextUtils.isEmpty(openShopModel.getBusiness_img()))
                open(v, openShopModel.getBusiness_img());
        } else {
            MenuSheetView menuSheetView =
                    new MenuSheetView(ShopProgressActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 1);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 1);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    @OnClick(R.id.iv_food_licence)
    public void addFoodLicence(final View v) {
        if (!canEdit) {
            if (!TextUtils.isEmpty(openShopModel.getFood_img()))
                open(v, openShopModel.getFood_img());
        } else {
            MenuSheetView menuSheetView =
                    new MenuSheetView(ShopProgressActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 2);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 2);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    @OnClick(R.id.iv_shop_photo)
    public void addShopPhoto(final View v) {
        if (!canEdit) {
            if (!TextUtils.isEmpty(openShopModel.getFacade_img()))
                open(v, openShopModel.getFacade_img());
        } else {
            MenuSheetView menuSheetView =
                    new MenuSheetView(ShopProgressActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 3);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 3);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    @OnClick(R.id.iv_idcard_front)
    public void addIdcardFront(final View v) {
        if (!canEdit) {
            if (!TextUtils.isEmpty(openShopModel.getIDcard_front()))
                open(v, openShopModel.getIDcard_front());
        } else {
            MenuSheetView menuSheetView =
                    new MenuSheetView(ShopProgressActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 4);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 4);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    @OnClick(R.id.iv_idcard_back)
    public void addIdcardBack(final View v) {
        if (!canEdit) {
            if (!TextUtils.isEmpty(openShopModel.getIDcard_back()))
                open(v, openShopModel.getIDcard_back());
        } else {
            MenuSheetView menuSheetView =
                    new MenuSheetView(ShopProgressActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 5);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 5);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    @OnClick(R.id.iv_idcard_body_photo)
    public void addIdcardBodyPhoto(final View v) {
        if (!canEdit) {
            if (!TextUtils.isEmpty(openShopModel.getIDcard_body_photo()))
                open(v, openShopModel.getIDcard_body_photo());
        } else {
            MenuSheetView menuSheetView =
                    new MenuSheetView(ShopProgressActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 6);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 6);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    @OnClick(R.id.iv_idcard_body_photo_back)
    public void addIdcardBodyPhotoBack(final View v) {
        if (!canEdit) {
            if (!TextUtils.isEmpty(openShopModel.getIDcard_body_photo_back()))
                open(v, openShopModel.getIDcard_body_photo_back());
        } else {
            MenuSheetView menuSheetView =
                    new MenuSheetView(ShopProgressActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 7);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 7);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    @OnClick(R.id.iv_idcard_bl_body)
    public void addIdcardBlBody(final View v) {
        if (!canEdit) {
            if (!TextUtils.isEmpty(openShopModel.getIDcard_bl_body()))
                open(v, openShopModel.getIDcard_bl_body());
        } else {
            MenuSheetView menuSheetView =
                    new MenuSheetView(ShopProgressActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 8);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 8);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    @OnClick(R.id.iv_bank_card_photo)
    public void addBankCardPhoto(final View v) {
        if (!canEdit) {
            if (!TextUtils.isEmpty(openShopModel.getBank_card_photo()))
                open(v, openShopModel.getBank_card_photo());
        } else {
            MenuSheetView menuSheetView =
                    new MenuSheetView(ShopProgressActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 9);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 9);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    /**
     * 从相册选择
     */
    private void takeFromAlbum(@NonNull final ImageView imageView, final int index) {
        //带配置
        FunctionConfig config = new FunctionConfig.Builder()
                .build();

        GalleryFinal.openGallerySingle(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {

            @Override
            public void onHanlderSuccess(int reqeustCode, @NonNull List<PhotoInfo> resultList) {
                // 显示本地图片
                Bitmap bitmap = BitmapUtils.createImageThumbnail(getApplicationContext(), resultList.get(0).getPhotoPath());
                imageView.setImageBitmap(bitmap);
                try {
                    if (index == 1)
                        businessLicencePath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 2)
                        foodLicencePath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 3)
                        shopPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 4)
                        idcardFrontPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 5)
                        idcardBackPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 6)
                        idcardBodyPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 7)
                        idcardBodyPhotoBackPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 8)
                        idcardBlBodyPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 9)
                        bankCardPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    /**
     * 拍照
     */
    private void takePhoto(@NonNull final ImageView imageView, final int index) {
        FunctionConfig config = getFunctionConfig();
        GalleryFinal.openCamera(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, @NonNull List<PhotoInfo> resultList) {
                // 显示本地图片
                Bitmap bitmap = BitmapUtils.createImageThumbnail(getApplicationContext(), resultList.get(0).getPhotoPath());
                imageView.setImageBitmap(bitmap);
                try {
                    if (index == 1)
                        businessLicencePath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 2)
                        foodLicencePath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 3)
                        shopPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 4)
                        idcardFrontPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 5)
                        idcardBackPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 6)
                        idcardBodyPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 7)
                        idcardBodyPhotoBackPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 8)
                        idcardBlBodyPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 9)
                        bankCardPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    private FunctionConfig getFunctionConfig() {
        return new FunctionConfig.Builder()
                .setEnableEdit(true)        //开启编辑功能
                .setEnableCrop(false)       //开启裁剪功能
                .setEnableRotate(false)     //开启旋转功能
                .setEnableCamera(true)      //开启相机功能
                .setCropWidth(DensityUtils.dp2px(getApplicationContext(), 100))     //裁剪宽度
                .setCropHeight(DensityUtils.dp2px(getApplicationContext(), 100))    //裁剪高度
                .setCropSquare(false)       //裁剪正方形
                .setForceCrop(false)        //启动强制裁剪功能,一进入编辑页面就开启图片裁剪，不需要用户手动点击裁剪，此功能只针对单选操作
                .setForceCropEdit(false)    //在开启强制裁剪功能时是否可以对图片进行编辑（也就是是否显示旋转图标和拍照图标）
                .setRotateReplaceSource(false)   //配置选择图片时是否替换原始图片，默认不替换
                .setCropReplaceSource(false)     //配置裁剪图片时是否替换原始图片，默认不替换
                .setEnablePreview(true)     //是否开启预览功能
                .build();
    }

    @OnClick(R.id.btn_submit)
    public void submit(View v) {
        String shopName = etShopName.getText().toString();
        if (TextUtils.isEmpty(shopName)) {
            ToastUtils.showMessage(getApplicationContext(), "请输入店名");
            return;
        }
        String shopLinkman = etShopLinkman.getText().toString();
        if (TextUtils.isEmpty(shopLinkman)) {
            ToastUtils.showMessage(getApplicationContext(), "请输入店主姓名");
            return;
        }
        String address = etAddress.getText().toString();
        if (TextUtils.isEmpty(address)) {
            ToastUtils.showMessage(getApplicationContext(), "请输入地址");
            return;
        }
        DialogHelper.showDimDialog(ShopProgressActivity.this, "正在上传...");

        /* form的分割线,自己定义 */
        String boundary = "xx--------------------------------------------------------------xx";

        MultipartBody.Builder builder = new MultipartBody.Builder(boundary).setType(MultipartBody.FORM)
                /* 上传一个普通的String参数 */
                .addFormDataPart("version", URLConstants.API_VERSION_3)
                .addFormDataPart("shopapply_id", openShopModel.getShopapply_id())
                .addFormDataPart("mobile", openShopModel.getMobile())
                .addFormDataPart("name", shopName)
                .addFormDataPart("linkman", shopLinkman)
                .addFormDataPart("address", address)
                .addFormDataPart("city_id", openShopModel.getCity_id())
                .addFormDataPart("brand_id", openShopModel.getBrand_id())
                .addFormDataPart("applytype", openShopModel.getApplytype_id());

        if (!TextUtils.isEmpty(businessLicencePath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(businessLicencePath));
            builder = builder.addFormDataPart("file[]", "business_img", fileBody);
        }
        if (!TextUtils.isEmpty(foodLicencePath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(foodLicencePath));
            builder = builder.addFormDataPart("file[]", "food_img", fileBody);
        }
        if (!TextUtils.isEmpty(shopPhotoPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(shopPhotoPath));
            builder = builder.addFormDataPart("file[]", "facade_img", fileBody);
        }
        if (!TextUtils.isEmpty(idcardFrontPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardFrontPath));
            builder = builder.addFormDataPart("file[]", "IDcard_front", fileBody);
        }
        if (!TextUtils.isEmpty(idcardBackPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardBackPath));
            builder = builder.addFormDataPart("file[]", "IDcard_back", fileBody);
        }
        if (!TextUtils.isEmpty(idcardBodyPhotoPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardBodyPhotoPath));
            builder = builder.addFormDataPart("file[]", "IDcard_body_photo", fileBody);
        }
        if (!TextUtils.isEmpty(idcardBodyPhotoBackPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardBodyPhotoBackPath));
            builder = builder.addFormDataPart("file[]", "IDcard_body_photo_back", fileBody);
        }
        if (!TextUtils.isEmpty(idcardBlBodyPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardBlBodyPath));
            builder = builder.addFormDataPart("file[]", "IDcard_bl_body", fileBody);
        }
        if (!TextUtils.isEmpty(bankCardPhotoPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(bankCardPhotoPath));
            builder = builder.addFormDataPart("file[]", "bank_card_photo", fileBody);
        }

        MultipartBody mBody = builder.build();

        final Request request = new Request.Builder().url(URLConstants.URL_OPEN_SHOP).post(mBody).build();
        new OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, @NonNull final IOException e) {
                runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      DialogHelper.dismissDialog();
                                      ToastUtils.showMessage(getApplicationContext(), e.getMessage());
                                  }
                              }
                );
            }

            public void onResponse(Call call, @NonNull final Response response) throws IOException {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DialogHelper.dismissDialog();

                        try {
                            String result = response.body().string();
                            if (response.code() != 200) {
                                JSONObject jsonObject = new JSONObject(result);
                                String error = jsonObject.getString("message");
                                ToastUtils.showMessage(getApplicationContext(), error);
                            } else {
                                OpenShopModel openShopModel = JSON.parseObject(result, OpenShopModel.class);
                                PictureUtil.deleteImgTmp(businessLicencePath);
                                PictureUtil.deleteImgTmp(foodLicencePath);
                                PictureUtil.deleteImgTmp(businessLicencePath);
                                // 保存申请的手机号
                                SPUtils.put(getApplicationContext(), KeyConstants.SHOP_MOBILE, openShopModel.getMobile());
                                ToastUtils.showMessage(getApplicationContext(), "信息提交成功");
                                Intent intent = new Intent(ShopProgressActivity.this, ShopProgressActivity.class);
                                intent.putExtra(ShopProgressActivity.KEY_MODEL, openShopModel);
                                startActivity(intent);
                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
