package com.zhaodiandao.shopkeeper.shop;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.igexin.sdk.PushManager;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.BigImageActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.ZDDWebViewActivity;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.config.URLConstants;
import com.zhaodiandao.shopkeeper.model.CityBean;
import com.zhaodiandao.shopkeeper.model.CityModel;
import com.zhaodiandao.shopkeeper.model.OpenShopModel;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.net.ZddStringCallback;
import com.zhaodiandao.shopkeeper.util.BitmapUtils;
import com.zhaodiandao.shopkeeper.util.CountDownTimerUtils;
import com.zhaodiandao.shopkeeper.util.DensityUtils;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.PictureUtil;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.util.Util;
import com.zhaodiandao.shopkeeper.view.FixedEditText;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by yanix on 2017/1/5.
 * 申请开店
 */
public class OpenShopActivity extends BaseActivity {
    private static final int REQUEST_CODE_GALLERY = 100;

    @Nullable
    @BindView(R.id.et_mobile)
    FixedEditText etMobile;
    @Nullable
    @BindView(R.id.et_msg_code)
    FixedEditText etMsgCode;
    @Nullable
    @BindView(R.id.spinner_city)
    Spinner citySpinner;
    @Nullable
    @BindView(R.id.spinner_brand)
    Spinner brandSpinner;
    @Nullable
    @BindView(R.id.spinner_apply_type)
    Spinner applyTypeSpinner;
    @Nullable
    @BindView(R.id.et_shop_name)
    FixedEditText etShopName;
    @Nullable
    @BindView(R.id.et_shop_linkman)
    FixedEditText etShopLinkman;
    @Nullable
    @BindView(R.id.et_salesman_mobile)
    FixedEditText etSalesmanMobile;
    @Nullable
    @BindView(R.id.et_address)
    FixedEditText etAddress;
    @Nullable
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @Nullable
    @BindView(R.id.btn_get_code)
    Button btnGetCode;
    @Nullable
    @BindView(R.id.bottomsheet)
    BottomSheetLayout mSheetLayout;

    @Nullable
    @BindView(R.id.iv_shop_photo)
    ImageView ivShopPhoto;
    @Nullable
    @BindView(R.id.iv_idcard_body_photo)
    ImageView ivIdcardBodyPhoto;
    @Nullable
    @BindView(R.id.iv_door_number_photo)
    ImageView ivDoorNumberPhoto;
    @Nullable
    @BindView(R.id.tv_example)
    TextView tvExample;
    @Nullable
    @BindView(R.id.tv_note)
    TextView tvNote;

    private String cityId;
    private String brandId;
    private String applyTypeId;
    private CountDownTimerUtils countDownTimerUtils;

    @Nullable
    private String doorNumberPhotoPath;
    @Nullable
    private String idcardBodyPhotoPath;
    @Nullable
    private String shopPhotoPath;

    // 指示当前图片放大动画是否打开，防止重复点击
    boolean isOpening;

    @Override
    protected void getView(Bundle savedInstanceState) {
        // 这里的作用是让状态栏从隐藏到显示的过程中,布局不受影响
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        );
        setContentView(R.layout.activity_open_shop);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvExample.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
        tvExample.getPaint().setAntiAlias(true);//抗锯齿

        countDownTimerUtils = new CountDownTimerUtils(btnGetCode, 60000, 1000);
        getCityData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpening = false;
    }

    @Override
    protected void onDestroy() {
        PictureUtil.deleteImgTmp(idcardBodyPhotoPath);
        PictureUtil.deleteImgTmp(doorNumberPhotoPath);
        PictureUtil.deleteImgTmp(shopPhotoPath);
        countDownTimerUtils.cancel();
        super.onDestroy();
    }

    private void getCityData() {
        mApiService.getCityBrandData(requestTag, new GenericCallback<CityModel>(getApplicationContext(), CityModel.class) {
            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                DialogHelper.showDimDialog(OpenShopActivity.this, "努力请求中...");
            }

            @Override
            public void onAfter(int id) {
                super.onAfter(id);
                DialogHelper.dismissDialog();
            }

            @Override
            public void success(@Nullable CityModel data) {
                btnSubmit.setEnabled(true);
                citySpinner.setAdapter(new ArrayAdapter<String>(OpenShopActivity.this, R.layout.spinner_item, R.id.tv_name, getCityNames(data.getCitybrand())));
                if (data.getCitybrand() != null && data.getCitybrand().size() > 0) {
                    cityId = data.getCitybrand().get(0).getId();
                    citySpinner.setSelection(0);
                    // 切换城市
                    citySpinner.setOnItemSelectedListener(new CityAdapter(data.getCitybrand()));

                    List<CityBean.BrandBean> brands = data.getCitybrand().get(0).getBrand();
                    brandSpinner.setAdapter(new ArrayAdapter<String>(OpenShopActivity.this, R.layout.spinner_item, R.id.tv_name, getBrandNames(brands)));
                    if (brands != null && brands.size() > 0) {
                        brandId = brands.get(0).getId();
                        brandSpinner.setSelection(0);
                        // 切换品牌
                        brandSpinner.setOnItemSelectedListener(new BrandAdapter(brands));
                    } else {
                        brandId = "";
                    }
                }

                List<CityModel.ApplyTypeBean> applyTypes = data.getApplytype();
                applyTypeSpinner.setAdapter(new ArrayAdapter<String>(OpenShopActivity.this, R.layout.spinner_item, R.id.tv_name, getApplyTypeNames(applyTypes)));
                if (applyTypes != null && applyTypes.size() > 0) {
                    applyTypeId = applyTypes.get(0).getId();
                    applyTypeSpinner.setSelection(0);
                    // 切换品牌
                    applyTypeSpinner.setOnItemSelectedListener(new ApplyTypeAdapter(applyTypes));
                } else {
                    applyTypeId = "";
                }

                tvNote.setText("*" + data.getNotes());
            }
        });
    }

    class CityAdapter implements AdapterView.OnItemSelectedListener {
        private List<CityBean> data;

        public CityAdapter(List<CityBean> data) {
            this.data = data;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String localCityId = getCityIds(data).get(position);
            if (localCityId.equals(cityId))
                return;
            cityId = localCityId;

            List<CityBean.BrandBean> brands = data.get(position).getBrand();
            brandSpinner.setAdapter(new ArrayAdapter<String>(OpenShopActivity.this, R.layout.spinner_item, R.id.tv_name, getBrandNames(brands)));
            if (brands != null && brands.size() > 0) {
                brandId = brands.get(0).getId();
                brandSpinner.setSelection(0);
            } else {
                brandId = "";
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class BrandAdapter implements AdapterView.OnItemSelectedListener {
        private List<CityBean.BrandBean> data;

        public BrandAdapter(List<CityBean.BrandBean> data) {
            this.data = data;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String localBrandId = getBrandIds(data).get(position);
            if (localBrandId.equals(brandId))
                return;
            brandId = localBrandId;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class ApplyTypeAdapter implements AdapterView.OnItemSelectedListener {
        private List<CityModel.ApplyTypeBean> data;

        public ApplyTypeAdapter(List<CityModel.ApplyTypeBean> data) {
            this.data = data;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String localApplyTypeId = getApplyTypeIds(data).get(position);
            if (localApplyTypeId.equals(applyTypeId))
                return;
            applyTypeId = localApplyTypeId;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    @NonNull
    private List<String> getCityNames(@NonNull List<CityBean> cities) {
        List<String> cityNames = new ArrayList<>();
        for (CityBean city : cities) {
            cityNames.add(city.getName());
        }
        return cityNames;
    }

    @NonNull
    private List<String> getCityIds(@NonNull List<CityBean> cities) {
        List<String> cityIds = new ArrayList<>();
        for (CityBean city : cities) {
            cityIds.add(city.getId());
        }
        return cityIds;
    }

    @NonNull
    private List<String> getBrandNames(@NonNull List<CityBean.BrandBean> brands) {
        List<String> brandNames = new ArrayList<>();
        for (CityBean.BrandBean brand : brands) {
            brandNames.add(brand.getName());
        }
        return brandNames;
    }

    @NonNull
    private List<String> getBrandIds(@NonNull List<CityBean.BrandBean> brands) {
        List<String> brandIds = new ArrayList<>();
        for (CityBean.BrandBean brand : brands) {
            brandIds.add(brand.getId());
        }
        return brandIds;
    }

    @NonNull
    private List<String> getApplyTypeNames(@NonNull List<CityModel.ApplyTypeBean> applyTypes) {
        List<String> applyTypeNames = new ArrayList<>();
        for (CityModel.ApplyTypeBean applyType : applyTypes) {
            applyTypeNames.add(applyType.getName());
        }
        return applyTypeNames;
    }

    @NonNull
    private List<String> getApplyTypeIds(@NonNull List<CityModel.ApplyTypeBean> applyTypes) {
        List<String> applyTypeIds = new ArrayList<>();
        for (CityModel.ApplyTypeBean applyType : applyTypes) {
            applyTypeIds.add(applyType.getId());
        }
        return applyTypeIds;
    }

    private boolean isMobileNO(@NonNull String mobile) {
        Pattern p = Pattern.compile("^((13[0-9])|(15[0-9])|(18[0-9])|(17[0-9])|(14[0-9]))\\d{8}$");
        Matcher m = p.matcher(mobile);
        return m.find();
    }

    private void getMsgCode(String mobile) {
        mApiService.getMsgCode(requestTag, mobile, new ZddStringCallback(getApplicationContext(), "success") {
            @Override
            public void success(String response) {
                ToastUtils.showMessage(getApplicationContext(), response);
            }
        });
    }

    @OnClick(R.id.btn_get_code)
    public void getCode(View v) {
        String mobile = etMobile.getText().toString();
        if (TextUtils.isEmpty(mobile)) {
            ToastUtils.showMessage(getApplicationContext(), "请填写手机号");
            return;
        } else if (!isMobileNO(mobile)) {
            ToastUtils.showMessage(getApplicationContext(), "手机号格式不正确");
            return;
        }
        getMsgCode(mobile);
        countDownTimerUtils.start();
    }

    @OnClick(R.id.tv_example)
    public void lookExample(View v) {
        Intent intent = new Intent(OpenShopActivity.this, ZDDWebViewActivity.class);
        intent.putExtra(ZDDWebViewActivity.TITLE, "证照示例");
        intent.putExtra(ZDDWebViewActivity.URL, URLConstants.URL_EXAMPLE);
        startActivity(intent);
    }

    @OnClick(R.id.btn_submit)
    public void submit(View v) {
        final String mobile = etMobile.getText().toString();
        if (TextUtils.isEmpty(mobile)) {
            ToastUtils.showMessage(getApplicationContext(), "请填写手机号");
            return;
        }
        String msgCode = etMsgCode.getText().toString();
        if (TextUtils.isEmpty(msgCode)) {
            ToastUtils.showMessage(getApplicationContext(), "请输入验证码");
            return;
        }
        String shopName = etShopName.getText().toString();
        if (TextUtils.isEmpty(shopName)) {
            ToastUtils.showMessage(getApplicationContext(), "请输入店名");
            return;
        }
        String shopLinkman = etShopLinkman.getText().toString();
        if (TextUtils.isEmpty(shopLinkman)) {
            ToastUtils.showMessage(getApplicationContext(), "请输入店主姓名");
            return;
        }
        String address = etAddress.getText().toString();
        if (TextUtils.isEmpty(address)) {
            ToastUtils.showMessage(getApplicationContext(), "请输入地址");
            return;
        }
        String salesmanMobile = etSalesmanMobile.getText().toString();

        /* form的分割线,自己定义 */
        String boundary = "xx--------------------------------------------------------------xx";

        MultipartBody.Builder builder = new MultipartBody.Builder(boundary).setType(MultipartBody.FORM)
                /* 上传一个普通的String参数 */
                .addFormDataPart("version", URLConstants.API_VERSION_4)
                .addFormDataPart("mobile", mobile)
                .addFormDataPart("code", msgCode)
                .addFormDataPart("name", shopName)
                .addFormDataPart("linkman", shopLinkman)
                .addFormDataPart("address", address)
                .addFormDataPart("city_id", cityId)
                //.addFormDataPart("brand_id", brandId)
                .addFormDataPart("applytype", applyTypeId);

        if (!TextUtils.isEmpty(salesmanMobile))
            builder.addFormDataPart("salesman_mobile", salesmanMobile);

        if (!TextUtils.isEmpty(shopPhotoPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(shopPhotoPath));
            builder = builder.addFormDataPart("file[]", "facade_img", fileBody);
        } else {
            ToastUtils.showMessage(getApplicationContext(), "请上传门头照");
            return;
        }
        if (!TextUtils.isEmpty(idcardBodyPhotoPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardBodyPhotoPath));
            builder = builder.addFormDataPart("file[]", "IDcard_body_photo", fileBody);
        } else {
            ToastUtils.showMessage(getApplicationContext(), "请上传手持身份证正面照");
            return;
        }

        if (!TextUtils.isEmpty(doorNumberPhotoPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(doorNumberPhotoPath));
            builder = builder.addFormDataPart("file[]", "door_number_photo", fileBody);
        }

        MultipartBody mBody = builder.build();

        DialogHelper.showDimDialog(OpenShopActivity.this, "正在上传...");

        final Request request = new Request.Builder().url(URLConstants.URL_OPEN_SHOP).post(mBody).build();
        new OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, @NonNull final IOException e) {
                runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      DialogHelper.dismissDialog();
                                      ToastUtils.showMessage(getApplicationContext(), e.getMessage());
                                  }
                              }
                );
            }

            public void onResponse(Call call, @NonNull final Response response) throws IOException {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DialogHelper.dismissDialog();

                        try {
                            String result = response.body().string();
                            if (response.code() != 200) {
                                JSONObject jsonObject = new JSONObject(result);
                                String error = jsonObject.getString("message");
                                ToastUtils.showMessage(getApplicationContext(), error);
                            } else {
                                OpenShopModel openShopModel = JSON.parseObject(result, OpenShopModel.class);
                                PictureUtil.deleteImgTmp(shopPhotoPath);
                                PictureUtil.deleteImgTmp(idcardBodyPhotoPath);
                                PictureUtil.deleteImgTmp(doorNumberPhotoPath);
                                // 保存申请的手机号
                                SPUtils.put(getApplicationContext(), KeyConstants.SHOP_MOBILE, mobile);
                                // 绑定推送别名
                                PushManager.getInstance().bindAlias(getApplicationContext(), "mobile_" + mobile);
                                ToastUtils.showMessage(getApplicationContext(), "信息提交成功");
                                Intent intent = new Intent(OpenShopActivity.this, ShopProgressActivity.class);
                                intent.putExtra(ShopProgressActivity.KEY_MODEL, openShopModel);
                                startActivity(intent);
                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    @OnClick(R.id.iv_shop_photo)
    public void addShopPhoto(@NonNull final View v) {
        // 判断图片Url是否为空，为空说明是添加图片，只有在修改模式下才会显示
        if (TextUtils.isEmpty(shopPhotoPath)) {
            MenuSheetView menuSheetView =
                    new MenuSheetView(OpenShopActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 1);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 1);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        } else {
            // 图片URL地址不为空，那么就是编辑这张图片
            MenuSheetView menuSheetView =
                    new MenuSheetView(OpenShopActivity.this, MenuSheetView.MenuType.LIST, "编辑照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            switch (item.getItemId()) {
                                case R.id.big_image:
                                    // 查看大图
                                    open(v, shopPhotoPath);
                                    break;
                                case R.id.delete:
                                    shopPhotoPath = null;
                                    ivShopPhoto.setImageResource(R.mipmap.add_image);
                                    break;
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_delete_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }


    @OnClick(R.id.iv_idcard_body_photo)
    public void addIdcardBodyPhoto(@NonNull final View v) {
        // 判断图片Url是否为空，为空说明是添加图片，只有在修改模式下才会显示
        if (TextUtils.isEmpty(idcardBodyPhotoPath)) {
            MenuSheetView menuSheetView =
                    new MenuSheetView(OpenShopActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 2);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 2);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        } else {
            // 图片URL地址不为空，那么就是编辑这张图片
            MenuSheetView menuSheetView =
                    new MenuSheetView(OpenShopActivity.this, MenuSheetView.MenuType.LIST, "编辑照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            switch (item.getItemId()) {
                                case R.id.big_image:
                                    // 查看大图
                                    open(v, idcardBodyPhotoPath);
                                    break;
                                case R.id.delete:
                                    idcardBodyPhotoPath = null;
                                    ivIdcardBodyPhoto.setImageResource(R.mipmap.add_image);
                                    break;
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_delete_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    @OnClick(R.id.iv_door_number_photo)
    public void addDoorNumberPhoto(@NonNull final View v) {
        // 判断图片Url是否为空，为空说明是添加图片，只有在修改模式下才会显示
        if (TextUtils.isEmpty(doorNumberPhotoPath)) {
            MenuSheetView menuSheetView =
                    new MenuSheetView(OpenShopActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.take_photo:
                                    // 拍照
                                    takePhoto((ImageView) v, 3);
                                    break;
                                case R.id.take_from_album:
                                    // 从相册选择
                                    takeFromAlbum((ImageView) v, 3);
                                    break;
                            }
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_choose_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        } else {
            // 图片URL地址不为空，那么就是编辑这张图片
            MenuSheetView menuSheetView =
                    new MenuSheetView(OpenShopActivity.this, MenuSheetView.MenuType.LIST, "编辑照片...", new MenuSheetView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(@NonNull MenuItem item) {
                            if (mSheetLayout.isSheetShowing()) {
                                mSheetLayout.dismissSheet();
                            }
                            switch (item.getItemId()) {
                                case R.id.big_image:
                                    // 查看大图
                                    open(v, doorNumberPhotoPath);
                                    break;
                                case R.id.delete:
                                    doorNumberPhotoPath = null;
                                    ivDoorNumberPhoto.setImageResource(R.mipmap.add_image);
                                    break;
                            }
                            return true;
                        }
                    });
            menuSheetView.inflateMenu(R.menu.menu_delete_image);
            mSheetLayout.showWithSheetView(menuSheetView);
        }
    }

    /**
     * 图片放大
     */
    private void open(@NonNull View view, String url) {
        if (!isOpening) {
            isOpening = true;
            Intent i = new Intent(getApplicationContext(), BigImageActivity.class);
            i.putExtra(KeyConstants.ORIG_X, Util.getXY(view)[0]);
            i.putExtra(KeyConstants.ORIG_Y, Util.getXY(view)[1]);
            i.putExtra(KeyConstants.ORIG_WIDTH, view.getWidth());
            i.putExtra(KeyConstants.ORIG_HEIGHT, view.getHeight());
            i.putExtra(KeyConstants.IMG_URL, url);

            // Start activity disable animations
            startActivity(i);
            OpenShopActivity.this.overridePendingTransition(0, 0);
        }
    }

    /**
     * 从相册选择
     */
    private void takeFromAlbum(@NonNull final ImageView imageView, final int index) {
        //带配置
        FunctionConfig config = new FunctionConfig.Builder()
                .build();

        GalleryFinal.openGallerySingle(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {

            @Override
            public void onHanlderSuccess(int reqeustCode, @NonNull List<PhotoInfo> resultList) {
                // 显示本地图片
                Bitmap bitmap = BitmapUtils.createImageThumbnail(getApplicationContext(), resultList.get(0).getPhotoPath());
                imageView.setImageBitmap(bitmap);
                try {
                    if (index == 1)
                        shopPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 2)
                        idcardBodyPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 3)
                        doorNumberPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    /**
     * 拍照
     */
    private void takePhoto(@NonNull final ImageView imageView, final int index) {
        if (Build.VERSION.SDK_INT >= 23) {
            int checkCallPhonePermission = ContextCompat.checkSelfPermission(OpenShopActivity.this, Manifest.permission.CAMERA);
            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(OpenShopActivity.this, new String[]{Manifest.permission.CAMERA}, 222);
                return;
            } else {
                openCamera(imageView, index);
            }
        } else {
            openCamera(imageView, index);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            //就像onActivityResult一样这个地方就是判断你是从哪来的。
            case 222:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                } else {
                    // Permission Denied
                    ToastUtils.showMessage(getApplicationContext(), "禁止相机权限将无法拍照");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void openCamera(@NonNull final ImageView imageView, final int index) {
        FunctionConfig config = getFunctionConfig();
        GalleryFinal.openCamera(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, @NonNull List<PhotoInfo> resultList) {
                // 显示本地图片
                Bitmap bitmap = BitmapUtils.createImageThumbnail(getApplicationContext(), resultList.get(0).getPhotoPath());
                imageView.setImageBitmap(bitmap);
                try {
                    if (index == 1)
                        shopPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 2)
                        idcardBodyPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 3)
                        doorNumberPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    private FunctionConfig getFunctionConfig() {
        return new FunctionConfig.Builder()
                .setEnableEdit(true)        //开启编辑功能
                .setEnableCrop(false)       //开启裁剪功能
                .setEnableRotate(false)     //开启旋转功能
                .setEnableCamera(true)      //开启相机功能
                .setCropWidth(DensityUtils.dp2px(getApplicationContext(), 100))     //裁剪宽度
                .setCropHeight(DensityUtils.dp2px(getApplicationContext(), 100))    //裁剪高度
                .setCropSquare(false)       //裁剪正方形
                .setForceCrop(false)        //启动强制裁剪功能,一进入编辑页面就开启图片裁剪，不需要用户手动点击裁剪，此功能只针对单选操作
                .setForceCropEdit(false)    //在开启强制裁剪功能时是否可以对图片进行编辑（也就是是否显示旋转图标和拍照图标）
                .setRotateReplaceSource(false)   //配置选择图片时是否替换原始图片，默认不替换
                .setCropReplaceSource(false)     //配置裁剪图片时是否替换原始图片，默认不替换
                .setEnablePreview(true)     //是否开启预览功能
                .build();
    }
}
