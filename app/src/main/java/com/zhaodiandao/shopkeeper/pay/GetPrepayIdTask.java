package com.zhaodiandao.shopkeeper.pay;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Xml;

import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.zhaodiandao.shopkeeper.config.KeyConstants;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class GetPrepayIdTask extends AsyncTask<Void, Void, Map<String, String>> {
    private ProgressDialog dialog;
    private Activity context;
    private Map<String, String> resultunifiedorder;
    private PayReq req;
    private String orderNumber;
    private float amount;
    private IWXAPI msgApi;
    private String notifyUrl;

    public GetPrepayIdTask(Activity context, String orderNumber, float amount, IWXAPI msgApi, String notifyUrl){
        req = new PayReq();
        this.orderNumber = orderNumber;
        this.amount = amount;
        this.context = context;
        this.msgApi = msgApi;
        this.notifyUrl = notifyUrl;
    }

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show(context, "提示", "正在获取支付订单");
    }

    @Override
    protected void onPostExecute(Map<String, String> result) {
        resultunifiedorder = result;
        if (dialog != null) {
            dialog.dismiss();
        }
        genPayReq();
        sendPayReq();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Nullable
    @Override
    protected Map<String, String> doInBackground(Void... params) {
        String url = String.format("https://api.mch.weixin.qq.com/pay/unifiedorder");
        String entity = genProductArgs();
        byte[] buf = Util.httpPost(url, entity);
        String content = new String(buf);
        Map<String, String> xml = decodeXml(content);
        return xml;
    }

    @Nullable
    public Map<String, String> decodeXml(@NonNull String content) {
        try {
            Map<String, String> xml = new HashMap<String, String>();
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(content));
            int event = parser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String nodeName = parser.getName();
                switch (event) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        if ("xml".equals(nodeName) == false) {
                            xml.put(nodeName, parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                }
                event = parser.next();
            }
            return xml;
        } catch (Exception e) {
            Log.e("orion", e.toString());
        }
        return null;
    }

    private String genProductArgs() {
        StringBuffer xml = new StringBuffer();
        try {
            String nonceStr = genNonceStr();
            xml.append("</xml>");
            List<NameValuePair> packageParams = new LinkedList<NameValuePair>();
            packageParams.add(new BasicNameValuePair("appid", KeyConstants.WEIXIN_APP_ID));
            packageParams.add(new BasicNameValuePair("body", "早点到掌柜订单: " + orderNumber));
            packageParams.add(new BasicNameValuePair("mch_id", KeyConstants.MCH_ID));
            packageParams.add(new BasicNameValuePair("nonce_str", nonceStr));
            packageParams.add(new BasicNameValuePair("notify_url", notifyUrl));
            packageParams.add(new BasicNameValuePair("out_trade_no", orderNumber + ""));
            packageParams.add(new BasicNameValuePair("spbill_create_ip", "127.0.0.1"));
            BigDecimal bd = new BigDecimal(amount * 100);
            int money = bd.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
            packageParams.add(new BasicNameValuePair("total_fee", money + ""));
            // packageParams.add(new BasicNameValuePair("total_fee", "1"));
            packageParams.add(new BasicNameValuePair("trade_type", "APP"));

            return genXml(packageParams);
        } catch (Exception e) {
            return null;
        }
    }

    @NonNull
    private String genXml(@NonNull List<NameValuePair> params) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("<?xml version='1.0' encoding='UTF-8' standalone='yes' ?><xml>");
        for (int i = 0; i < params.size(); i++) {
            // sb是用来计算签名的
            sb.append(params.get(i).getName());
            sb.append('=');
            sb.append(params.get(i).getValue());
            sb.append('&');
            // sb2是用来做请求的xml参数
            sb2.append("<" + params.get(i).getName() + ">");
            sb2.append(params.get(i).getValue());
            sb2.append("</" + params.get(i).getName() + ">");
        }
        sb.append("key=");
        sb.append(KeyConstants.API_KEY);
        String packageSign = null;
        // 生成签名
        packageSign = MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
        sb2.append("<sign><![CDATA[");
        sb2.append(packageSign);
        sb2.append("]]></sign>");
        sb2.append("</xml>");
        // 这一步最关键 我们把字符转为 字节后,再使用"ISO8859-1"进行编码，得到"ISO8859-1"的字符串
        try {
            return new String(sb2.toString().getBytes(), "ISO8859-1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void sendPayReq() {
        msgApi.registerApp(KeyConstants.WEIXIN_APP_ID);
        msgApi.sendReq(req);
    }

    private void genPayReq() {
        req.appId = KeyConstants.WEIXIN_APP_ID;
        req.partnerId = KeyConstants.MCH_ID;
        req.prepayId = resultunifiedorder.get("prepay_id");
        req.packageValue = "Sign=WXPay";
        req.nonceStr = genNonceStr();
        req.timeStamp = String.valueOf(genTimeStamp());

        List<NameValuePair> signParams = new LinkedList<NameValuePair>();
        signParams.add(new BasicNameValuePair("appid", req.appId));
        signParams.add(new BasicNameValuePair("noncestr", req.nonceStr));
        signParams.add(new BasicNameValuePair("package", req.packageValue));
        signParams.add(new BasicNameValuePair("partnerid", req.partnerId));
        signParams.add(new BasicNameValuePair("prepayid", req.prepayId));
        signParams.add(new BasicNameValuePair("timestamp", req.timeStamp));

        /* 这里是个坑，设置之后微信支付调不起来，所以这里不能设置超时参数
        String dateFormat = "yyyyMMddHHmmss";
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) + 30);
        Date dateInvalid = cal.getTime();
        String time_expire = new SimpleDateFormat(dateFormat).format(dateInvalid);
        signParams.add(new BasicNameValuePair("time_expire", time_expire));
        */

        req.sign = genAppSign(signParams);
    }

    @Nullable
    private String genNonceStr() {
        Random random = new Random();
        return MD5.getMessageDigest(String.valueOf(random.nextInt(10000)).getBytes());
    }

    private long genTimeStamp() {
        return System.currentTimeMillis() / 1000;
    }

    @Nullable
    private String genAppSign(@NonNull List<NameValuePair> params) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < params.size(); i++) {
            sb.append(params.get(i).getName());
            sb.append('=');
            sb.append(params.get(i).getValue());
            sb.append('&');
        }
        sb.append("key=");
        sb.append(KeyConstants.API_KEY);
        String appSign = MD5.getMessageDigest(sb.toString().getBytes());
        return appSign;
    }
}