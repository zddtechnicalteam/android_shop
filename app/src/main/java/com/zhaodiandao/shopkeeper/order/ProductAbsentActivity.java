/*
 * Copyright (c) 2017.
 * 早点到餐饮管理有限公司 版权所有
 */

package com.zhaodiandao.shopkeeper.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.model.AbsentProductInfo;
import com.zhaodiandao.shopkeeper.net.ZddStringCallback;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.ToastUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/2/17.
 */
public class ProductAbsentActivity extends BaseActivity {
    public static final String KEY_PRODUCTS = "key_products";
    public static final String KEY_ORDER_ID = "key_order_id";

    @Nullable
    @BindView(R.id.ll_product)
    LinearLayout llProduct;
    @Nullable
    @BindView(R.id.et_problem_desc)
    EditText etProblemDesc;

    private ArrayList<AbsentProductInfo> mData;
    private String orderId;
    private String problem;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_product_absent);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        mData = (ArrayList<AbsentProductInfo>) intent.getSerializableExtra(KEY_PRODUCTS);
        orderId = intent.getStringExtra(KEY_ORDER_ID);
        if (orderId == null)
            orderId = "";
        initProduct();
    }

    private void initProduct() {
        llProduct.removeAllViews();

        for (final AbsentProductInfo absentProductInfo : mData) {
            View v = getLayoutInflater().inflate(R.layout.listitem_product_absent, null);
            TextView tvName = (TextView) v.findViewById(R.id.tv_name);
            tvName.setText(absentProductInfo.getPname());
            ImageView ivAdd = (ImageView) v.findViewById(R.id.iv_add);
            final ImageView ivReduce = (ImageView) v.findViewById(R.id.iv_reduce);
            final TextView tvCount = (TextView) v.findViewById(R.id.tv_count);
            ivAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ivReduce.setVisibility(View.VISIBLE);
                    int num = absentProductInfo.getNumber()+1;
                    if (num > absentProductInfo.getAmount()) {
                        ToastUtils.showMessage(getApplicationContext(), "缺货数量不能超过购买数量");
                        return;
                    }
                    absentProductInfo.setNumber(num);
                    tvCount.setText(num + "");
                }
            });

            ivReduce.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int num = absentProductInfo.getNumber()-1;
                    absentProductInfo.setNumber(num);
                    tvCount.setText(num + "");
                    if (num <= 0) {
                        ivReduce.setVisibility(View.INVISIBLE);
                    }
                }
            });
            llProduct.addView(v);
        }
    }

    @OnClick(R.id.btn_submit)
    public void submit(View v) {
        JSONArray jsonArray = new JSONArray();
        for (AbsentProductInfo absentProductInfo : mData) {
            if (absentProductInfo.getNumber() > 0) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("sop_id", absentProductInfo.getSop_id());
                jsonObject.put("amount", absentProductInfo.getNumber() + "");
                jsonArray.add(jsonObject);
            }
        }

        if (jsonArray.size() <= 0) {
            ToastUtils.showMessage(getApplicationContext(), "请选择缺货产品数量");
            return;
        }

        problem = etProblemDesc.getText().toString().trim();

        DialogHelper.showDimDialog(ProductAbsentActivity.this, "正在提交...");
        mApiService.absentSubmit(requestTag, orderId, problem, jsonArray.toString(),
                new ZddStringCallback(getApplicationContext(), "message") {
                    @Override
                    public void success(String response) {
                        ToastUtils.showMessage(getApplicationContext(), response);
                        finish();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }
                });
    }
}
