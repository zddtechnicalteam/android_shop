package com.zhaodiandao.shopkeeper.order;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.alibaba.fastjson.JSONArray;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.account.PaySuccessActivity;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.PayModel;
import com.zhaodiandao.shopkeeper.model.PayResultModel;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.pay.Alipay;
import com.zhaodiandao.shopkeeper.pay.GetPrepayIdTask;
import com.zhaodiandao.shopkeeper.pay.PayResult;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/1/12.
 * 支付界面
 */
public class PayActivity extends BaseActivity implements View.OnClickListener {
    private static final int SDK_PAY_FLAG = 6;
    public static final String AMOUNT = "23";
    public static final String REMONEY = "24";
    public static final String WX = "21";
    public static final String ALIPAY = "22";
    @Nullable
    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;
    @Nullable
    @BindView(R.id.tv_order_money)
    TextView tvOrderMoney;
    @Nullable
    @BindView(R.id.rb_amount)
    RadioButton rbAmount;
    @Nullable
    @BindView(R.id.rb_remoney)
    RadioButton rbRemoney;
    @Nullable
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @Nullable
    @BindView(R.id.tv_remoney)
    TextView tvRemoney;
    @Nullable
    @BindView(R.id.rb_wx)
    RadioButton rbWx;
    @Nullable
    @BindView(R.id.rb_alipay)
    RadioButton rbAlipay;
    @Nullable
    @BindView(R.id.ll_third_pay)
    LinearLayout llThirdPay;
    @Nullable
    @BindView(R.id.line)
    View line;

    private double orderMoney;
    private double currentMoney;
    private double amount;
    private double remoney;
    private String ordernumber;

    public final IWXAPI msgApi = WXAPIFactory.createWXAPI(this, null);

    @NonNull
    private Handler mHandler = new InternalHandler(this);

    private static class InternalHandler extends Handler {
        private WeakReference<Activity> weakRefActivity;

        /**
         * A constructor that gets a weak reference to the enclosing class. We
         * do this to avoid memory leaks during Java Garbage Collection.
         */
        public InternalHandler(Activity activity) {
            weakRefActivity = new WeakReference<Activity>(activity);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            Activity activity = weakRefActivity.get();
            if (activity != null) {
                switch (msg.what) {
                    case SDK_PAY_FLAG:
                        PayResult payResult = new PayResult((String) msg.obj);
                        String resultStatus = payResult.getResultStatus();
                        if (TextUtils.equals(resultStatus, "9000")) {
                            ToastUtils.showMessage(activity.getApplicationContext(), "支付成功");
                            EventBus.getDefault().post(new MessageEvent(EventCode.PAY_SUCCESS, "支付成功!"));
                            activity.startActivity(new Intent(activity, PaySuccessActivity.class));
                        } else {
                            if (TextUtils.equals(resultStatus, "8000")) {
                                ToastUtils.showMessage(activity.getApplicationContext(), "支付结果确认中");
                            } else {
                                ToastUtils.showMessage(activity.getApplicationContext(), "支付失败");
                            }
                        }
                        break;
                }
            }
        }
    }

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_pay);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.ll_amount).setOnClickListener(this);
        findViewById(R.id.ll_remoney).setOnClickListener(this);
        findViewById(R.id.ll_wx).setOnClickListener(this);
        findViewById(R.id.ll_alipay).setOnClickListener(this);

        PayModel payModel = (PayModel) getIntent().getSerializableExtra("payInfo");
        ordernumber = payModel.getOrdernumber();
        amount = payModel.getAmount();
        remoney = payModel.getRemoney();
        orderMoney = payModel.getMoney();
        currentMoney = getOffsetMoney(amount);
        tvOrderNumber.setText(payModel.getOrdernumber());
        tvOrderMoney.setText("￥" + payModel.getMoney());
        tvAmount.setText("￥" + payModel.getAmount());
        tvRemoney.setText("￥" + payModel.getRemoney());

        checkAmount();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.ll_amount:
                if (rbAmount.isChecked()) {
                    rbAmount.setChecked(false);
                    currentMoney -= getOffsetMoney(amount);
                } else {
                    if ((currentMoney >= orderMoney || amount >= orderMoney) && rbRemoney.isChecked()) {
                        rbRemoney.setChecked(false);
                        currentMoney -= getOffsetMoney(remoney);
                    }
                    rbAmount.setChecked(true);
                    currentMoney += getOffsetMoney(amount);
                }
                checkAmount();
                break;
            case R.id.ll_remoney:
                if (rbRemoney.isChecked()) {
                    rbRemoney.setChecked(false);
                    currentMoney -= getOffsetMoney(remoney);
                } else {
                    if ((currentMoney >= orderMoney || remoney >= orderMoney) && rbAmount.isChecked()) {
                        rbAmount.setChecked(false);
                        currentMoney -= getOffsetMoney(amount);
                    }

                    rbRemoney.setChecked(true);
                    currentMoney += getOffsetMoney(remoney);
                }
                checkAmount();
                break;
            case R.id.ll_wx:
                rbWx.setChecked(true);
                rbAlipay.setChecked(false);
                break;
            case R.id.ll_alipay:
                rbWx.setChecked(false);
                rbAlipay.setChecked(true);
                break;
        }
    }

    private double getOffsetMoney(double amount) {
        return amount > orderMoney ? orderMoney : amount;
    }

    private void checkAmount() {
        if (currentMoney < orderMoney) {
            llThirdPay.setVisibility(View.VISIBLE);
            line.setVisibility(View.VISIBLE);
        } else {
            llThirdPay.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_pay)
    public void pay(View v) {
        JSONArray jsonArray = new JSONArray();
        if (rbAmount.isChecked()) {
            jsonArray.add(AMOUNT);
        }
        if (rbRemoney.isChecked()) {
            jsonArray.add(REMONEY);
        }
        if (llThirdPay.getVisibility() == View.VISIBLE) {
            if (rbWx.isChecked()) {
                jsonArray.add(WX);
            } else {
                jsonArray.add(ALIPAY);
            }
        }
        DialogHelper.showDimDialog(this, "正在提交...");
        submitPay(jsonArray.toString());
    }

    private void submitPay(String payType) {
        mApiService.pay(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), ordernumber, payType,
                new GenericCallback<PayResultModel>(getApplicationContext(), PayResultModel.class) {
                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }

                    @Override
                    public void success(@NonNull PayResultModel data) {
                        if (data.getPay_money() <= 0) {
                            ToastUtils.showMessage(getApplicationContext(), "支付成功");
                            EventBus.getDefault().post(new MessageEvent(EventCode.PAY_SUCCESS, "支付成功!"));
                            Intent intent = new Intent(PayActivity.this, PaySuccessActivity.class);
                            startActivity(intent);
                        } else {
                            if (rbWx.isChecked()) {
                                BigDecimal bd = new BigDecimal(data.getPay_money() * 100);
                                int money = bd.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
                                GetPrepayIdTask getPrepayId = new GetPrepayIdTask(PayActivity.this, data.getOrdernumber() + "_" + money, data.getPay_money(), msgApi, KeyConstants.WEIXIN_NOTIFY_URL2);
                                getPrepayId.execute();
                            } else if (rbAlipay.isChecked()) {
                                Alipay alipay = new Alipay(data.getOrdernumber(), data.getPay_money(), mHandler, PayActivity.this, KeyConstants.ALIPAY_NOTIFY_URL2);
                                alipay.pay();
                            }
                        }
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.REFRESH_PAY)
            finish();
    }
}
