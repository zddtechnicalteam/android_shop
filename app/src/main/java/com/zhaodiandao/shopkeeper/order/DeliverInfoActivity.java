package com.zhaodiandao.shopkeeper.order;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.DeliverInfoModel;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.PreOrderModel;
import com.zhaodiandao.shopkeeper.model.ProductClassBean;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.util.WheelView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/1/12.
 * 预生成订单界面
 */
public class DeliverInfoActivity extends BaseActivity {
    @Nullable
    @BindView(R.id.et_contact)
    EditText etContact;
    @Nullable
    @BindView(R.id.et_mobile)
    EditText etMobile;
    @Nullable
    @BindView(R.id.et_address)
    EditText etAddress;
    @Nullable
    @BindView(R.id.tv_date)
    TextView tvDate;
    @Nullable
    @BindView(R.id.tv_deliver)
    TextView tvDeliver;
    @Nullable
    @BindView(R.id.tv_deliver_desc)
    TextView tvDeliverDesc;
    @Nullable
    @BindView(R.id.et_remark)
    EditText etRemark;
    @Nullable
    @BindView(R.id.btn_to_pay)
    Button btnOk;
    @Nullable
    @BindView(R.id.line)
    View line;
    @Nullable
    @BindView(R.id.ll_choose_date)
    LinearLayout llChooseDate;

    private int currentTime = 0;
    private String deliverTime = "";
    private String deliverId;
    private int selectedDeliverIndex = 0;
    private Dialog dialog;
    @NonNull
    private List<String> dates = new ArrayList<>();
    @NonNull
    private List<String> deliverStr = new ArrayList<>();
    @NonNull
    private List<DeliverInfoModel.DeliverType> deliver = new ArrayList<>();
    private List<ProductClassBean.ProductBean> productInfos;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_deliver_info);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // 获取购物车信息
        Intent intent = getIntent();
        productInfos = (List<ProductClassBean.ProductBean>) intent.getSerializableExtra("infos");
        DialogHelper.showDimDialog(DeliverInfoActivity.this, "正在请求配送方式...");
        getDeliverInfo();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void preOrder() {
        JSONArray jsonArray = new JSONArray();
        for (ProductClassBean.ProductBean cartInfo : productInfos) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pid", cartInfo.getPid());
                jsonObject.put("amount", cartInfo.getNumber());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        String remark = etRemark.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String contact = etContact.getText().toString().trim();
        String mobile = etMobile.getText().toString().trim();

        mApiService.getPreOrder(requestTag, (String)SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                contact, mobile, address, remark, deliverId, deliverTime, jsonArray.toString(),
                new GenericCallback<PreOrderModel>(getApplicationContext(), PreOrderModel.class) {

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }

                    @Override
                    public void success(PreOrderModel data) {
                        Intent intent = new Intent(DeliverInfoActivity.this, PreOrderActivity.class);
                        intent.putExtra("orderInfo", data);
                        intent.putExtra("infos", (Serializable) productInfos);
                        startActivity(intent);
                    }
                });
    }

    private void getDeliverInfo() {
        mApiService.getDeliverInfo(requestTag, (String)SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericCallback<DeliverInfoModel>(getApplicationContext(), DeliverInfoModel.class) {

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }

                    @Override
                    public void success(@NonNull DeliverInfoModel data) {
                        etContact.setText(data.getLinkman());
                        etMobile.setText(data.getMobile());
                        etAddress.setText(data.getAddress());

                        deliverStr.clear();
                        deliver.clear();
                        deliver.addAll(data.getDeliver());
                        for (DeliverInfoModel.DeliverType deliverType : data.getDeliver()){
                            deliverStr.add(deliverType.getType());
                        }

                        dates.clear();
                        dates.addAll(data.getDelivertime());
                        btnOk.setEnabled(true);
                    }
                });
    }


    @OnClick(R.id.ll_choose_date)
    public void chooseDate(View v) {
        if (dates.size() <= 0){
            ToastUtils.showMessage(getApplicationContext(), "日期数据为空");
            return;
        }
        showDateWindow();
    }

    @OnClick(R.id.ll_choose_deliver)
    public void chooseDeliver(View v) {
        if (deliverStr.size() <= 0){
            ToastUtils.showMessage(getApplicationContext(), "配送方式不存在");
            return;
        }
        showDeliverWindow();
    }

    /**
     * 显示日期时间选择框
     */
    private void showDateWindow() {
        View outerView = LayoutInflater.from(this).inflate(R.layout.window_date_picker, (ViewGroup) getWindow().getDecorView(), false);
        // 确认按钮
        outerView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvDate.setText(dates.get(currentTime));
                deliverTime = dates.get(currentTime);
                dialog.dismiss();
            }
        });
        // 取消按钮
        outerView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        WheelView wv = (WheelView) outerView.findViewById(R.id.time_picker);
        wv.setOffset(1);
        wv.setItems(dates);
        wv.setSeletion(currentTime);
        wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                currentTime = selectedIndex - 1;
            }
        });

        dialog = new Dialog(this, R.style.DialogStyle);
        dialog.setContentView(outerView);
        dialog.show();
    }

    /**
     * 显示配送方式选择框
     */
    private void showDeliverWindow() {
        View outerView = LayoutInflater.from(this).inflate(R.layout.window_date_picker, (ViewGroup) getWindow().getDecorView(), false);
        TextView tvTitle = (TextView) outerView.findViewById(R.id.tv_dialog_title);
        tvTitle.setText("选择配送方式");
        // 确认按钮
        outerView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deliverType = deliverStr.get(selectedDeliverIndex);
                tvDeliver.setText(deliverType);
                deliverId = deliver.get(selectedDeliverIndex).getId();
                // 判断配送方式，只有"配送"类型才能选择配送时间
                if ("1".equals(deliverId)) {
                    line.setVisibility(View.VISIBLE);
                    llChooseDate.setVisibility(View.VISIBLE);
                } else {
                    deliverTime = "";
                    line.setVisibility(View.GONE);
                    llChooseDate.setVisibility(View.GONE);
                }
                tvDeliverDesc.setText(deliver.get(selectedDeliverIndex).getRemark());
                tvDeliverDesc.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });
        // 取消按钮
        outerView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        WheelView wv = (WheelView) outerView.findViewById(R.id.time_picker);
        wv.setOffset(1);
        wv.setItems(deliverStr);
        wv.setSeletion(selectedDeliverIndex);
        wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                selectedDeliverIndex = selectedIndex - 1;
            }
        });

        dialog = new Dialog(this, R.style.DialogStyle);
        dialog.setContentView(outerView);
        dialog.show();
    }

    @OnClick(R.id.btn_to_pay)
    public void submit(View v) {
        if (llChooseDate.getVisibility() == View.VISIBLE && TextUtils.isEmpty(deliverTime)) {
            ToastUtils.showMessage(getApplicationContext(), "请选择送货时间");
            return;
        }
        if (TextUtils.isEmpty(deliverId)) {
            ToastUtils.showMessage(getApplicationContext(), "请选择配送方式");
            return;
        }
        DialogHelper.showDimDialog(DeliverInfoActivity.this, "正在提交信息...");
        preOrder();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.CLOSE)
            finish();
    }
}
