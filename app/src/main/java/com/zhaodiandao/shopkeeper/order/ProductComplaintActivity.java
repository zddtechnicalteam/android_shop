package com.zhaodiandao.shopkeeper.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.BigImageActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.adapter.BrandImageAdapter;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.config.URLConstants;
import com.zhaodiandao.shopkeeper.model.BrandDetailModel;
import com.zhaodiandao.shopkeeper.model.ProductModel;
import com.zhaodiandao.shopkeeper.util.DensityUtils;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.PictureUtil;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.util.Util;
import com.zhaodiandao.shopkeeper.view.FixedEditText;
import com.zhaodiandao.shopkeeper.view.ScrollGridLayoutManager;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zhaodiandao.shopkeeper.R.id.recyclerView;
import static com.zhaodiandao.shopkeeper.account.BankRemarkActivity.REQUEST_CODE_GALLERY;

/**
 * Created by yanix on 2017/2/17.
 */
public class ProductComplaintActivity extends BaseActivity {
    public static final String KEY_PRODUCTS = "key_products";
    public static final String KEY_ORDER_ID = "key_order_id";

    @Nullable
    @BindView(R.id.bottomsheet)
    BottomSheetLayout mSheetLayout;
    @Nullable
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @Nullable
    @BindView(R.id.et_product_batch)
    FixedEditText mFixedEditText;
    @Nullable
    @BindView(R.id.et_problem_desc)
    EditText etProblemDesc;
    @Nullable
    @BindView(recyclerView)
    RecyclerView mRecyclerView;

    private ProductModel mProductModel;
    private String orderId;
    private String productId;
    private String batch;
    private String problem;
    // 当前显示的图片
    @NonNull
    private List<BrandDetailModel.ImageBean> datas = new ArrayList<>();
    // 从本地添加的图片
    @NonNull
    private List<BrandDetailModel.ImageBean> localDatas = new ArrayList<>();
    // 添加图片
    @NonNull
    private BrandDetailModel.ImageBean mImageBean = new BrandDetailModel.ImageBean();
    private BrandImageAdapter adapter;
    // 保存临时文件名
    @NonNull
    private List<String> tempNames = new ArrayList<>();
    // 保存要上传的文件
    @NonNull
    private List<File> uploadFiles = new ArrayList<>();
    // 最多能添加的图片数量
    private int maxSize;
    // 门店之前拥有的图片数量
    private int networkImageCount;
    // 指示当前图片放大动画是否打开，防止重复点击
    boolean isOpening;

    @Override
    protected void getView(Bundle savedInstanceState) {
        // 这里的作用是让状态栏从隐藏到显示的过程中,布局不受影响
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        );
        setContentView(R.layout.activity_product_complaint);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        /*FullyGridLayoutManager manager = new FullyGridLayoutManager(this, 3);
        manager.setOrientation(GridLayoutManager.VERTICAL);
        manager.setSmoothScrollbarEnabled(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setNestedScrollingEnabled(false);*/

        ScrollGridLayoutManager recyclerViews = new ScrollGridLayoutManager(this, 3);
        recyclerViews.setScrollEnabled(false);
        mRecyclerView.setLayoutManager(recyclerViews);

        // mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        datas.add(mImageBean);
        adapter = new BrandImageAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);
        // 照片点击事件
        adapter.setOnItemClickListener(new BrandImageAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(@NonNull final View view, @NonNull final BrandDetailModel.ImageBean data) {
                // 判断图片Url是否为空，为空说明是添加图片，只有在修改模式下才会显示
                if (TextUtils.isEmpty(data.getImg_url())) {
                    MenuSheetView menuSheetView =
                            new MenuSheetView(ProductComplaintActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(@NonNull MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.take_photo:
                                            // 拍照
                                            takePhoto();
                                            break;
                                        case R.id.take_from_album:
                                            // 从相册选择
                                            takeFromAlbum();
                                            break;
                                    }
                                    if (mSheetLayout.isSheetShowing()) {
                                        mSheetLayout.dismissSheet();
                                    }
                                    return true;
                                }
                            });
                    menuSheetView.inflateMenu(R.menu.menu_choose_image);
                    mSheetLayout.showWithSheetView(menuSheetView);
                } else {
                    MenuSheetView menuSheetView =
                            new MenuSheetView(ProductComplaintActivity.this, MenuSheetView.MenuType.LIST, "编辑照片...", new MenuSheetView.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(@NonNull MenuItem item) {
                                    if (mSheetLayout.isSheetShowing()) {
                                        mSheetLayout.dismissSheet();
                                    }
                                    switch (item.getItemId()) {
                                        case R.id.big_image:
                                            // 查看大图
                                            open(((RelativeLayout) view).getChildAt(0), data.getImg_url());
                                            break;
                                        case R.id.delete:
                                            // 删除图片，如果是本地添加的图片，直接从本地图片集合删除
                                            localDatas.remove(data);
                                            datas.remove(data);
                                            // 判断是否添加+号图片，图片如果已经存在，就不再重复添加
                                            if (!datas.contains(mImageBean))
                                                datas.add(mImageBean);
                                            adapter.notifyDataSetChanged();
                                            break;
                                    }
                                    return true;
                                }
                            });
                    menuSheetView.inflateMenu(R.menu.menu_delete_image);
                    mSheetLayout.showWithSheetView(menuSheetView);
                }
            }
        });

        Intent intent = getIntent();
        mProductModel = (ProductModel) intent.getSerializableExtra(KEY_PRODUCTS);
        orderId = intent.getStringExtra(KEY_ORDER_ID);
        if (orderId == null)
            orderId = "";
        initProduct();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpening = false;
    }

    private void open(@NonNull View view, String url) {
        if (!isOpening) {
            isOpening = true;
            Intent i = new Intent(getApplicationContext(), BigImageActivity.class);
            i.putExtra(KeyConstants.ORIG_X, Util.getXY(view)[0]);
            i.putExtra(KeyConstants.ORIG_Y, Util.getXY(view)[1]);
            i.putExtra(KeyConstants.ORIG_WIDTH, view.getWidth());
            i.putExtra(KeyConstants.ORIG_HEIGHT, view.getHeight());
            i.putExtra(KeyConstants.IMG_URL, url);

            // Start activity disable animations
            startActivity(i);
            ProductComplaintActivity.this.overridePendingTransition(0, 0);
        }
    }

    private void takeFromAlbum() {
        ArrayList<String> selectedImages = new ArrayList<>();
        // 计算已经选择的本地图片
        for (BrandDetailModel.ImageBean imageBean : localDatas) {
            selectedImages.add(imageBean.getImg_url());
        }

        //带配置
        FunctionConfig config = new FunctionConfig.Builder()
                .setMutiSelectMaxSize(6 - networkImageCount)    // 设置还能选中的图片数量
                .setSelected(selectedImages)  // 设置选中的图片
                .build();

        GalleryFinal.openGalleryMuti(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, @NonNull List<PhotoInfo> resultList) {
                updateGridView(resultList);
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    private void takePhoto() {
        FunctionConfig config = getFunctionConfig();
        GalleryFinal.openCamera(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, @NonNull List<PhotoInfo> resultList) {
                datas.remove(mImageBean);
                datas.removeAll(localDatas);
                BrandDetailModel.ImageBean imageBean = new BrandDetailModel.ImageBean(resultList.get(0).getPhotoPath());
                localDatas.add(imageBean);

                datas.addAll(localDatas);
                maxSize = 6 - datas.size();
                if (maxSize > 0) {
                    datas.add(mImageBean);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    private void updateGridView(@NonNull List<PhotoInfo> resultList) {
        datas.remove(mImageBean);
        datas.removeAll(localDatas);
        localDatas.clear();
        for (PhotoInfo photoInfo : resultList) {
            BrandDetailModel.ImageBean imageBean = new BrandDetailModel.ImageBean(photoInfo.getPhotoPath());
            localDatas.add(imageBean);
        }
        datas.addAll(localDatas);
        maxSize = 6 - datas.size();
        if (maxSize > 0) {
            datas.add(mImageBean);
        }
        adapter.notifyDataSetChanged();
    }

    private FunctionConfig getFunctionConfig() {
        return new FunctionConfig.Builder()
                .setEnableEdit(true)        //开启编辑功能
                .setEnableCrop(false)       //开启裁剪功能
                .setEnableRotate(false)     //开启旋转功能
                .setEnableCamera(true)      //开启相机功能
                .setCropWidth(DensityUtils.dp2px(getApplicationContext(), 100))     //裁剪宽度
                .setCropHeight(DensityUtils.dp2px(getApplicationContext(), 100))    //裁剪高度
                .setCropSquare(false)       //裁剪正方形
                .setForceCrop(false)        //启动强制裁剪功能,一进入编辑页面就开启图片裁剪，不需要用户手动点击裁剪，此功能只针对单选操作
                .setForceCropEdit(false)    //在开启强制裁剪功能时是否可以对图片进行编辑（也就是是否显示旋转图标和拍照图标）
                .setRotateReplaceSource(false)   //配置选择图片时是否替换原始图片，默认不替换
                .setCropReplaceSource(false)     //配置裁剪图片时是否替换原始图片，默认不替换
                .setEnablePreview(true)     //是否开启预览功能
                .build();
    }

    private void initProduct() {
        mRadioGroup.removeAllViews();

        for (ProductModel.ProductBean productBean : mProductModel.getProducts()) {
            View v = getLayoutInflater().inflate(R.layout.listitem_complaint_product, null);
            RadioButton radioButton = (RadioButton) v;
            radioButton.setId(productBean.getProduct_id());
            radioButton.setText(productBean.getProduct_name());
            mRadioGroup.addView(v);
        }

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                productId = checkedId + "";
            }
        });
    }

    @OnClick(R.id.btn_submit)
    public void submit(View v) {
        if (TextUtils.isEmpty(productId)) {
            ToastUtils.showMessage(getApplicationContext(), "请选择问题产品");
            return;
        }
        batch = mFixedEditText.getText().toString().trim();
        if (TextUtils.isEmpty(batch)) {
            ToastUtils.showMessage(getApplicationContext(), "请输入产品批次");
            return;
        }
        problem = etProblemDesc.getText().toString().trim();
        if (TextUtils.isEmpty(problem)) {
            ToastUtils.showMessage(getApplicationContext(), "请输入问题描述");
            return;
        }

        DialogHelper.showDimDialog(ProductComplaintActivity.this, "正在提交...");

        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                // 添加待上传文件
                for (BrandDetailModel.ImageBean imageBean : localDatas) {
                    try {
                        String tempName = PictureUtil.bitmapToPath(imageBean.getImg_url());
                        tempNames.add(tempName);
                        uploadFiles.add(new File(tempName));
                    } catch (@NonNull final IOException e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DialogHelper.dismissDialog();
                                ToastUtils.showMessage(getApplicationContext(), e.toString());
                            }
                        });
                        return;
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        uploadImage();
                    }
                });
            }
        });
    }

    private void uploadImage() {
        String boundary = "xx--------------------------------------------------------------xx";

        MultipartBody.Builder builder = new MultipartBody.Builder(boundary).setType(MultipartBody.FORM)
                .addFormDataPart("version", URLConstants.API_VERSION)
                .addFormDataPart("shop_id", (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""))
                .addFormDataPart("order_id", orderId)
                .addFormDataPart("product_id", productId)
                .addFormDataPart("batch", batch)
                .addFormDataPart("problem", problem);

        for (int i = 0; i < uploadFiles.size(); i++) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), uploadFiles.get(i));
            String fileName = uploadFiles.get(i).getName();
            builder = builder.addFormDataPart("file[]", fileName, fileBody);
        }
        MultipartBody mBody = builder.build();

        Request request = new Request.Builder().url(URLConstants.URL_SUBMIT_FEEDBACK).post(mBody).build();
        new OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, @NonNull final IOException e) {
                runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      DialogHelper.dismissDialog();
                                      ToastUtils.showMessage(getApplicationContext(), e.getMessage());
                                  }
                              }
                );
            }

            public void onResponse(Call call, @NonNull final Response response) throws IOException {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DialogHelper.dismissDialog();

                        try {
                            String result = response.body().string();
                            if (response.code() != 200) {
                                JSONObject jsonObject = new JSONObject(result);
                                String error = jsonObject.getString("message");
                                ToastUtils.showMessage(getApplicationContext(), error);
                            } else {
                                PictureUtil.deleteImgTmp(tempNames);
                                tempNames.clear();
                                ToastUtils.showMessage(getApplicationContext(), "提交成功");
                                startActivity(new Intent(ProductComplaintActivity.this, ProductComplaintSuccessActivity.class));
                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
