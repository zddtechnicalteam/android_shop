package com.zhaodiandao.shopkeeper.order;

import android.os.Bundle;
import android.view.View;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/2/20.
 * 投诉成功
 */
public class ProductComplaintSuccessActivity extends BaseActivity {

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_complaint_success);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.btn_ok)
    public void ok(View v) {
        finish();
    }
}
