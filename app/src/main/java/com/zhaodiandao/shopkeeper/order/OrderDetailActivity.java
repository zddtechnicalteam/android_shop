package com.zhaodiandao.shopkeeper.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.AbsentProductInfo;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.OrderDetailModel;
import com.zhaodiandao.shopkeeper.model.PayModel;
import com.zhaodiandao.shopkeeper.model.ProductModel;
import com.zhaodiandao.shopkeeper.model.TicketItemBean;
import com.zhaodiandao.shopkeeper.net.GenericArrayCallback;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.net.ZddStringCallback;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.view.CustemConfirmDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by yanix on 2017/1/13.
 * 订单详情
 */
public class OrderDetailActivity extends BaseActivity {
    public static final String ORDER_ID = "order_id";
    @Nullable
    @BindView(R.id.ll_menu)
    LinearLayout llMenu;
    @Nullable
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @Nullable
    @BindView(R.id.ll_pay)
    LinearLayout llPay;
    @Nullable
    @BindView(R.id.pb_loading)
    View mProgressBar;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView mTvRetry;
    @Nullable
    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;
    @Nullable
    @BindView(R.id.tv_linkman)
    TextView tvLinkman;
    @Nullable
    @BindView(R.id.tv_mobile)
    TextView tvMobile;
    @Nullable
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @Nullable
    @BindView(R.id.tv_deliver_time)
    TextView tvDeliverTime;
    @Nullable
    @BindView(R.id.tv_vehicle_number)
    TextView tvVehicleNumber;
    @Nullable
    @BindView(R.id.tv_driver_name)
    TextView tvDriverName;
    @Nullable
    @BindView(R.id.tv_driver_mobile)
    TextView tvDriverMobile;
    @Nullable
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @Nullable
    @BindView(R.id.tv_product_money)
    TextView tvProductMoney;
    @Nullable
    @BindView(R.id.tv_deliver_fee)
    TextView tvDeliverFee;
    @Nullable
    @BindView(R.id.tv_discount_money)
    TextView tvDiscountMoney;
    @Nullable
    @BindView(R.id.tv_sum)
    TextView tvSum;
    @Nullable
    @BindView(R.id.tv_deliver_type)
    TextView tvDeliverType;
    @Nullable
    @BindView(R.id.line)
    View line;
    @Nullable
    @BindView(R.id.ll_deliver_time)
    LinearLayout llDeliverTime;
    @Nullable
    @BindView(R.id.ll_complaint)
    LinearLayout llComplaint;
    @BindView(R.id.ll_deliver)
    LinearLayout llDeliver;
    @Nullable
    @BindView(R.id.tv_ticket_money)
    TextView tvTicketMoney;
    @BindView(R.id.tv_stk_ticket_money)
    TextView tvStkTicketMoney;

    private String orderId;
    private String orderNumber;
    private CustemConfirmDialog dialog;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        orderId = getIntent().getStringExtra(ORDER_ID);
        getOrderDetail();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void getOrderDetail() {
        mApiService.getOrderDetail(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), orderId,
                new GenericCallback<OrderDetailModel>(getApplicationContext(), OrderDetailModel.class) {

                    @Override
                    public void success(@NonNull OrderDetailModel data) {
                        orderNumber = data.getOrdernumber();
                        tvOrderNumber.setText(data.getOrdernumber());
                        tvLinkman.setText(data.getLinkman());
                        tvMobile.setText(data.getMobile());
                        tvAddress.setText(data.getAddress());
                        tvDeliverType.setText(data.getDelivertype_label());
                        if ("1".equals(data.getDelivertype_id())) {
                            line.setVisibility(View.VISIBLE);
                            llDeliverTime.setVisibility(View.VISIBLE);
                            tvDeliverTime.setText(data.getDelivertime());
                        } else {
                            line.setVisibility(View.GONE);
                            llDeliverTime.setVisibility(View.GONE);
                        }
                        tvVehicleNumber.setText(data.getVehiclenumber());
                        tvDriverName.setText(data.getDrivername());
                        tvDriverMobile.setText(data.getDrivermobile());
                        int status = data.getStatus();
                        // 5=未支付 10未审核 20=已审核 50=已配送 60=已完成 80=已取消
                        llPay.setVisibility(View.GONE);
                        llComplaint.setVisibility(View.GONE);
                        llDeliver.setVisibility(View.GONE);
                        if (status == 5) {
                            tvStatus.setText("未支付");
                            llPay.setVisibility(View.VISIBLE);
                        } else if (status == 10) {
                            tvStatus.setText("未审核");
                        } else if (status == 20) {
                            tvStatus.setText("已审核");
                        } else if (status == 50) {
                            llDeliver.setVisibility(View.VISIBLE);
                            tvStatus.setText("已配送");
                        } else if (status == 60) {
                            llComplaint.setVisibility(View.VISIBLE);
                            tvStatus.setText("已完成");
                        } else if (status == 80) {
                            tvStatus.setText("已取消");
                        } else {
                            tvStatus.setText("未知状态");
                        }
                        tvProductMoney.setText("￥" + data.getProductmoney());
                        tvDeliverFee.setText("￥" + data.getDelivermoney());
                        tvTicketMoney.setText("-￥" + data.getPft_money());
                        tvStkTicketMoney.setText("-￥" + data.getStk_money());
                        tvDiscountMoney.setText("-￥" + data.getDiscountmoney());
                        tvSum.setText("￥" + data.getSummoney());
                        llContent.setVisibility(View.VISIBLE);
                        initMenu(data);
                    }

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    private void initMenu(@NonNull OrderDetailModel data) {
        llMenu.removeAllViews();

        for (OrderDetailModel.BrandBean brandBean : data.getProductdata()) {
            View v = getLayoutInflater().inflate(R.layout.listitem_brand, null);
            TextView tvBrandName = (TextView) v.findViewById(R.id.tv_brand_name);
            TextView tvDeliverFee = (TextView) v.findViewById(R.id.tv_deliver_fee);
            LinearLayout llProduct = (LinearLayout) v.findViewById(R.id.ll_product);
            tvBrandName.setText(brandBean.getBrandname());
            tvDeliverFee.setText("￥" + brandBean.getDelivermoney());

            for (OrderDetailModel.BrandBean.ProductBean productBean : brandBean.getProducts()) {
                View vProduct = getLayoutInflater().inflate(R.layout.item_product, null);
                TextView tvProductName = (TextView) vProduct.findViewById(R.id.tv_product_name);
                TextView tvCount = (TextView) vProduct.findViewById(R.id.tv_count);
                TextView tvMoney = (TextView) vProduct.findViewById(R.id.tv_money);
                tvProductName.setText(productBean.getName());
                tvCount.setText("x" + productBean.getAmount());
                tvMoney.setText("￥" + productBean.getMoney());
                llProduct.addView(vProduct);
            }
            llMenu.addView(v);
        }

        if (data.getFoodtickets() != null && data.getFoodtickets().size() > 0) {
            View v = getLayoutInflater().inflate(R.layout.item_ticket, null);
            LinearLayout llTicket = (LinearLayout) v.findViewById(R.id.ll_ticket);
            for (TicketItemBean ticketItemBean : data.getFoodtickets()) {
                View vProduct = getLayoutInflater().inflate(R.layout.item_product, null);
                TextView tvProductName = (TextView) vProduct.findViewById(R.id.tv_product_name);
                TextView tvCount = (TextView) vProduct.findViewById(R.id.tv_count);
                TextView tvMoney = (TextView) vProduct.findViewById(R.id.tv_money);
                tvProductName.setText(ticketItemBean.getName());
                tvCount.setText("x" + ticketItemBean.getAmount());
                tvMoney.setText("￥" + ticketItemBean.getMoney());
                llTicket.addView(vProduct);
            }
            llMenu.addView(v);
        }

        if (data.getStktickets() != null && data.getStktickets().size() > 0) {
            View v = getLayoutInflater().inflate(R.layout.item_ticket, null);
            TextView title = (TextView) v.findViewById(R.id.tv_brand_name);
            title.setText("预购券");
            LinearLayout llTicket = (LinearLayout) v.findViewById(R.id.ll_ticket);
            for (TicketItemBean ticketItemBean : data.getStktickets()) {
                View vProduct = getLayoutInflater().inflate(R.layout.item_product, null);
                TextView tvProductName = (TextView) vProduct.findViewById(R.id.tv_product_name);
                TextView tvCount = (TextView) vProduct.findViewById(R.id.tv_count);
                TextView tvMoney = (TextView) vProduct.findViewById(R.id.tv_money);
                tvProductName.setText(ticketItemBean.getName());
                tvCount.setText("x" + ticketItemBean.getAmount());
                tvMoney.setText("￥" + ticketItemBean.getMoney());
                llTicket.addView(vProduct);
            }
            llMenu.addView(v);
        }
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getOrderDetail();
    }

    @OnClick(R.id.tv_cancel)
    public void cancel(View view) {
        if (dialog == null) {
            dialog = new CustemConfirmDialog(this, R.style.QQStyle, "确定取消订单吗?", "确定",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            cancelOrder();
                        }
                    });
        }
        dialog.show();

    }

    private void cancelOrder() {
        mApiService.cancelOrder(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), orderNumber,
                new ZddStringCallback(getApplicationContext(), "message") {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(OrderDetailActivity.this, "正在取消订单...");
                    }

                    @Override
                    public void onAfter(int id) {
                        DialogHelper.dismissDialog();
                        super.onAfter(id);
                    }

                    @Override
                    public void success(String response) {
                        ToastUtils.showMessage(getApplicationContext(), response);
                        EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_ORDER, "刷新订单!"));
                        getOrderDetail();
                    }
                });
    }

    @OnClick(R.id.tv_to_pay)
    public void toPay(View v) {
        mApiService.toPay(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), orderNumber,
                new GenericCallback<PayModel>(getApplicationContext(), PayModel.class) {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(OrderDetailActivity.this, "正在获取支付信息...");
                    }

                    @Override
                    public void onAfter(int id) {
                        DialogHelper.dismissDialog();
                        super.onAfter(id);
                    }

                    @Override
                    public void success(PayModel data) {
                        Intent intent = new Intent(OrderDetailActivity.this, PayActivity.class);
                        intent.putExtra("payInfo", data);
                        startActivity(intent);
                    }
                });
    }

    @OnClick(R.id.btn_complaint)
    public void toComplaint(View v) {
        mApiService.toFeedback(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), orderId,
                new GenericCallback<ProductModel>(getApplicationContext(), ProductModel.class) {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(OrderDetailActivity.this, "努力请求中...");
                    }

                    @Override
                    public void onAfter(int id) {
                        DialogHelper.dismissDialog();
                        super.onAfter(id);
                    }

                    @Override
                    public void success(ProductModel data) {
                        Intent intent = new Intent(OrderDetailActivity.this, ProductComplaintActivity.class);
                        intent.putExtra(ProductComplaintActivity.KEY_PRODUCTS, data);
                        intent.putExtra(ProductComplaintActivity.KEY_ORDER_ID, orderId);
                        startActivity(intent);
                    }
                });
    }

    @OnClick(R.id.tv_absent)
    public void toAbsent(View v) {
        mApiService.toAbsent(requestTag, orderId,
                new GenericArrayCallback<AbsentProductInfo>(getApplicationContext(), AbsentProductInfo.class) {
                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(OrderDetailActivity.this, "努力请求中...");
                    }

                    @Override
                    public void onAfter(int id) {
                        DialogHelper.dismissDialog();
                        super.onAfter(id);
                    }

                    @Override
                    public void success(List<AbsentProductInfo> data) {
                        ArrayList<AbsentProductInfo> tData = new ArrayList<>();
                        tData.addAll(data);
                        Intent intent = new Intent(OrderDetailActivity.this, ProductAbsentActivity.class);
                        intent.putExtra(ProductComplaintActivity.KEY_PRODUCTS, tData);
                        intent.putExtra(ProductComplaintActivity.KEY_ORDER_ID, orderId);
                        startActivity(intent);
                    }
                });
    }

    @OnClick(R.id.tv_finish)
    public void orderFinish(View v) {
        mApiService.orderFinish(requestTag, orderNumber,
                new ZddStringCallback(getApplicationContext(), "message") {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(OrderDetailActivity.this, "正在请求...");
                    }

                    @Override
                    public void onAfter(int id) {
                        DialogHelper.dismissDialog();
                        super.onAfter(id);
                    }

                    @Override
                    public void success(String response) {
                        ToastUtils.showMessage(getApplicationContext(), response);
                        EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_ORDER, "刷新订单!"));
                        getOrderDetail();
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.REFRESH_PAY) {
            EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_ORDER, "刷新订单!"));
            getOrderDetail();
        }
    }
}
