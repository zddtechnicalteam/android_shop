package com.zhaodiandao.shopkeeper.order;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.fragment.OrderStatusFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yangxu on 2017/1/10.
 */
public class OrderListActivity extends BaseActivity {
    public static final String KEY_FROM_FEEDBACK = "from_feedback";
    private static final int STATUS_NOT_PAY = 1;
    private static final int STATUS_VALID = 2;
    private static final int STATUS_INVALID = 3;
    @Nullable
    @BindView(R.id.viewpager)
    ViewPager mPager;
    @Nullable
    @BindView(R.id.cursor)
    View cursor;
    @Nullable
    @BindView(R.id.rb_not_pay)
    RadioButton rbNotPay;
    @Nullable
    @BindView(R.id.rb_valid)
    RadioButton rbValid;
    @Nullable
    @BindView(R.id.rb_invalid)
    RadioButton rbInvalid;
    private RelativeLayout.LayoutParams layoutParams;
    private int bmpW;
    private int screenW;
    private int wholeWidth;
    private List<Fragment> fragmentList;
    private static final int TAB_COUNT = 3;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        boolean isFromFeedback = getIntent().getBooleanExtra(KEY_FROM_FEEDBACK, false);
        initCursor();
        initTab(isFromFeedback);
        initViewPager(isFromFeedback);
    }

    private void initTab(boolean isFromFeedback) {
        rbNotPay.setOnClickListener(new MyClickListener(0));
        rbValid.setOnClickListener(new MyClickListener(1));
        rbInvalid.setOnClickListener(new MyClickListener(2));
        if (isFromFeedback) {
            rbValid.setChecked(true);
        } else {
            rbNotPay.setChecked(true);
        }
    }

    public class MyClickListener implements View.OnClickListener {
        private int index = 0;

        public MyClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            mPager.setCurrentItem(index, true);
        }
    }

    private void initCursor() {
        layoutParams = (RelativeLayout.LayoutParams) cursor.getLayoutParams();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        wholeWidth = dm.widthPixels;
        screenW = dm.widthPixels;
        bmpW = screenW / TAB_COUNT;
        layoutParams.width = bmpW;
        cursor.setLayoutParams(layoutParams);
        cursor.setVisibility(View.VISIBLE);
    }

    private void initViewPager(boolean isFromFeedback) {
        fragmentList = new ArrayList<>();
        OrderStatusFragment notPayFragment = new OrderStatusFragment();
        Bundle bundleNotUse= new Bundle();
        bundleNotUse.putInt("status", STATUS_NOT_PAY);
        notPayFragment.setArguments(bundleNotUse);

        OrderStatusFragment validFragment = new OrderStatusFragment();
        Bundle bundleUsed= new Bundle();
        bundleUsed.putInt("status", STATUS_VALID);
        validFragment.setArguments(bundleUsed);

        OrderStatusFragment invalidFragment = new OrderStatusFragment();
        Bundle bundleInvalid = new Bundle();
        bundleInvalid.putInt("status", STATUS_INVALID);
        invalidFragment.setArguments(bundleInvalid);

        fragmentList.add(notPayFragment);
        fragmentList.add(validFragment);
        fragmentList.add(invalidFragment);

        //给ViewPager设置适配器
        mPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager()));
        mPager.setOffscreenPageLimit(TAB_COUNT-1);
        mPager.setCurrentItem(isFromFeedback ? 1 : 0);//设置当前显示标签页
        mPager.addOnPageChangeListener(new MyOnPageChangeListener());//页面变化时的监听器
    }

    /**
     * 视图加载完成之后执行：值得注意的是PopupWindow每次显示和销毁都会执行一次
     */
    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            tabMove(position, positionOffsetPixels);
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageSelected(int position) {
            tabChange(position);
        }

        private void tabMove(int position, int positionOffsetPixels) {
            int moveI = (int) (bmpW * position + (((double) positionOffsetPixels / wholeWidth) * bmpW));
            layoutParams.leftMargin = moveI;
            cursor.setLayoutParams(layoutParams);
        }

        private void tabChange(int position) {
            if (position == 0) {
                rbNotPay.setChecked(true);
            } else if (position == 1) {
                rbValid.setChecked(true);
            } else if (position == 2) {
                rbInvalid.setChecked(true);
            }
        }
    }

    class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
