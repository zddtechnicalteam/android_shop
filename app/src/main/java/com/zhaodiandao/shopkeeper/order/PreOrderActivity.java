package com.zhaodiandao.shopkeeper.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.PayModel;
import com.zhaodiandao.shopkeeper.model.PreOrderModel;
import com.zhaodiandao.shopkeeper.model.ProductClassBean;
import com.zhaodiandao.shopkeeper.model.TicketItemBean;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/1/12.
 * 预生成订单界面
 */
public class PreOrderActivity extends BaseActivity {
    @Nullable
    @BindView(R.id.tv_linkman)
    TextView tvLinkman;
    @Nullable
    @BindView(R.id.tv_mobile)
    TextView tvMobile;
    @Nullable
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @Nullable
    @BindView(R.id.tv_remark)
    TextView tvRemark;
    @Nullable
    @BindView(R.id.tv_deliver_time)
    TextView tvDeliverTime;
    @Nullable
    @BindView(R.id.tv_deliver_type)
    TextView tvDeliverType;
    @Nullable
    @BindView(R.id.ll_menu)
    LinearLayout llMenu;
    @Nullable
    @BindView(R.id.tv_product_money)
    TextView tvProductMoney;
    @Nullable
    @BindView(R.id.tv_deliver_explain)
    TextView tvDeliverExplain;
    @Nullable
    @BindView(R.id.tv_deliver_fee)
    TextView tvDeliverFee;
    @Nullable
    @BindView(R.id.tv_ticket_money)
    TextView tvTicketMoney;
    @BindView(R.id.tv_stk_ticket_money)
    TextView tvStkTicketMoney;
    @Nullable
    @BindView(R.id.tv_discount_money)
    TextView tvDiscountMoney;
    @Nullable
    @BindView(R.id.tv_sum)
    TextView tvSum;
    @Nullable
    @BindView(R.id.btn_to_pay)
    Button btnOk;
    @Nullable
    @BindView(R.id.line)
    View line;
    @Nullable
    @BindView(R.id.ll_deliver_time)
    LinearLayout llDeliverTime;

    private PreOrderModel data;
    private List<ProductClassBean.ProductBean> productInfos;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_pre_order);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // 获取购物车信息
        Intent intent = getIntent();
        data = (PreOrderModel) intent.getSerializableExtra("orderInfo");
        productInfos = (List<ProductClassBean.ProductBean>) intent.getSerializableExtra("infos");
        tvLinkman.setText(data.getLinkman());
        tvMobile.setText(data.getMobile());
        tvAddress.setText(data.getAddress());
        tvRemark.setText(data.getRemark());
        if (!TextUtils.isEmpty(data.getDelivertime())) {
            llDeliverTime.setVisibility(View.VISIBLE);
            line.setVisibility(View.VISIBLE);
            tvDeliverTime.setText(data.getDelivertime());
        } else {
            llDeliverTime.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
        }
        tvDeliverType.setText(data.getDelivertype_label());
        tvProductMoney.setText("￥" + data.getProductmoney());
        tvDeliverFee.setText("￥" + data.getDelivermoney());
        tvTicketMoney.setText("-￥" + data.getPft_money());
        tvStkTicketMoney.setText("-￥" + data.getStk_money());
        tvDiscountMoney.setText("-￥" + data.getDiscountmoney());
        tvSum.setText("￥" + data.getSummoney());
        initMenu();
        btnOk.setEnabled(true);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void submitOrder() {
        JSONArray jsonArray = new JSONArray();
        for (ProductClassBean.ProductBean cartInfo : productInfos) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pid", cartInfo.getPid());
                jsonObject.put("amount", cartInfo.getNumber());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }

        mApiService.createOrder(requestTag, (String)SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                data.getLinkman(), data.getMobile(), data.getAddress(), data.getDelivertype(), data.getDelivertime(),
                data.getRemark(), jsonArray.toString(),
                new GenericCallback<PayModel>(getApplicationContext(), PayModel.class) {

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }

                    @Override
                    public void success(PayModel data) {
                        EventBus.getDefault().post(new MessageEvent(EventCode.CLOSE, "关闭!"));
                        Intent intent = new Intent(PreOrderActivity.this, PayActivity.class);
                        intent.putExtra("payInfo", data);
                        startActivity(intent);
                        finish();
                    }
                });
    }

    private void initMenu() {
        llMenu.removeAllViews();

        for (PreOrderModel.BrandBean brandBean : data.getProductdata()) {
            View v = getLayoutInflater().inflate(R.layout.listitem_brand, null);
            TextView tvBrandName = (TextView) v.findViewById(R.id.tv_brand_name);
            TextView tvDeliverFee = (TextView) v.findViewById(R.id.tv_deliver_fee);
            LinearLayout llProduct = (LinearLayout) v.findViewById(R.id.ll_product);
            tvBrandName.setText(brandBean.getBrandname());
            tvDeliverFee.setText("￥" + brandBean.getDelivermoney());

            for (PreOrderModel.BrandBean.ProductBean productBean : brandBean.getProducts()) {
                View vProduct = getLayoutInflater().inflate(R.layout.item_product, null);
                TextView tvProductName = (TextView) vProduct.findViewById(R.id.tv_product_name);
                TextView tvCount = (TextView) vProduct.findViewById(R.id.tv_count);
                TextView tvMoney = (TextView) vProduct.findViewById(R.id.tv_money);
                tvProductName.setText(productBean.getName());
                tvCount.setText("x" + productBean.getAmount());
                tvMoney.setText("￥" + productBean.getMoney());
                llProduct.addView(vProduct);
            }
            llMenu.addView(v);
        }

        if (data.getFoodticket() != null && data.getFoodticket().size() > 0) {
            View v = getLayoutInflater().inflate(R.layout.item_ticket, null);
            LinearLayout llTicket = (LinearLayout) v.findViewById(R.id.ll_ticket);
            for (TicketItemBean ticketItemBean : data.getFoodticket()) {
                View vProduct = getLayoutInflater().inflate(R.layout.item_product, null);
                TextView tvProductName = (TextView) vProduct.findViewById(R.id.tv_product_name);
                TextView tvCount = (TextView) vProduct.findViewById(R.id.tv_count);
                TextView tvMoney = (TextView) vProduct.findViewById(R.id.tv_money);
                tvProductName.setText(ticketItemBean.getName());
                tvCount.setText("x" + ticketItemBean.getAmount());
                tvMoney.setText("￥" + ticketItemBean.getMoney());
                llTicket.addView(vProduct);
            }
            llMenu.addView(v);
        }

        if (data.getStkticket() != null && data.getStkticket().size() > 0) {
            View v = getLayoutInflater().inflate(R.layout.item_ticket, null);
            TextView title = (TextView) v.findViewById(R.id.tv_brand_name);
            title.setText("预购券");
            LinearLayout llTicket = (LinearLayout) v.findViewById(R.id.ll_ticket);
            for (TicketItemBean ticketItemBean : data.getStkticket()) {
                View vProduct = getLayoutInflater().inflate(R.layout.item_product, null);
                TextView tvProductName = (TextView) vProduct.findViewById(R.id.tv_product_name);
                TextView tvCount = (TextView) vProduct.findViewById(R.id.tv_count);
                TextView tvMoney = (TextView) vProduct.findViewById(R.id.tv_money);
                tvProductName.setText(ticketItemBean.getName());
                tvCount.setText("x" + ticketItemBean.getAmount());
                tvMoney.setText("￥" + ticketItemBean.getMoney());
                llTicket.addView(vProduct);
            }
            llMenu.addView(v);
        }
    }

    @OnClick(R.id.btn_to_pay)
    public void toPay(View v) {
        DialogHelper.showDimDialog(PreOrderActivity.this, "正在生成订单...");
        submitOrder();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.PAY_SUCCESS)
            finish();
    }
}
