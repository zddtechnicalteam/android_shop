package com.zhaodiandao.shopkeeper;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.hyphenate.chat.EMClient;
import com.igexin.sdk.PushManager;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.login.LoginActivity;
import com.zhaodiandao.shopkeeper.model.OpenShopModel;
import com.zhaodiandao.shopkeeper.model.UpdateInfo;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.shop.ShopProgressActivity;
import com.zhaodiandao.shopkeeper.util.AppUtils;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.view.CustemConfirmDialog;
import com.zx.uploadlibrary.listener.impl.UIProgressListener;
import com.zx.uploadlibrary.utils.OKHttpUtils;

import java.io.File;
import java.util.Locale;

import okhttp3.Call;

/**
 * Created by yanix on 2016/11/24.
 * 启动页：检查app更新，有更新就下载，没更新就正常进入app
 */
public class SplashActivity extends BaseActivity {
    private static final int LOGIN = 1;     // 未登录，则去登录
    private static final int MAIN = 2;      // 已登录，进入首页
    private static final int SPLASH_DURATION = 1500;      // 启动页展示时间
    // 获取应用信息的工具类
    @Nullable
    private AppUtils mAppUtils = AppUtils.getInstance();
    // app更新提示对话框
    private CustemConfirmDialog mDialog;
    // apk下载进度条
    private ProgressDialog progressDialog;
    // apk保存路径
    private static final String SAVE = "sdcard/operator.apk";

    @NonNull
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case LOGIN:
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                    break;
                case MAIN:
                    if (!EMClient.getInstance().isLoggedInBefore()) {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                        finish();
                    } else {
                        // 绑定推送别名
                        PushManager.getInstance().bindAlias(getApplicationContext(), "shop_id_" + SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""));
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();
                        overridePendingTransition(R.anim.scale_in, R.anim.scale_out);
                    }
                    break;
            }
        }
    };

    @Override
    protected void beforeContent(Bundle savedInstanceState) {
        super.beforeContent(savedInstanceState);
        // 设置全屏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_splash);
        getUpdateInfo();
    }

    /**
     * 获取app更新信息
     */
    private void getUpdateInfo() {
        mApiService.getUpdateInfo(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                mAppUtils.getVersion(getApplicationContext()), mAppUtils.getVersionCode(getApplicationContext()),
                new GenericCallback<UpdateInfo>(getApplicationContext(), UpdateInfo.class) {

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        // 网络连接有问题，进入首页
                        normalEnterApp();
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        // 出错了，进入首页
                        normalEnterApp();
                    }

                    @Override
                    public void success(@NonNull final UpdateInfo data) {
                        if ("0".equals(data.getUpdate())) {
                            // 没有新版本，进入首页
                            normalEnterApp();
                        } else {
                            // 有更新，弹出更新提示对话框
                            mDialog = new CustemConfirmDialog(SplashActivity.this, R.style.QQStyle, data.getUpdate_mark(), "下载", "0".equals(data.getUpdate_is_mandatory()),
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                            // 下载apk
                                            download(data.getDownloadUrl());
                                        }
                                    },
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                            // 取消下载，进入首页
                                            normalEnterApp();
                                        }
                                    }
                            );
                            mDialog.setCancelable(false);
                            mDialog.setCanceledOnTouchOutside(false);
                            mDialog.show();
                        }
                    }
                });
    }

    /**
     * 进入首页
     */
    private void normalEnterApp() {
        String mobile = (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_MOBILE, "");
        if (TextUtils.isEmpty(mobile)) {
            realEnter();
        } else {
            queryProgress(mobile);
        }
    }

    private void queryProgress(String mobile) {
        mApiService.queryProgress(requestTag, mobile, new GenericCallback<OpenShopModel>(getApplicationContext(), OpenShopModel.class) {

            @Override
            public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                super.onError(call, e, id);
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            }

            @Override
            public void failure() {
                super.failure();
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            }

            @Override
            public void success(OpenShopModel data) {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                intent.putExtra(ShopProgressActivity.KEY_MODEL, data);
                startActivity(intent);
                finish();
            }
        });
    }

    private void realEnter() {
        if (TextUtils.isEmpty((String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""))) {
            handler.sendEmptyMessageDelayed(LOGIN, SPLASH_DURATION);
        } else {
            handler.sendEmptyMessageDelayed(MAIN, SPLASH_DURATION);
        }
    }

    /**
     * 下载apk
     *
     * @param url 下载地址
     */
    protected void download2(String url) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            // 显示进度框
            showProgress();
            // 使用XUtils库下载
            HttpUtils httpUtils = new HttpUtils();
            httpUtils.download(url, SAVE, new RequestCallBack<File>() {
                @Override
                public void onSuccess(ResponseInfo<File> responseInfo) {
                    // 下载完成
                    //1、进度框消失
                    progressDialog.dismiss();
                    try {
                        //2、安装新版本
                        install();
                        //3、退出界面
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(HttpException e, String s) {
                    // 下载失败
                    progressDialog.dismiss();
                    ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_download_fail));
                    // 进入首页
                    normalEnterApp();
                }

                /**
                 * 下载中...
                 * @param total     文件总大小
                 * @param current   当前下载大小
                 * @param isUploding
                 */
                public void onLoading(long total, long current, boolean isUploding) {
                    try {
                        super.onLoading(total, current, isUploding);
                        progressDialog.setMax((int) total);
                        progressDialog.setProgress((int) current);
                        // 格式化下载进度，以MB为单位
                        float all = total / 1024.0f / 1024.0f;
                        float percent = current / 1024.0f / 1024.0f;
                        progressDialog.setProgressNumberFormat(String.format(Locale.CHINA, "%.2fM/%.2fM", percent, all));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            // 没有SDCard
            ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_no_sdcard));
        }
    }

    private void download(String url) {
        //这个是ui线程回调，可直接操作UI
        final UIProgressListener uiProgressResponseListener = new UIProgressListener() {
            @Override
            public void onUIProgress(long bytesRead, long contentLength, boolean done) {
                Log.i("TAG", "bytesRead:" + bytesRead);
                Log.i("TAG", "contentLength:" + contentLength);
                Log.i("TAG", "done:" + done);
                if (contentLength != -1) {
                    //长度未知的情况下回返回-1
                    Log.i("TAG", (100 * bytesRead) / contentLength + "% done");
                }
                Log.i("TAG", "================================");
                progressDialog.setMax((int) contentLength);
                progressDialog.setProgress((int) bytesRead);
                // 格式化下载进度，以MB为单位
                float all = contentLength / 1024.0f / 1024.0f;
                float percent = bytesRead / 1024.0f / 1024.0f;
                progressDialog.setProgressNumberFormat(String.format(Locale.CHINA, "%.2fM/%.2fM", percent, all));
            }

            @Override
            public void onUIStart(long bytesRead, long contentLength, boolean done) {
                super.onUIStart(bytesRead, contentLength, done);
                ToastUtils.showMessage(getApplicationContext(), "开始下载");
            }

            @Override
            public void onUIFinish(long bytesRead, long contentLength, boolean done) {
                super.onUIFinish(bytesRead, contentLength, done);
                progressDialog.dismiss();
                if (!done) {
                    // 下载失败
                    ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_download_fail));
                    // 进入首页
                    normalEnterApp();
                } else {
                    // 下载完成
                    try {
                        // 安装新版本
                        install();
                        // 退出界面
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            // 显示进度框
            showProgress();
            //开启文件下载
            OKHttpUtils.downloadandSaveFile(url, SAVE, uiProgressResponseListener);
        } else {
            // 没有SDCard
            ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_no_sdcard));
        }
    }

    /**
     * 显示进度对话框
     */
    private void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("下载进度");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /**
     * 安装apk
     */
    protected void install() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        // 添加flag
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(Uri.fromFile(new File(SAVE)), "application/vnd.android.package-archive");
        startActivity(intent);
    }
}
