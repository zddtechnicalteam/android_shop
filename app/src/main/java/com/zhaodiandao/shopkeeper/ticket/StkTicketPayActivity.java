/*
 * Copyright (c) 2017.
 * 早点到餐饮管理有限公司 版权所有
 */

package com.zhaodiandao.shopkeeper.ticket;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.account.PaySuccessActivity;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.ProductTicketBean;
import com.zhaodiandao.shopkeeper.model.TicketPayResult;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.pay.Alipay;
import com.zhaodiandao.shopkeeper.pay.GetPrepayIdTask;
import com.zhaodiandao.shopkeeper.pay.PayResult;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/1/12.
 * 支付界面
 */
public class StkTicketPayActivity extends BaseActivity implements View.OnClickListener {
    private static final int SDK_PAY_FLAG = 6;
    public static final String WX = "21";
    public static final String ALIPAY = "20";
    @Nullable
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @Nullable
    @BindView(R.id.ll_menu)
    LinearLayout llMenu;
    @Nullable
    @BindView(R.id.rb_wx)
    RadioButton rbWx;
    @Nullable
    @BindView(R.id.rb_alipay)
    RadioButton rbAlipay;
    private List<ProductTicketBean.ProductBean> datas;

    public final IWXAPI msgApi = WXAPIFactory.createWXAPI(this, null);

    @NonNull
    Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG:
                    PayResult payResult = new PayResult((String) msg.obj);
                    String resultStatus = payResult.getResultStatus();
                    if (TextUtils.equals(resultStatus, "9000")) {
                        ToastUtils.showMessage(getApplicationContext(), "支付成功");
                        EventBus.getDefault().post(new MessageEvent(EventCode.PAY_SUCCESS, "支付成功!"));
                        startActivity(new Intent(StkTicketPayActivity.this, PaySuccessActivity.class));
                    } else {
                        if (TextUtils.equals(resultStatus, "8000")) {
                            ToastUtils.showMessage(getApplicationContext(), "支付结果确认中");
                        } else {
                            ToastUtils.showMessage(getApplicationContext(), "支付失败");
                        }
                    }
                    break;
            }
        }
    };

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_ticket_pay);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.ll_wx).setOnClickListener(this);
        findViewById(R.id.ll_alipay).setOnClickListener(this);

        datas = (List<ProductTicketBean.ProductBean>) getIntent().getSerializableExtra("infos");
        if (datas == null)
            datas = new ArrayList<>();
        initMenu();
    }

    private void initMenu() {
        llMenu.removeAllViews();
        double amount = 0.0;
        for (ProductTicketBean.ProductBean productBean : datas) {
            View v = getLayoutInflater().inflate(R.layout.listitem_ticket, null);
            TextView tvName = (TextView) v.findViewById(R.id.tv_name);
            TextView tvNumber = (TextView) v.findViewById(R.id.tv_number);

            tvName.setText(productBean.getName());
            tvNumber.setText("x" + productBean.getNumber());
            double price = productBean.getPrice();
            int number = productBean.getNumber();
            amount += price * number;
            llMenu.addView(v);
        }
        BigDecimal bigDecimal = new BigDecimal(amount);
        tvAmount.setText("￥" + bigDecimal.setScale(KeyConstants.SCALE, BigDecimal.ROUND_HALF_UP).floatValue());
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.ll_wx:
                rbWx.setChecked(true);
                rbAlipay.setChecked(false);
                break;
            case R.id.ll_alipay:
                rbWx.setChecked(false);
                rbAlipay.setChecked(true);
                break;
        }
    }

    @OnClick(R.id.btn_pay)
    public void pay(View v) {
        JSONArray jsonArray = new JSONArray();
        for (ProductTicketBean.ProductBean data : datas) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pid", data.getPid());
                jsonObject.put("amount", data.getNumber());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        String payType = "";
        if (rbWx.isChecked()) {
            payType = WX;
        } else {
            payType = ALIPAY;
        }
        DialogHelper.showDimDialog(this, "正在提交...");
        submitPay(jsonArray.toString(), payType);
    }

    private void submitPay(String pfks, String payType) {
        mApiService.buyStkTicket(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), payType, pfks,
                new GenericCallback<TicketPayResult>(getApplicationContext(), TicketPayResult.class) {
                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }

                    @Override
                    public void success(@NonNull TicketPayResult data) {
                        if (data.getPay_money() <= 0) {
                            ToastUtils.showMessage(getApplicationContext(), "支付异常");
                            finish();
                        } else {
                            if (rbWx.isChecked()) {
                                BigDecimal bd = new BigDecimal(data.getPay_money() * 100);
                                int money = bd.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
                                GetPrepayIdTask getPrepayId = new GetPrepayIdTask(StkTicketPayActivity.this, data.getOrdernumber() + "_" + money, data.getPay_money(), msgApi, KeyConstants.WEIXIN_NOTIFY_URL4);
                                getPrepayId.execute();
                            } else if (rbAlipay.isChecked()) {
                                Alipay alipay = new Alipay(data.getOrdernumber(), data.getPay_money(), handler, StkTicketPayActivity.this, KeyConstants.ALIPAY_NOTIFY_URL4);
                                alipay.pay();
                            }
                        }
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.REFRESH_PAY)
            finish();
    }
}
