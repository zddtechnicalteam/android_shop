/*
 * Copyright (c) 2017.
 * 早点到餐饮管理有限公司 版权所有
 */

package com.zhaodiandao.shopkeeper.ticket;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.fragment.StkTicketBuyFragment;
import com.zhaodiandao.shopkeeper.fragment.StkTicketUseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yangxu on 2017/1/10.
 */
public class StkTicketDetailActivity extends BaseActivity {
    public static final String KEY_TICKET_ID = "ticket_id";
    public static final String KEY_TITLE = "title";
    private static final int STATUS_USE = 1;
    private static final int STATUS_BUY = 2;
    @Nullable
    @BindView(R.id.viewpager)
    ViewPager mPager;
    @Nullable
    @BindView(R.id.cursor)
    View cursor;
    @Nullable
    @BindView(R.id.rb_use)
    RadioButton rbUse;
    @Nullable
    @BindView(R.id.rb_buy)
    RadioButton rbBuy;
    private RelativeLayout.LayoutParams layoutParams;
    private int bmpW;
    private int screenW;
    private int wholeWidth;
    private List<Fragment> fragmentList;
    private static final int TAB_COUNT = 2;
    private String ticketId;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_ticket_detail);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ticketId = getIntent().getStringExtra(KEY_TICKET_ID);
        setTitle(getIntent().getStringExtra(KEY_TITLE));
        initCursor();
        initTab();
        initViewPager();
    }

    private void initTab() {
        rbUse.setOnClickListener(new MyClickListener(0));
        rbBuy.setOnClickListener(new MyClickListener(1));
        rbUse.setChecked(true);
    }

    public class MyClickListener implements View.OnClickListener {
        private int index = 0;

        public MyClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            mPager.setCurrentItem(index, true);
        }
    }

    private void initCursor() {
        layoutParams = (RelativeLayout.LayoutParams) cursor.getLayoutParams();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        wholeWidth = dm.widthPixels;
        screenW = dm.widthPixels;
        bmpW = screenW / TAB_COUNT;
        layoutParams.width = bmpW;
        cursor.setLayoutParams(layoutParams);
        cursor.setVisibility(View.VISIBLE);
    }

    private void initViewPager() {
        fragmentList = new ArrayList<>();
        StkTicketUseFragment useFragment = new StkTicketUseFragment();
        Bundle bundleUse= new Bundle();
        bundleUse.putInt("status", STATUS_USE);
        bundleUse.putString("ticket_id", ticketId);
        useFragment.setArguments(bundleUse);

        StkTicketBuyFragment buyFragment = new StkTicketBuyFragment();
        Bundle bundleBuy= new Bundle();
        bundleBuy.putInt("status", STATUS_BUY);
        bundleBuy.putString("ticket_id", ticketId);
        buyFragment.setArguments(bundleBuy);

        fragmentList.add(useFragment);
        fragmentList.add(buyFragment);

        //给ViewPager设置适配器
        mPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager()));
        mPager.setOffscreenPageLimit(TAB_COUNT-1);
        mPager.setCurrentItem(0);//设置当前显示标签页为第一页
        mPager.addOnPageChangeListener(new MyOnPageChangeListener());//页面变化时的监听器
    }

    /**
     * 视图加载完成之后执行：值得注意的是PopupWindow每次显示和销毁都会执行一次
     */
    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            tabMove(position, positionOffsetPixels);
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageSelected(int position) {
            tabChange(position);
        }

        private void tabMove(int position, int positionOffsetPixels) {
            int moveI = (int) (bmpW * position + (((double) positionOffsetPixels / wholeWidth) * bmpW));
            layoutParams.leftMargin = moveI;
            cursor.setLayoutParams(layoutParams);
        }

        private void tabChange(int position) {
            if (position == 0) {
                rbUse.setChecked(true);
            } else if (position == 1) {
                rbBuy.setChecked(true);
            }
        }
    }

    class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
