package com.zhaodiandao.shopkeeper.ticket;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.ZDDWebViewActivity;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.config.URLConstants;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.ProductTicketBean;
import com.zhaodiandao.shopkeeper.net.GenericArrayCallback;
import com.zhaodiandao.shopkeeper.util.AnimUtils;
import com.zhaodiandao.shopkeeper.util.DensityUtils;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.util.simpleadpter.CommonAdapter;
import com.zhaodiandao.shopkeeper.util.simpleadpter.ViewHolder;
import com.zhaodiandao.shopkeeper.view.CustemConfirmDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by yanix on 2017/3/27.
 */

public class TicketActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    @Nullable
    @BindView(R.id.ll_content)
    RelativeLayout llContent;
    @Nullable
    @BindView(R.id.pb_loading)
    View pbloading;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView tvFail;
    @Nullable
    @BindView(R.id.lv_product)
    ListView lvProduct;
    @Nullable
    @BindView(R.id.tv_ok)
    TextView tvOk;
    @Nullable
    @BindView(R.id.tv_sum)
    TextView tvSum;
    @Nullable
    @BindView(R.id.tv_count)
    TextView tvCount;
    @Nullable
    @BindView(R.id.ll_cart)
    LinearLayout llCart;
    @Nullable
    @BindView(R.id.iv_cart)
    ImageView ivCart;
    @Nullable
    @BindView(R.id.refresh)
    SwipeRefreshLayout refreshLayout;
    // 辅助购物车显示的view
    @Nullable
    @BindView(R.id.black_view)
    View blackView;

    ImageView imageView;

    @NonNull
    private List<ProductTicketBean> datas = new ArrayList<>();
    // 菜品、临时购物车、最终购物车集合
    @NonNull
    private List<ProductTicketBean.ProductBean> productInfos = new ArrayList<>();
    @NonNull
    private List<ProductTicketBean.ProductBean> cart = new ArrayList<>();
    @NonNull
    private List<ProductTicketBean.ProductBean> realCart = new ArrayList<>();

    // 菜品列表、购物车列表适配器
    private CommonAdapter<ProductTicketBean.ProductBean> productAdapter;
    @Nullable
    private CommonAdapter<ProductTicketBean.ProductBean> cartAdapter;

    private boolean isShowCart = true;
    private PopupWindow popWindow;
    private CustemConfirmDialog dialog;

    private NumberReceiver numberReceiver;
    @Nullable
    private AnimUtils animUtils;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_ticket);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvFail.setOnClickListener(this);
        tvOk.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        llCart.setOnClickListener(this);

        blackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
            }
        });

        animUtils = new AnimUtils(this, ivCart);

        productAdapter = new CommonAdapter<ProductTicketBean.ProductBean>(getApplicationContext(), productInfos, R.layout.listitem_ticket_product) {

            @Override
            public void convert(@NonNull final ViewHolder helper, @NonNull final ProductTicketBean.ProductBean item) {
                ImageView ivProduct = helper.getView(R.id.iv_product);
                String path = item.getImage();
                if (!TextUtils.isEmpty(path)) {
                    Picasso.with(getApplicationContext()).load(item.getImage())
                            .resize(DensityUtils.dp2px(getApplicationContext(), 70), DensityUtils.dp2px(getApplicationContext(), 70))
                            .placeholder(R.mipmap.img_default).into(ivProduct);
                } else {
                    ivProduct.setImageResource(R.mipmap.img_default);
                }

                RelativeLayout rlAdded = helper.getView(R.id.rl_added);
                TextView tvDayLeft = helper.getView(R.id.tv_day_left);

                if (item.getStatus_id() == 1) {
                    tvDayLeft.setVisibility(View.GONE);
                    rlAdded.setVisibility(View.VISIBLE);
                } else {
                    rlAdded.setVisibility(View.GONE);
                    tvDayLeft.setVisibility(View.VISIBLE);
                    tvDayLeft.setText(item.getStatus_label());
                }

                helper.setText(R.id.tv_name, item.getName());
                helper.setText(R.id.tv_format, "起购量：" + item.getFormat());
                helper.setText(R.id.tv_detail, item.getDetail());
                helper.setText(R.id.tv_price, "￥" + item.getPrice() + "/" + item.getUnit());

                TextView tvOriginPrice = helper.getView(R.id.tv_origin_price);
                tvOriginPrice.setText("原价￥" + item.getOrg_price());
                tvOriginPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG); //中划线

                helper.setText(R.id.tv_sell, "剩余" + item.getSold_percentage() + "张");
                //ProgressBar progressBar = helper.getView(R.id.my_progress);
                //progressBar.setProgress(item.getSold_percentage());

                final TextView tvCount = helper.getView(R.id.tv_count);
                final ImageView ivAnimReduce = helper.getView(R.id.iv_anim_reduce);
                final ImageView ivAdd = helper.getView(R.id.iv_add);

                final ImageView ivReduce = helper.getView(R.id.iv_reduce);
                ivReduce.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int number = item.getNumber();
                        int format = Integer.parseInt(item.getFormat());
                        int amount = 1;
                        if (number > format) {
                            item.setNumber(number - 1);
                        } else {
                            item.setNumber(0);
                            amount = format;
                        }

                        if (item.getNumber() == 0) {
                            ivReduce.setEnabled(false);
                            hide(ivReduce, ivAnimReduce);
                        }

                        Intent intent = new Intent(KeyConstants.ACTION_CHANGE_TICKET);
                        intent.putExtra("info", item);
                        intent.putExtra("type", KeyConstants.REDUCE);
                        intent.putExtra("amount", amount);
                        sendBroadcast(intent);
                    }
                });

                if (item.getNumber() > 0) {
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(item.getNumber() + "");
                    ivReduce.setVisibility(View.VISIBLE);
                } else {
                    tvCount.setVisibility(View.INVISIBLE);
                    ivReduce.setVisibility(View.INVISIBLE);
                }

                ivAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(@NonNull View v) {
                        int number = item.getNumber();
                        int format = Integer.parseInt(item.getFormat());
                        int amount = 1;
                        if (number >= format) {
                            item.setNumber(number + 1);
                        } else {
                            item.setNumber(format);
                            amount = format;
                        }

                        if (item.getNumber() == 1) {
                            ivAdd.setEnabled(false);
                            show(ivAnimReduce, ivAdd, ivReduce, item, amount);
                        } else {
                            Intent intent = new Intent(KeyConstants.ACTION_CHANGE_TICKET);
                            intent.putExtra("info", item);
                            intent.putExtra("type", KeyConstants.ADD);
                            intent.putExtra("amount", amount);
                            sendBroadcast(intent);
                        }

                        int[] start_location = new int[2];// 一个整型数组，用来存储按钮的在屏幕的X、Y坐标
                        v.getLocationInWindow(start_location);// 这是获取购买按钮的在屏幕的X、Y坐标（这也是动画开始的坐标）
                        imageView = new ImageView(getApplicationContext());
                        imageView.setImageBitmap(animUtils.getAddDrawBitMap());// 设置buyImg的图片
                        animUtils.setAnim(imageView, start_location, null);// 开始执行动画
                    }
                });
            }
        };
        lvProduct.setAdapter(productAdapter);

        registerReceiver();
        getProduct();
    }

    private void show(@NonNull View v, @NonNull final View clickView, @NonNull final View endView, final ProductTicketBean.ProductBean item, final int amount) {
        int[] startPos = new int[2];
        v.getLocationOnScreen(startPos);

        final int[] endPos = new int[2];
        endView.getLocationOnScreen(endPos);

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setDuration(300);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, endPos[0] - startPos[0], 0, 0);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        RotateAnimation rotateAnimation = new RotateAnimation(0, -720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(300);
        //animationSet.addAnimation(rotateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        v.startAnimation(animationSet);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                endView.setVisibility(View.VISIBLE);

                Intent intent = new Intent(KeyConstants.ACTION_CHANGE_TICKET);
                intent.putExtra("info", item);
                intent.putExtra("type", KeyConstants.ADD);
                intent.putExtra("amount", amount);
                sendBroadcast(intent);

                if (cartAdapter != null) {
                    cartAdapter.notifyDataSetChanged();
                }

                clickView.setEnabled(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void hide(@NonNull final View v, @NonNull View endView) {
        final int[] startPos = new int[2];
        v.getLocationOnScreen(startPos);

        final int[] endPos = new int[2];
        endView.getLocationOnScreen(endPos);

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setDuration(300);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, endPos[0] - startPos[0], 0, 0);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        RotateAnimation rotateAnimation = new RotateAnimation(0, 720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(300);
        //animationSet.addAnimation(rotateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        v.startAnimation(animationSet);

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setEnabled(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void registerReceiver() {
        // 注册广播接收
        numberReceiver = new NumberReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(KeyConstants.ACTION_CHANGE_TICKET);
        filter.addAction(KeyConstants.ACTION_CART_CLEAR_TICKET);
        filter.addAction(KeyConstants.ACTION_REFRESH_TICKET);
        registerReceiver(numberReceiver, filter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_question, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.question:
                Intent intent = new Intent(this, ZDDWebViewActivity.class);
                intent.putExtra(ZDDWebViewActivity.TITLE, "提购劵说明");
                intent.putExtra(ZDDWebViewActivity.URL, URLConstants.URL_TICKET_DESC);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getProduct();
        clearCart();
    }

    private int totalCount = 0;
    private float sumMoney = 0.0f;

    private class NumberReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, @NonNull Intent intent) {
            String action = intent.getAction();
            if (action.equals(KeyConstants.ACTION_CHANGE_TICKET)) { // 改变数量
                productAdapter.notifyDataSetChanged();
                ProductTicketBean.ProductBean info = (ProductTicketBean.ProductBean) intent.getSerializableExtra("info");
                int type = intent.getIntExtra("type", 0);
                int amount = intent.getIntExtra("amount", 0);

                boolean flag = true;
                for (ProductTicketBean.ProductBean cartInfo : cart) {
                    if (cartInfo.getPid().equals(info.getPid())) {
                        cartInfo.setNumber(info.getNumber());
                        flag = false;
                        break;
                    }
                }

                if (flag) {
                    cart.add(info);
                }

                totalCount = Integer.parseInt(tvCount.getText().toString());
                sumMoney = Float.parseFloat(tvSum.getText().toString());

                if (type == KeyConstants.ADD) {
                    totalCount += amount;
                    sumMoney += info.getPrice() * amount;
                } else {
                    totalCount -= amount;
                    sumMoney -= info.getPrice() * amount;
                }

                tvCount.setTextSize(12);
                if (totalCount > 0) {
                    if (totalCount > 999) {
                        tvCount.setTextSize(6);
                    } else if (totalCount > 99) {
                        tvCount.setTextSize(8);
                    } else if (totalCount > 9) {
                        tvCount.setTextSize(10);
                    }
                    tvOk.setEnabled(true);
                    tvCount.setText(totalCount + "");
                    tvCount.setVisibility(View.VISIBLE);
                    BigDecimal bigDecimal = new BigDecimal(sumMoney);
                    tvSum.setText("" + bigDecimal.setScale(KeyConstants.SCALE, BigDecimal.ROUND_HALF_UP).floatValue());
                } else {
                    tvOk.setEnabled(false);
                    tvCount.setText("0");
                    tvCount.setVisibility(View.GONE);
                    tvSum.setText("0");
                }
            } else if (action.equals(KeyConstants.ACTION_CART_CLEAR_TICKET)) {     // 清空
                tvSum.setText("0");
                tvCount.setText("0");
                tvCount.setVisibility(View.GONE);
                tvOk.setEnabled(false);
                totalCount = 0;
                sumMoney = 0.0f;
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
                cart.clear();

                for (ProductTicketBean productClassInfo : datas) {
                    for (ProductTicketBean.ProductBean info : productClassInfo.getProducts()) {
                        info.setNumber(0);
                    }
                }
                productAdapter.notifyDataSetChanged();
            } else if (action.equals(KeyConstants.ACTION_REFRESH_TICKET)) {
                getProduct();
                clearCart();
            }
        }
    }


    /**
     * 获取菜品数据
     */
    private void getProduct() {
        mApiService.getTicketList(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericArrayCallback<ProductTicketBean>(getApplicationContext(), ProductTicketBean.class) {

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() == View.GONE) {
                            AnimUtils.crossfade(tvFail, pbloading);
                        }
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() == View.GONE) {
                            AnimUtils.crossfade(tvFail, pbloading);
                        }
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (refreshLayout.isRefreshing()) {
                            refreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void success(@NonNull List<ProductTicketBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        if (data.size() <= 0) {
                            ToastUtils.showMessage(getApplicationContext(), "沒有数据");
                            productInfos.clear();
                            productAdapter.notifyDataSetChanged();
                            if (pbloading.getVisibility() == View.VISIBLE || tvFail.getVisibility() == View.VISIBLE) {
                                AnimUtils.visible(llContent);
                            } else {
                                llContent.setVisibility(View.VISIBLE);
                            }
                            AnimUtils.gone(pbloading);
                            AnimUtils.gone(tvFail);
                            return;
                        }

                        productInfos.clear();
                        for (ProductTicketBean productClassBean : data) {
                            productInfos.addAll(productClassBean.getProducts());
                        }
                        productAdapter.notifyDataSetChanged();

                        if (pbloading.getVisibility() == View.VISIBLE || tvFail.getVisibility() == View.VISIBLE) {
                            AnimUtils.visible(llContent);
                        } else {
                            llContent.setVisibility(View.VISIBLE);
                        }
                        AnimUtils.gone(pbloading);
                        AnimUtils.gone(tvFail);
                    }
                });
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.tv_retry:
                pbloading.setVisibility(View.VISIBLE);
                AnimUtils.gone(tvFail);
                getProduct();
                break;
            case R.id.iv_cart:
            case R.id.ll_cart:
                getCart();
                if (isShowCart) {
                    // 判断购物车是否为空
                    if (isCartEmpty()) {
                        ToastUtils.showMessage(getApplicationContext(), getString(R.string.cart_empty));
                    } else {
                        showPop(ivCart);
                        isShowCart = false;
                    }
                } else {
                    if (popWindow != null && popWindow.isShowing()) {
                        popWindow.dismiss();
                        blackView.setVisibility(View.GONE);
                        isShowCart = true;
                    }
                }
                break;
            case R.id.tv_ok:  // 提 交
                getCart();
                Intent intent = new Intent(getApplicationContext(), TicketPayActivity.class);
                intent.putExtra("infos", (Serializable) realCart);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (popWindow != null && popWindow.isShowing()) {
            popWindow.dismiss();
            blackView.setVisibility(View.GONE);
            isShowCart = true;
        }
    }

    /**
     * 显示购物车的详情
     */
    private void showPop(@NonNull View temp) {
        View v = getLayoutInflater().inflate(R.layout.cart_popwindow, null);
        LinearLayout llCart = (LinearLayout) v.findViewById(R.id.ll_cart);
        int[] location = new int[2];
        temp.getLocationOnScreen(location);
        popWindow = new PopupWindow(v, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ListView menuList = (ListView) v.findViewById(R.id.card_food_list);
        cartAdapter = new CommonAdapter<ProductTicketBean.ProductBean>(getApplicationContext(), realCart, R.layout.listitem_cart) {

            @Override
            public void convert(@NonNull ViewHolder helper, @NonNull final ProductTicketBean.ProductBean item) {
                helper.setText(R.id.tv_format, "起购量：" + item.getFormat());
                helper.setText(R.id.tv_price, "￥" + item.getPrice() + "/" + item.getUnit());
                helper.setText(R.id.tv_name, item.getName());

                final TextView tvCount = helper.getView(R.id.tv_count);
                final ImageView ivAnimReduce = helper.getView(R.id.iv_anim_reduce);
                final ImageView ivAdd = helper.getView(R.id.iv_add);

                final ImageView ivReduce = helper.getView(R.id.iv_reduce);
                ivReduce.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (ProductTicketBean productClassInfo : datas) {
                            for (ProductTicketBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                if (item.getPid().equals(productInfo.getPid())) {
                                    int number = productInfo.getNumber();
                                    if (number > Integer.parseInt(productInfo.getFormat())) {
                                        productInfo.setNumber(number - 1);
                                    } else {
                                        productInfo.setNumber(0);
                                    }
                                }
                            }
                        }

                        int number = item.getNumber();
                        int format = Integer.parseInt(item.getFormat());
                        int amount = 1;
                        if (number > format) {
                            item.setNumber(number - 1);
                        } else {
                            item.setNumber(0);
                            amount = format;
                        }

                        if (item.getNumber() == 0) {
                            ivReduce.setEnabled(false);
                            hide(ivReduce, ivAnimReduce);
                        }

                        Intent intent = new Intent(KeyConstants.ACTION_CHANGE_TICKET);
                        intent.putExtra("info", item);
                        intent.putExtra("type", KeyConstants.REDUCE);
                        intent.putExtra("amount", amount);
                        sendBroadcast(intent);

                        cartAdapter.notifyDataSetChanged();
                    }
                });

                if (item.getNumber() > 0) {
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(item.getNumber() + "");
                    ivReduce.setVisibility(View.VISIBLE);
                } else {
                    tvCount.setVisibility(View.INVISIBLE);
                    ivReduce.setVisibility(View.INVISIBLE);
                }

                ivAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (ProductTicketBean productClassInfo : datas) {
                            for (ProductTicketBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                if (item.getPid().equals(productInfo.getPid())) {
                                    int number = productInfo.getNumber();
                                    int format = Integer.parseInt(productInfo.getFormat());
                                    if (number >= format) {
                                        productInfo.setNumber(number + 1);
                                    } else {
                                        productInfo.setNumber(format);
                                    }
                                }
                            }
                        }

                        int number = item.getNumber();
                        int format = Integer.parseInt(item.getFormat());
                        int amount = 1;
                        if (number >= format) {
                            item.setNumber(number + 1);
                        } else {
                            item.setNumber(format);
                            amount = format;
                        }

                        if (item.getNumber() == 1) {
                            ivAdd.setEnabled(false);
                            show(ivAnimReduce, ivAdd, ivReduce, item, amount);
                        } else {
                            Intent intent = new Intent(KeyConstants.ACTION_CHANGE_TICKET);
                            intent.putExtra("info", item);
                            intent.putExtra("type", KeyConstants.ADD);
                            intent.putExtra("amount", amount);
                            sendBroadcast(intent);
                            cartAdapter.notifyDataSetInvalidated();
                        }
                    }
                });
            }
        };
        menuList.setAdapter(cartAdapter);
        popWindow.showAtLocation(temp, Gravity.BOTTOM, 0, DensityUtils.dp2px(getApplicationContext(), 40));
        blackView.setVisibility(View.VISIBLE);

        llCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
            }
        });

        v.findViewById(R.id.tv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog == null) {
                    dialog = new CustemConfirmDialog(TicketActivity.this, R.style.QQStyle, "确定清空购物车吗？"
                            , "清空",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    clearCart();
                                    dialog.dismiss();
                                }
                            });
                }
                dialog.show();
            }
        });
    }

    /**
     * 获取购物车的内容
     */
    public void getCart() {
        realCart.clear();
        for (ProductTicketBean.ProductBean info : cart) {
            if (info.getNumber() > 0) {
                realCart.add(info);
            }
        }
    }

    /**
     * 判断购物车是否为空
     */
    public boolean isCartEmpty() {
        return realCart.size() <= 0;
    }

    /**
     * 清空购物车
     */
    private void clearCart() {
        // 清空列表
        Intent intent = new Intent();
        intent.setAction(KeyConstants.ACTION_CART_CLEAR_TICKET);
        sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cartAdapter = null;
        EventBus.getDefault().unregister(this);
        unregisterReceiver(numberReceiver);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.REFRESH_PAY) {
            getProduct();
            clearCart();
        }
    }
}
