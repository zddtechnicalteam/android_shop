package com.zhaodiandao.shopkeeper.ticket;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.ZDDWebViewActivity;
import com.zhaodiandao.shopkeeper.adapter.MyTicketAdapter;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.config.URLConstants;
import com.zhaodiandao.shopkeeper.model.MyTicket;
import com.zhaodiandao.shopkeeper.net.GenericArrayCallback;
import com.zhaodiandao.shopkeeper.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2017/3/28.
 * 我的提货券
 */
public class MyTicketActivity extends BaseActivity {
    @Nullable
    @BindView(R.id.pb_loading)
    View mProgressBar;
    @Nullable
    @BindView(R.id.ll_content)
    LinearLayout mLayoutContent;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @Nullable
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private MyTicketAdapter mAdapter;
    @NonNull
    private List<MyTicket> datas = new ArrayList<>();

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_my_ticket);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MyTicketAdapter(getApplicationContext(), datas);
        mAdapter.setOnItemClickListener(new MyTicketAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, @NonNull MyTicket data) {
                Intent intent = new Intent(MyTicketActivity.this, TicketDetailActivity.class);
                intent.putExtra(TicketDetailActivity.KEY_TICKET_ID, data.getPft_id());
                intent.putExtra(TicketDetailActivity.KEY_TITLE, data.getPft_name());
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_question, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.question:
                Intent intent = new Intent(this, ZDDWebViewActivity.class);
                intent.putExtra(ZDDWebViewActivity.TITLE, "提购劵说明");
                intent.putExtra(ZDDWebViewActivity.URL, URLConstants.URL_TICKET_DESC);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getData() {
        // 获取数据
        mApiService.getMyTicket(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericArrayCallback<MyTicket>(getApplicationContext(), MyTicket.class) {

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(@NonNull List<MyTicket> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }
}
