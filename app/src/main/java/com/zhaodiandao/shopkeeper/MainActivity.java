package com.zhaodiandao.shopkeeper;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.igexin.sdk.PushManager;
import com.socks.library.KLog;
import com.squareup.picasso.Picasso;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.fragment.BrandFragment;
import com.zhaodiandao.shopkeeper.fragment.CollegeFragment;
import com.zhaodiandao.shopkeeper.fragment.MeFragment;
import com.zhaodiandao.shopkeeper.fragment.OrderFragment;
import com.zhaodiandao.shopkeeper.model.NewProductBean;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.service.DemoIntentService;
import com.zhaodiandao.shopkeeper.service.DemoPushService;
import com.zhaodiandao.shopkeeper.util.DensityUtils;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private static final int REQUEST_PERMISSION = 0;
    private static final int CACHED_FRAGMENT_COUNT = 3;     // 缓存的fragment数量
    // 记录第一次按后退键的时间，实现按两次退出app的功能
    private long mExitTime = 0;

    @Nullable
    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    // 保存Fragment的集合
    @NonNull
    private List<Fragment> mTabs = new ArrayList<Fragment>();
    private FragmentPagerAdapter mAdapter;

    @Nullable
    @BindView(R.id.rb_order)
    RadioButton rbOrder;
    @Nullable
    @BindView(R.id.rb_brand)
    RadioButton rbBrand;
    @Nullable
    @BindView(R.id.rb_college)
    RadioButton rbCollege;
    @Nullable
    @BindView(R.id.rb_me)
    RadioButton rbMe;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);

        PackageManager pkgManager = getPackageManager();

        // 读写 sd card 权限非常重要, android6.0默认禁止的, 建议初始化之前就弹窗让用户赋予该权限
        boolean sdCardWritePermission =
                pkgManager.checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getPackageName()) == PackageManager.PERMISSION_GRANTED;

        // read phone state用于获取 imei 设备信息
        boolean phoneSatePermission =
                pkgManager.checkPermission(android.Manifest.permission.READ_PHONE_STATE, getPackageName()) == PackageManager.PERMISSION_GRANTED;

        if (Build.VERSION.SDK_INT >= 23 && !sdCardWritePermission || !phoneSatePermission) {
            requestPermission();
        } else {
            PushManager.getInstance().initialize(this.getApplicationContext(), DemoPushService.class);
        }

        // 注册 intentService 后 PushDemoReceiver 无效, sdk 会使用 DemoIntentService 传递数据,
        // AndroidManifest 对应保留一个即可(如果注册 DemoIntentService, 可以去掉 PushDemoReceiver, 如果注册了
        // IntentService, 必须在 AndroidManifest 中声明)
        PushManager.getInstance().registerPushIntentService(this.getApplicationContext(), DemoIntentService.class);

        ButterKnife.bind(this);
        // 初始化视图和事件
        initViewAndEvent();
        initAdapter();
        checkNewProduct();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_PHONE_STATE},
                REQUEST_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            if ((grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                PushManager.getInstance().initialize(this.getApplicationContext(), DemoPushService.class);
            } else {
                KLog.e("We highly recommend that you need to grant the special permissions before initializing the SDK, otherwise some "
                        + "functions will not work");
                PushManager.getInstance().initialize(this.getApplicationContext(), DemoPushService.class);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void initViewAndEvent() {
        mViewPager.setOffscreenPageLimit(CACHED_FRAGMENT_COUNT);
        mViewPager.addOnPageChangeListener(this);
        findViewById(R.id.rl_order).setOnClickListener(this);
        findViewById(R.id.rl_brand).setOnClickListener(this);
        findViewById(R.id.rl_college).setOnClickListener(this);
        findViewById(R.id.rl_me).setOnClickListener(this);
    }

    private void initAdapter() {
        OrderFragment orderFragment = new OrderFragment();
        BrandFragment brandFragment = new BrandFragment();
        CollegeFragment collegeFragment = new CollegeFragment();
        MeFragment meFragment = new MeFragment();

        mTabs.add(orderFragment);
        mTabs.add(brandFragment);
        mTabs.add(collegeFragment);
        mTabs.add(meFragment);

        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Override
            public int getCount() {
                return mTabs.size();
            }

            @Override
            public Fragment getItem(int position) {
                return mTabs.get(position);
            }
        };

        mViewPager.setAdapter(mAdapter);
        changeColor(0);
        mViewPager.setCurrentItem(0, false);
    }

    /**
     * 修改图标状态和标题
     *
     * @param position 要高亮的图标位置
     */
    private void changeColor(int position) {
        if (position == 0) {
            rbOrder.setChecked(true);
            rbBrand.setChecked(false);
            rbCollege.setChecked(false);
            rbMe.setChecked(false);
            setTitle("订货");
        } else if (position == 1) {
            rbOrder.setChecked(false);
            rbBrand.setChecked(true);
            rbCollege.setChecked(false);
            rbMe.setChecked(false);
            setTitle("品牌加盟");
        } else if (position == 2) {
            rbOrder.setChecked(false);
            rbBrand.setChecked(false);
            rbCollege.setChecked(true);
            rbMe.setChecked(false);
            setTitle("学院");
        } else if (position == 3) {
            rbOrder.setChecked(false);
            rbBrand.setChecked(false);
            rbCollege.setChecked(false);
            rbMe.setChecked(true);
            setTitle("我的");
        }
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.rl_order:
                changeColor(0);
                mViewPager.setCurrentItem(0, false);
                break;
            case R.id.rl_brand:
                changeColor(1);
                mViewPager.setCurrentItem(1, false);
                break;
            case R.id.rl_college:
                changeColor(2);
                mViewPager.setCurrentItem(2, false);
                break;
            case R.id.rl_me:
                changeColor(3);
                mViewPager.setCurrentItem(3, false);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        changeColor(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                ToastUtils.showMessage(getApplicationContext(), "再按一次退出" + getString(R.string.app_name));
                mExitTime = System.currentTimeMillis();
            } else {
                finish();
                overridePendingTransition(R.anim.close_zoom_enter, R.anim.close_zoom_exit);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 检查新产品
     */
    private void checkNewProduct() {
        mApiService.checkNewProduct(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericCallback<NewProductBean>(getApplicationContext(), NewProductBean.class) {
                    @Override
                    public void success(@NonNull NewProductBean data) {
                        if (!TextUtils.isEmpty(data.getPid())){
                            final Dialog dialog = new Dialog(MainActivity.this, R.style.QQStyle);
                            View v = getLayoutInflater().inflate(R.layout.dialog_new_product, null);
                            dialog.setContentView(v);
                            ImageView ivProduct = (ImageView) v.findViewById(R.id.iv_product);
                            TextView tvName = (TextView) v.findViewById(R.id.tv_name);
                            TextView tvFormat = (TextView) v.findViewById(R.id.tv_format);
                            TextView tvPrice = (TextView) v.findViewById(R.id.tv_price);
                            TextView tvDetail = (TextView) v.findViewById(R.id.tv_detail);
                            TextView tvMenuType = (TextView) v.findViewById(R.id.tv_menutype);
                            if (!TextUtils.isEmpty(data.getImage())) {
                                Picasso.with(getApplicationContext()).load(data.getImage())
                                        .resize(DensityUtils.dp2px(getApplicationContext(), 70), DensityUtils.dp2px(getApplicationContext(), 70))
                                        .centerCrop()
                                        .placeholder(R.mipmap.img_default)
                                        .into(ivProduct);
                            }
                            tvName.setText(data.getName());
                            tvFormat.setText(data.getFormat());
                            tvPrice.setText(data.getPrice() + "/" + data.getUnit());
                            tvDetail.setText(data.getDetail());
                            tvMenuType.setText(data.getMenutype());
                            v.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            v.findViewById(R.id.tv_ok).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        }
                    }
                });
    }
}
