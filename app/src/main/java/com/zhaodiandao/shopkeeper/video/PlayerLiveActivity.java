/*
 * Copyright (c) 2017.
 * 早点到餐饮管理有限公司 版权所有
 */

package com.zhaodiandao.shopkeeper.video;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.dou361.ijkplayer.widget.PlayStateParams;
import com.dou361.ijkplayer.widget.PlayerView;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.adapter.ChatAdapter;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.MessageBean;
import com.zhaodiandao.shopkeeper.net.ApiService;
import com.zhaodiandao.shopkeeper.net.ZddStringCallback;
import com.zhaodiandao.shopkeeper.util.MediaUtils;
import com.zhaodiandao.shopkeeper.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlayerLiveActivity extends Activity {
    public static final String KEY_URL = "key_url";
    public static final String KEY_TITLE = "key_title";
    public static final String KEY_ROOM_ID = "key_room_id";
    public static final String KEY_LIVE_ID = "key_live_id";
    private PlayerView player;
    private View rootView;
    private String url;
    private String title;
    private String roomId;
    private String liveId;
    private PowerManager.WakeLock wakeLock;

    private ChatAdapter mAdapter;
    @NonNull
    private List<MessageBean> datas = new ArrayList<>();
    @Nullable
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Nullable
    @BindView(R.id.et_content)
    EditText etContent;
    @Nullable
    private String nickname;
    @NonNull
    private String requestTag = "PlayerLiveActivity_post";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 设置全屏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        rootView = getLayoutInflater().from(this).inflate(R.layout.simple_player_view_player, null);
        setContentView(rootView);

        ButterKnife.bind(this);

        mAdapter = new ChatAdapter(getApplicationContext(), datas);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerView.setAdapter(mAdapter);

        /**常亮*/
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "liveTAG");
        wakeLock.acquire();
        
        url = getIntent().getStringExtra(KEY_URL);
        title = getIntent().getStringExtra(KEY_TITLE);
        roomId = getIntent().getStringExtra(KEY_ROOM_ID);
        liveId = getIntent().getStringExtra(KEY_LIVE_ID);

        player = new PlayerView(this, rootView)
                .setTitle(title)
                .setScaleType(PlayStateParams.fitparent)
                .hideMenu(true)
                .hideSteam(true)
                .setForbidDoulbeUp(true)
                .hideCenterPlayer(true)
                .hideControlPanl(true);
        player.setPlaySource(url).startPlay();

        EMClient.getInstance().chatManager().addMessageListener(msgListener);
        nickname = (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_NAME, "");
    }

    @Override
    protected void onStart() {
        super.onStart();
        // 在线：1
        ApiService.getInstance().uploadLiveStatus(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                liveId, "1", new ZddStringCallback(getApplicationContext(), "message") {
                    @Override
                    public void success(String response) {

                    }
                });
    }

    @Override
    protected void onStop() {
        // 离线：2
        ApiService.getInstance().uploadLiveStatus(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                liveId, "2", new ZddStringCallback(getApplicationContext(), "message") {
                    @Override
                    public void success(String response) {

                    }
                });
        super.onStop();
    }

    @NonNull
    EMMessageListener msgListener = new EMMessageListener() {

        @Override
        public void onMessageReceived(@NonNull List<EMMessage> messages) {
            //收到消息
            for (EMMessage message : messages) {
                MessageBean messageBean = new MessageBean(message.getFrom(), ((EMTextMessageBody) message.getBody()).getMessage());
                datas.add(datas.size(), messageBean);
            }
            //<editor-fold desc="自定义代码块">
            PlayerLiveActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyDataSetChanged();
                    mRecyclerView.scrollToPosition(datas.size() - 1);
                }
            });
            //</editor-fold>
        }

        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {
            //收到透传消息
        }

        @Override
        public void onMessageRead(List<EMMessage> messages) {
            //收到已读回执
        }

        @Override
        public void onMessageDelivered(List<EMMessage> message) {
            //收到已送达回执
        }

        @Override
        public void onMessageChanged(EMMessage message, Object change) {
            //消息状态变动
        }
    };

    @OnClick(R.id.tv_send)
    public void send(View v) {
        String content = etContent.getText().toString().trim();
        if (TextUtils.isEmpty(content)) {
            return;
        }

        MessageBean messageBean = new MessageBean(nickname, content);
        datas.add(datas.size(), messageBean);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.scrollToPosition(datas.size() - 1);
        etContent.setText("");

        //创建一条文本消息，content为消息文字内容，toChatUsername为对方用户或者群聊的id，后文皆是如此
        EMMessage message = EMMessage.createTxtSendMessage(content, roomId);
        message.setChatType(EMMessage.ChatType.ChatRoom);
        message.setFrom(nickname);
        //发送消息
        EMClient.getInstance().chatManager().sendMessage(message);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (player != null) {
            player.onPause();
        }
        MediaUtils.muteAudioFocus(this, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (player != null) {
            player.onResume();
        }
        MediaUtils.muteAudioFocus(this, false);
        if (wakeLock != null) {
            wakeLock.acquire();
        }
    }

    @OnClick(R.id.iv_close)
    public void close(View v) {
        finish();
    }

    @Override
    protected void onDestroy() {
        EMClient.getInstance().chatManager().removeMessageListener(msgListener);
        EMClient.getInstance().chatroomManager().leaveChatRoom(roomId);

        if (player != null) {
            player.onDestroy();
        }
        super.onDestroy();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (player != null) {
            player.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void onBackPressed() {
        if (player != null && player.onBackPressed()) {
            return;
        }
        super.onBackPressed();
        if (wakeLock != null) {
            wakeLock.release();
        }
    }

}
