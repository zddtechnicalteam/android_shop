package com.zhaodiandao.shopkeeper.account;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.RechargeModel;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.pay.Alipay;
import com.zhaodiandao.shopkeeper.pay.GetPrepayIdTask;
import com.zhaodiandao.shopkeeper.pay.PayResult;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Author： 杨旭 on 2016/5/19 14:42
 * Contact： 444386433@qq.com
 */
public class RechargeActivity extends BaseActivity implements View.OnClickListener {
    private static final int SDK_PAY_FLAG = 6;
    @Nullable
    @BindView(R.id.rb_union)
    RadioButton rbUnion;
    @Nullable
    @BindView(R.id.rb_wx)
    RadioButton rbWx;
    @Nullable
    @BindView(R.id.rb_alipay)
    RadioButton rbAlipay;
    @Nullable
    @BindView(R.id.et_money)
    EditText etMoney;

    private float amountPay;
    public final IWXAPI msgApi = WXAPIFactory.createWXAPI(this, null);

    @NonNull
    Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG:
                    PayResult payResult = new PayResult((String) msg.obj);
                    String resultStatus = payResult.getResultStatus();
                    if (TextUtils.equals(resultStatus, "9000")) {
                        ToastUtils.showMessage(getApplicationContext(), "支付成功");
                        EventBus.getDefault().post(new MessageEvent(EventCode.PAY_SUCCESS, "支付成功!"));
                        startActivity(new Intent(RechargeActivity.this, PaySuccessActivity.class));
                    } else {
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(getApplicationContext(), "支付结果确认中", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "支付失败", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
            }
        }
    };

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_recharge);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViewById(R.id.btn_recharge).setOnClickListener(this);
        findViewById(R.id.ll_wx).setOnClickListener(this);
        findViewById(R.id.ll_alipay).setOnClickListener(this);
        findViewById(R.id.ll_union).setOnClickListener(this);

        msgApi.registerApp(KeyConstants.WEIXIN_APP_ID);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.btn_recharge:
                String text = etMoney.getText().toString().trim();
                if (TextUtils.isEmpty(text)) {
                    ToastUtils.showMessage(getApplicationContext(), "请输入有效金额");
                    return;
                }
                try {
                    int amount = Integer.parseInt(text);
                    if (amount <= 0) {
                        ToastUtils.showMessage(getApplicationContext(), "金额必须大于0");
                        return;
                    }
                    amountPay = amount;
                } catch (Exception e) {
                    ToastUtils.showMessage(getApplicationContext(), "请输入有效金额");
                    return;
                }

                DialogHelper.showDimDialog(this, "努力请求中...");

                if (rbWx.isChecked()) {
                    getOrderNumber(amountPay, "4");
                } else if (rbAlipay.isChecked()) {
                    getOrderNumber(amountPay, "5");
                } else if (rbUnion.isChecked()) {
                    getOrderNumber(amountPay, "6");
                } else {
                    DialogHelper.dismissDialog();
                    ToastUtils.showMessage(getApplicationContext(), "请选择支付方式...");
                }
                break;
            case R.id.ll_union:
                rbUnion.setChecked(true);
                rbWx.setChecked(false);
                rbAlipay.setChecked(false);
                break;
            case R.id.ll_wx:
                rbUnion.setChecked(false);
                rbWx.setChecked(true);
                rbAlipay.setChecked(false);
                break;
            case R.id.ll_alipay:
                rbUnion.setChecked(false);
                rbWx.setChecked(false);
                rbAlipay.setChecked(true);
                break;
        }
    }

    /**
     * 获取订单号
     */
    private void getOrderNumber(float money, String type) {
        mApiService.recharge(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), type, money + "",
                new GenericCallback<RechargeModel>(getApplicationContext(), RechargeModel.class) {

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }

                    @Override
                    public void success(@NonNull RechargeModel data) {
                        if (rbWx.isChecked()) {
                            GetPrepayIdTask getPrepayId = new GetPrepayIdTask(RechargeActivity.this, data.getOrdernumber(), amountPay, msgApi, KeyConstants.WEIXIN_NOTIFY_URL);
                            getPrepayId.execute();
                        } else if (rbAlipay.isChecked()) {
                            Alipay alipay = new Alipay(data.getOrdernumber(), amountPay, handler, RechargeActivity.this, KeyConstants.ALIPAY_NOTIFY_URL);
                            alipay.pay();
                        } else if (rbUnion.isChecked()) {
                            EventBus.getDefault().post(new MessageEvent(EventCode.PAY_SUCCESS, "支付成功!"));
                            Intent intent = new Intent(RechargeActivity.this, BankRemarkActivity.class);
                            intent.putExtra("remark", data.getMark());
                            intent.putExtra("money", amountPay + "");
                            startActivity(intent);
                        }
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.REFRESH_PAY)
            finish();
    }
}
