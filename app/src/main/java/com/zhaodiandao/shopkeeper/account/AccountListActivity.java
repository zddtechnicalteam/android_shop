package com.zhaodiandao.shopkeeper.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.fragment.AccountStatusFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/1/11.
 */
public class AccountListActivity extends BaseActivity {
    private static final int STATUS_WHOLE = 1;
    private static final int STATUS_INCOME = 2;
    private static final int STATUS_PAY = 3;
    @Nullable
    @BindView(R.id.viewpager)
    ViewPager mPager;
    @Nullable
    @BindView(R.id.cursor)
    View cursor;
    @Nullable
    @BindView(R.id.rb_whole)
    RadioButton rbWhole;
    @Nullable
    @BindView(R.id.rb_income)
    RadioButton rbIncome;
    @Nullable
    @BindView(R.id.rb_pay)
    RadioButton rbPay;
    @Nullable
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @Nullable
    @BindView(R.id.tv_money)
    TextView tvMoney;
    private RelativeLayout.LayoutParams layoutParams;
    private int bmpW;
    private int screenW;
    private int wholeWidth;
    private List<Fragment> fragmentList;
    private static final int TAB_COUNT = 3;

    private String mBalance, mMoney;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_account_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initCursor();
        initTab();
        initViewPager();
    }

    public void setBalance(String balance, String money) {
        tvBalance.setText(balance);
        tvMoney.setText(money);
        mBalance = balance;
        mMoney = money;
    }

    private void initTab() {
        rbWhole.setOnClickListener(new AccountListActivity.MyClickListener(0));
        rbIncome.setOnClickListener(new AccountListActivity.MyClickListener(1));
        rbPay.setOnClickListener(new AccountListActivity.MyClickListener(2));
        rbWhole.setChecked(true);
    }

    /*@OnClick(R.id.btn_look)
    public void toBalance(View v) {
        Intent intent = new Intent(this, BalanceActivity.class);
        intent.putExtra(BalanceActivity.KEY_BALANCE, mBalance);
        intent.putExtra(BalanceActivity.KEY_MONEY, mMoney);
        startActivity(intent);
    }*/

    @OnClick(R.id.tv_recharge)
    public void toRecharge(View view) {
        startActivity(new Intent(this, RechargeActivity.class));
    }

    public class MyClickListener implements View.OnClickListener {
        private int index = 0;

        public MyClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            mPager.setCurrentItem(index, true);
        }
    }

    private void initCursor() {
        layoutParams = (RelativeLayout.LayoutParams) cursor.getLayoutParams();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        wholeWidth = dm.widthPixels;
        screenW = dm.widthPixels;
        bmpW = screenW / TAB_COUNT;
        layoutParams.width = bmpW;
        cursor.setLayoutParams(layoutParams);
        cursor.setVisibility(View.VISIBLE);
    }

    private void initViewPager() {
        fragmentList = new ArrayList<>();
        AccountStatusFragment notPayFragment = new AccountStatusFragment();
        Bundle bundleWhole= new Bundle();
        bundleWhole.putInt("status", STATUS_WHOLE);
        notPayFragment.setArguments(bundleWhole);

        AccountStatusFragment incomeFragment = new AccountStatusFragment();
        Bundle bundleIncome= new Bundle();
        bundleIncome.putInt("status", STATUS_INCOME);
        incomeFragment.setArguments(bundleIncome);

        AccountStatusFragment payFragment = new AccountStatusFragment();
        Bundle bundlePay = new Bundle();
        bundlePay.putInt("status", STATUS_PAY);
        payFragment.setArguments(bundlePay);

        fragmentList.add(notPayFragment);
        fragmentList.add(incomeFragment);
        fragmentList.add(payFragment);

        //给ViewPager设置适配器
        mPager.setAdapter(new AccountListActivity.MyFragmentPagerAdapter(getSupportFragmentManager()));
        mPager.setOffscreenPageLimit(TAB_COUNT-1);
        mPager.setCurrentItem(0);//设置当前显示标签页为第一页
        mPager.addOnPageChangeListener(new AccountListActivity.MyOnPageChangeListener());//页面变化时的监听器
    }

    /**
     * 视图加载完成之后执行：值得注意的是PopupWindow每次显示和销毁都会执行一次
     */
    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            tabMove(position, positionOffsetPixels);
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageSelected(int position) {
            tabChange(position);
        }

        private void tabMove(int position, int positionOffsetPixels) {
            int moveI = (int) (bmpW * position + (((double) positionOffsetPixels / wholeWidth) * bmpW));
            layoutParams.leftMargin = moveI;
            cursor.setLayoutParams(layoutParams);
        }

        private void tabChange(int position) {
            if (position == 0) {
                rbWhole.setChecked(true);
            } else if (position == 1) {
                rbIncome.setChecked(true);
            } else if (position == 2) {
                rbPay.setChecked(true);
            }
        }
    }

    class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
