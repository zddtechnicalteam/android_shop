package com.zhaodiandao.shopkeeper.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/1/11.
 * 余额界面
 */
public class BalanceActivity extends BaseActivity {
    public static final String KEY_BALANCE = "key_balance";
    public static final String KEY_MONEY = "key_money";
    @Nullable
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @Nullable
    @BindView(R.id.tv_money)
    TextView tvMoney;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_balance);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        tvBalance.setText(intent.getStringExtra(KEY_BALANCE));
        tvMoney.setText("￥" + intent.getStringExtra(KEY_MONEY));
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @OnClick(R.id.tv_recharge)
    public void toRecharge(View view) {
        startActivity(new Intent(this, RechargeActivity.class));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.PAY_SUCCESS)
            finish();
    }
}
