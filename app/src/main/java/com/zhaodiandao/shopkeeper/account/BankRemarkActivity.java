package com.zhaodiandao.shopkeeper.account;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.config.URLConstants;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.util.DensityUtils;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.PictureUtil;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by yanix on 2017/1/12.
 * 银行转账说明
 */
public class BankRemarkActivity extends BaseActivity {
    public static final int REQUEST_CODE_GALLERY = 100;
    // 图片压缩保存的临时文件名
    private String tmepName;
    @Nullable
    @BindView(R.id.tv_bank_remark)
    TextView tvRemark;
    @Nullable
    @BindView(R.id.bottomsheet)
    BottomSheetLayout mSheetLayout;

    private String money;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_bank_remark);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvRemark.setText(Html.fromHtml(getIntent().getStringExtra("remark")));
        money = getIntent().getStringExtra("money");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // 监听返回键，统一处理返回逻辑
        if (item.getItemId() == android.R.id.home) {
            EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_PAY, "刷新!"));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_PAY, "刷新!"));
        finish();
    }

    @OnClick(R.id.btn_ok)
    public void toAccount(View v) {
        EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_PAY, "刷新!"));
        finish();
    }

    @OnClick(R.id.iv_add)
    public void addImage(View v) {
        MenuSheetView menuSheetView =
                new MenuSheetView(BankRemarkActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.take_photo:
                                // 拍照
                                takePhoto();
                                break;
                            case R.id.take_from_album:
                                // 从相册选择
                                takeFromAlbum();
                                break;
                        }
                        if (mSheetLayout.isSheetShowing()) {
                            mSheetLayout.dismissSheet();
                        }
                        return true;
                    }
                });
        menuSheetView.inflateMenu(R.menu.menu_choose_image);
        mSheetLayout.showWithSheetView(menuSheetView);
    }

    /**
     * 从相册选择
     */
    private void takeFromAlbum() {
        FunctionConfig config = getFunctionConfig();
        GalleryFinal.openGallerySingle(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, @NonNull List<PhotoInfo> resultList) {
                try {
                    // 上传压缩后的图片
                    tmepName = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    uploadByOkHttp(new File(tmepName));
                } catch (IOException e) {
                    e.printStackTrace();
                    ToastUtils.showMessage(getApplicationContext(), e.toString());
                }
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    /**
     * 拍一张
     */
    private void takePhoto() {
        FunctionConfig config = getFunctionConfig();
        GalleryFinal.openCamera(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, @NonNull List<PhotoInfo> resultList) {
                try {
                    tmepName = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    uploadByOkHttp(new File(tmepName));
                } catch (IOException e) {
                    e.printStackTrace();
                    ToastUtils.showMessage(getApplicationContext(), e.toString());
                }
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    private FunctionConfig getFunctionConfig() {
        return new FunctionConfig.Builder()
                .setEnableEdit(true)        //开启编辑功能
                .setEnableCrop(false)       //禁止裁剪功能
                .setEnableRotate(false)     //禁止旋转功能
                .setEnableCamera(true)      //开启相机功能
                .setCropWidth(DensityUtils.dp2px(getApplicationContext(), 110))     //裁剪宽度
                .setCropHeight(DensityUtils.dp2px(getApplicationContext(), 100))    //裁剪高度
                .setCropSquare(false)       //裁剪正方形
                .setForceCrop(false)        //启动强制裁剪功能,一进入编辑页面就开启图片裁剪，不需要用户手动点击裁剪，此功能只针对单选操作
                .setForceCropEdit(false)    //在开启强制裁剪功能时是否可以对图片进行编辑（也就是是否显示旋转图标和拍照图标）
                .setRotateReplaceSource(false)   //配置选择图片时是否替换原始图片，默认不替换
                .setCropReplaceSource(false)     //配置裁剪图片时是否替换原始图片，默认不替换
                .setEnablePreview(true)     //是否开启预览功能
                .build();
    }

    /**
     * 上传文件
     *
     * @param file 要上传的文件
     */
    private void uploadByOkHttp(@NonNull File file) {
        DialogHelper.showDimDialog(BankRemarkActivity.this, "正在上传...");
        // 要上传的file
        RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), file);
        String fileName = file.getName();

        // form的分割线, 自己定义
        String boundary = "xx--------------------------------------------------------------xx";

        MultipartBody mBody = new MultipartBody.Builder(boundary).setType(MultipartBody.FORM)
                /* 上传一个普通的String参数 */
                .addFormDataPart("version", URLConstants.API_VERSION)
                .addFormDataPart("shop_id", (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""))
                .addFormDataPart("type", "6")
                .addFormDataPart("money", money)
                /* 上传文件 */
                .addFormDataPart("file", fileName, fileBody)
                .build();

        Request request = new Request.Builder().url(URLConstants.URL_RECHARGE).post(mBody).build();
        new OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
            public void onResponse(Call call, @NonNull Response response) throws IOException {
                try {
                    String result = response.body().string();
                    if (response.code() != 200) {
                        JSONObject jsonObject = new JSONObject(result);
                        String error = jsonObject.getString("message");
                        ToastUtils.showMessage(getApplicationContext(), error);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DialogHelper.dismissDialog();
                            }
                        });
                    } else {
                        // 删除临时文件
                        PictureUtil.deleteImgTmp(tmepName);
                        runOnUiThread(new Runnable() {
                            public void run() {
                                DialogHelper.dismissDialog();
                                ToastUtils.showMessage(getApplicationContext(), "凭证上传成功");
                                // 重新获取数据
                                finish();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Call call, @NonNull final IOException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        DialogHelper.dismissDialog();
                        ToastUtils.showMessage(getApplicationContext(), e.toString());
                    }
                });
            }
        });
    }
}
