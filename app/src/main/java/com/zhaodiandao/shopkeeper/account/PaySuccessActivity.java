package com.zhaodiandao.shopkeeper.account;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.view.View;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/1/12.
 * 充值成功
 */
public class PaySuccessActivity extends BaseActivity {

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_pay_success);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // 监听返回键，统一处理返回逻辑
        if (item.getItemId() == android.R.id.home) {
            EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_PAY, "刷新!"));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_ok)
    public void paySuccess(View v) {
        EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_PAY, "刷新!"));
        finish();
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_PAY, "刷新!"));
        finish();
    }
}
