package com.zhaodiandao.shopkeeper.brand;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tencent.smtt.sdk.WebChromeClient;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.BrandIntroModel;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.view.SlowlyProgressBar;
import com.zhaodiandao.shopkeeper.view.X5WebView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

import static com.zhaodiandao.shopkeeper.R.id.webView;

/**
 * Created by yanix on 2017/1/17.
 */
public class BrandIntroActivity extends BaseActivity {
    public static final String KEY_BRAND_ID = "brand_id";
    @Nullable
    @BindView(R.id.pb_loading)
    View mProgressBar;
    @Nullable
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView mTvRetry;
    @Nullable
    @BindView(webView)
    X5WebView mWebView;
    @Nullable
    @BindView(R.id.line)
    View line;
    @Nullable
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    private String brandId;
    private BrandIntroModel mBrandIntroModel;
    private boolean isAnimStart = false;
    private int currentProgress;
    private SlowlyProgressBar slowlyProgressBar;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_brand_intro);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        EventBus.getDefault().register(this);

        getWindow().setFormat(PixelFormat.TRANSLUCENT);

        mWebView.getView().setOverScrollMode(View.OVER_SCROLL_ALWAYS);

        slowlyProgressBar = new SlowlyProgressBar(findViewById(R.id.holder), getWindowManager().getDefaultDisplay().getWidth()).setViewHeight(3);

        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(com.tencent.smtt.sdk.WebView webView, int i) {
                super.onProgressChanged(webView, i);
                if (slowlyProgressBar == null)
                    return;
                slowlyProgressBar.setProgress(i);
            }

        });

        // 支持js
        mWebView.getSettings().setJavaScriptEnabled(true);

        brandId = getIntent().getStringExtra(KEY_BRAND_ID);
        getData(brandId);
    }

    public void getData(String brandId) {
        // 获取数据
        mApiService.getBrandIntro(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), brandId,
                new GenericCallback<BrandIntroModel>(getApplicationContext(), BrandIntroModel.class) {

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(@NonNull BrandIntroModel data) {
                        mBrandIntroModel = data;
                        mWebView.loadUrl(data.getDescription());
                        int status = data.getStatus();
                        btnSubmit.setEnabled(true);
                        if (status == 0 || status == 2) {
                            btnSubmit.setText("查 看");
                        } else if (status == 1 || status == 4) {
                            line.setVisibility(View.GONE);
                            btnSubmit.setVisibility(View.GONE);
                        } else if (status == 5) {
                            btnSubmit.setText("加 盟");
                        }
                        llContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData(brandId);
    }

    @OnClick(R.id.btn_submit)
    public void toDetail(View v) {
        Intent intent = new Intent(this, BrandApplyActivity.class);
        intent.putExtra(BrandApplyActivity.KEY_BRAND_ID, mBrandIntroModel.getBrand_id());
        intent.putExtra(BrandApplyActivity.KEY_STATUS, mBrandIntroModel.getStatus());
        intent.putExtra(BrandApplyActivity.KEY_RESTAURANT_ID, mBrandIntroModel.getRestaurant_id());
        startActivity(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.REFRESH_BRAND) {
            finish();
        }
    }
}
