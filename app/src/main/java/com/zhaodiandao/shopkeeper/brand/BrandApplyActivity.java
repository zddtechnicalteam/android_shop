package com.zhaodiandao.shopkeeper.brand;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONArray;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.socks.library.KLog;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.BigImageActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.adapter.BrandImageAdapter;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.config.URLConstants;
import com.zhaodiandao.shopkeeper.model.BrandDetailModel;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.util.DensityUtils;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.PictureUtil;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.util.Util;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by yanix on 2017/1/17.
 */
public class BrandApplyActivity extends BaseActivity {
    public static final String KEY_RESTAURANT_ID = "restaurant_id";
    public static final String KEY_BRAND_ID = "brand_id";
    public static final String KEY_STATUS = "status";
    private static final int REQUEST_CODE_GALLERY = 100;
    // 查看模式：修改和查看
    private static final int STATUS_CHANGE = 1;
    private static final int STATUS_VIEW = 2;
    // 当前模式：修改
    private int status = STATUS_CHANGE;

    @Nullable
    @BindView(R.id.pb_loading)
    View mProgressBar;
    @Nullable
    @BindView(R.id.ll_content)
    LinearLayout mLayoutContent;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @Nullable
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Nullable
    @BindView(R.id.tv_brand_name)
    TextView mTvBrandName;
    @Nullable
    @BindView(R.id.tv_hint)
    TextView tvHint;
    @Nullable
    @BindView(R.id.ll_reason)
    LinearLayout llReason;
    @Nullable
    @BindView(R.id.tv_reason)
    TextView tvReason;

    @Nullable
    @BindView(R.id.btn_apply)
    Button mBtnApply;
    @Nullable
    @BindView(R.id.bottomsheet)
    BottomSheetLayout mSheetLayout;

    // 门店ID
    private String mRestaurantId;
    private String mBrandId;
    private int applyStatus;
    // 当前显示的图片
    @NonNull
    private List<BrandDetailModel.ImageBean> datas = new ArrayList<>();
    // 门店原有的图片
    @NonNull
    private List<BrandDetailModel.ImageBean> originDatas = new ArrayList<>();
    // 从本地添加的图片
    @NonNull
    private List<BrandDetailModel.ImageBean> localDatas = new ArrayList<>();
    // 添加图片
    @NonNull
    private BrandDetailModel.ImageBean mImageBean = new BrandDetailModel.ImageBean();
    private BrandImageAdapter adapter;
    // 保存要删除的图片url
    @NonNull
    private JSONArray jsonArray = new JSONArray();
    // 保存临时文件名
    @NonNull
    private List<String> tempNames = new ArrayList<>();
    // 保存要上传的文件
    @NonNull
    private List<File> uploadFiles = new ArrayList<>();
    // 最多能添加的图片数量
    private int maxSize;
    // 门店之前拥有的图片数量
    private int networkImageCount;
    // 指示当前图片放大动画是否打开，防止重复点击
    boolean isOpening;

    @Nullable
    private String businessLicencePath;
    @Nullable
    private String foodLicencePath;
    @Nullable
    private String idcardFrontPath;
    @Nullable
    private String idcardBackPath;
    @Nullable
    private String idcardBodyPhotoPath;
    @Nullable
    private String idcardBodyPhotoBackPath;
    @Nullable
    private String idcardBlBodyPath;
    @Nullable
    private String bankCardPhotoPath;
    @Nullable
    private String shopPhotoPath;

    @Override
    protected void getView(Bundle savedInstanceState) {
        // 这里的作用是让状态栏从隐藏到显示的过程中,布局不受影响
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        );
        setContentView(R.layout.activity_brand_apply);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        mRestaurantId = getIntent().getStringExtra(KEY_RESTAURANT_ID);
        mBrandId = getIntent().getStringExtra(KEY_BRAND_ID);
        applyStatus = getIntent().getIntExtra(KEY_STATUS, -1);

        if (TextUtils.isEmpty(mRestaurantId)) {
            status = STATUS_VIEW;
            tvHint.setText("请上传加盟条件中所需的照片");
        } else {
            tvHint.setText("上传的证件照");
        }
        if (applyStatus == 0) {      // 未过审
            status = STATUS_VIEW;
            mBtnApply.setText("重新申请");
            llReason.setVisibility(View.VISIBLE);
        } else if (applyStatus == 1) {
            status = STATUS_CHANGE;
            mBtnApply.setText("已加盟");
            mBtnApply.setEnabled(false);
        } else if (applyStatus == 2) {
            status = STATUS_CHANGE;
            mBtnApply.setText("审核中");
            mBtnApply.setEnabled(false);
        }
        getData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpening = false;
    }

    private void getData() {
        mApiService.getBrandDetail(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), mRestaurantId, mBrandId,
                new GenericCallback<BrandDetailModel>(getApplicationContext(), BrandDetailModel.class) {

                    @Override
                    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(@NonNull BrandDetailModel data) {
                        datas.clear();
                        mTvBrandName.setText(data.getBrand_name());
                        tvReason.setText(data.getDescription());
                        // 门店原始照片
                        originDatas.addAll(data.getImg());
                        datas.addAll(data.getImg());
                        // 门店原始照片数量
                        networkImageCount = datas.size();
                        adapter = new BrandImageAdapter(getApplicationContext(), datas);
                        mRecyclerView.setAdapter(adapter);
                        // 照片点击事件
                        adapter.setOnItemClickListener(new BrandImageAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(@NonNull final View view, @NonNull final BrandDetailModel.ImageBean data) {
                                // 判断图片Url是否为空，为空说明是添加图片，只有在修改模式下才会显示
                                if (TextUtils.isEmpty(data.getImg_url())) {
                                    if (status == STATUS_VIEW) {
                                        MenuSheetView menuSheetView =
                                                new MenuSheetView(BrandApplyActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                                                    @Override
                                                    public boolean onMenuItemClick(@NonNull MenuItem item) {
                                                        switch (item.getItemId()) {
                                                            case R.id.take_photo:
                                                                // 拍照
                                                                takePhoto(data.getPos());
                                                                break;
                                                            case R.id.take_from_album:
                                                                // 从相册选择
                                                                takeFromAlbum(data.getPos() + 1);
                                                                break;
                                                        }
                                                        if (mSheetLayout.isSheetShowing()) {
                                                            mSheetLayout.dismissSheet();
                                                        }
                                                        return true;
                                                    }
                                                });
                                        menuSheetView.inflateMenu(R.menu.menu_choose_image);
                                        mSheetLayout.showWithSheetView(menuSheetView);
                                    }
                                } else {
                                    // 图片URL地址不为空，那么就是编辑这张图片，只有在修改模式下才能编辑
                                    if (status == STATUS_VIEW) {
                                        MenuSheetView menuSheetView =
                                                new MenuSheetView(BrandApplyActivity.this, MenuSheetView.MenuType.LIST, "编辑照片...", new MenuSheetView.OnMenuItemClickListener() {
                                                    @Override
                                                    public boolean onMenuItemClick(@NonNull MenuItem item) {
                                                        if (mSheetLayout.isSheetShowing()) {
                                                            mSheetLayout.dismissSheet();
                                                        }
                                                        switch (item.getItemId()) {
                                                            case R.id.big_image:
                                                                // 查看大图
                                                                open(((RelativeLayout) view).getChildAt(0), data.getImg_url());
                                                                break;
                                                            case R.id.delete:
                                                                // 删除图片，如果是本地添加的图片，直接从本地图片集合删除
                                                                // 如果是原始图片，则需要把这个图片id从保存的id集合中移除，同时将原始的图片数量减一
                                                                if (!data.getImg_url().startsWith("/storage")) {
                                                                    com.alibaba.fastjson.JSONObject jsonObject = new com.alibaba.fastjson.JSONObject();
                                                                    jsonObject.put("ri_id", data.getRi_id());
                                                                    jsonObject.put("img_attr", data.getImg_attr());
                                                                    jsonArray.add(jsonObject);
                                                                } else {
                                                                    int index = data.getPos() + 1;
                                                                    if (index == 1)
                                                                        businessLicencePath = "";
                                                                    else if (index == 2)
                                                                        foodLicencePath = "";
                                                                    else if (index == 9)
                                                                        shopPhotoPath = "";
                                                                    else if (index == 3)
                                                                        idcardFrontPath = "";
                                                                    else if (index == 4)
                                                                        idcardBackPath = "";
                                                                    else if (index == 5)
                                                                        idcardBodyPhotoPath = "";
                                                                    else if (index == 6)
                                                                        idcardBodyPhotoBackPath = "";
                                                                    else if (index == 7)
                                                                        idcardBlBodyPath = "";
                                                                    else if (index == 8)
                                                                        bankCardPhotoPath = "";
                                                                }
                                                                data.setImg_url("");
                                                                /*datas.remove(data);
                                                                // 判断是否添加+号图片，图片如果已经存在，就不再重复添加
                                                                if (!datas.contains(mImageBean))
                                                                    datas.add(mImageBean);*/
                                                                adapter.notifyDataSetChanged();
                                                                break;
                                                        }
                                                        return true;
                                                    }
                                                });
                                        menuSheetView.inflateMenu(R.menu.menu_delete_image);
                                        mSheetLayout.showWithSheetView(menuSheetView);
                                    } else {
                                        // 如果是查看模式，只能查看大图，不能编辑
                                        open(((RelativeLayout) view).getChildAt(0), data.getImg_url());
                                    }
                                }
                            }
                        });
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    private void open(@NonNull View view, String url) {
        if (!isOpening) {
            isOpening = true;
            Intent i = new Intent(getApplicationContext(), BigImageActivity.class);
            i.putExtra(KeyConstants.ORIG_X, Util.getXY(view)[0]);
            i.putExtra(KeyConstants.ORIG_Y, Util.getXY(view)[1]);
            i.putExtra(KeyConstants.ORIG_WIDTH, view.getWidth());
            i.putExtra(KeyConstants.ORIG_HEIGHT, view.getHeight());
            i.putExtra(KeyConstants.IMG_URL, url);

            // Start activity disable animations
            startActivity(i);
            BrandApplyActivity.this.overridePendingTransition(0, 0);
        }
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    private void takeFromAlbum(final int index) {
        //带配置
        FunctionConfig config = new FunctionConfig.Builder()
                .build();

        GalleryFinal.openGallerySingle(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {

            @Override
            public void onHanlderSuccess(int reqeustCode, @NonNull List<PhotoInfo> resultList) {
                try {
                    BrandDetailModel.ImageBean imageBean = datas.get(index-1);
                    imageBean.setImg_url(PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath()));
                    datas.set(index-1, imageBean);
                    if (index == 1)
                        businessLicencePath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 2)
                        foodLicencePath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 9)
                        shopPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 3)
                        idcardFrontPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 4)
                        idcardBackPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 5)
                        idcardBodyPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 6)
                        idcardBodyPhotoBackPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 7)
                        idcardBlBodyPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 8)
                        bankCardPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    private void takePhoto(int index) {
        if (Build.VERSION.SDK_INT >= 23) {
            int checkCallPhonePermission = ContextCompat.checkSelfPermission(BrandApplyActivity.this, Manifest.permission.CAMERA);
            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(BrandApplyActivity.this, new String[]{Manifest.permission.CAMERA}, 222);
                return;
            } else {
                openCamera(index+1);
            }
        } else {
            openCamera(index+1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            //就像onActivityResult一样这个地方就是判断你是从哪来的。
            case 222:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                } else {
                    // Permission Denied
                    ToastUtils.showMessage(getApplicationContext(), "禁止相机权限将无法拍照");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void openCamera(final int index) {
        FunctionConfig config = getFunctionConfig();
        GalleryFinal.openCamera(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, @NonNull List<PhotoInfo> resultList) {
                try {
                    BrandDetailModel.ImageBean imageBean = datas.get(index-1);
                    imageBean.setImg_url(PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath()));
                    datas.set(index-1, imageBean);
                    if (index == 1)
                        businessLicencePath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 2)
                        foodLicencePath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 9)
                        shopPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 3)
                        idcardFrontPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 4)
                        idcardBackPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 5)
                        idcardBodyPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 6)
                        idcardBodyPhotoBackPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 7)
                        idcardBlBodyPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    else if (index == 8)
                        bankCardPhotoPath = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    private void updateGridView(@NonNull List<PhotoInfo> resultList) {
        datas.remove(mImageBean);
        datas.removeAll(localDatas);
        localDatas.clear();
        for (PhotoInfo photoInfo : resultList) {
            BrandDetailModel.ImageBean imageBean = new BrandDetailModel.ImageBean(photoInfo.getPhotoPath());
            localDatas.add(imageBean);
        }
        datas.addAll(localDatas);
        maxSize = 6 - datas.size();
        if (maxSize > 0) {
            datas.add(mImageBean);
        }
        adapter.notifyDataSetChanged();
    }

    private FunctionConfig getFunctionConfig() {
        return new FunctionConfig.Builder()
                .setEnableEdit(true)        //开启编辑功能
                .setEnableCrop(false)       //开启裁剪功能
                .setEnableRotate(false)     //开启旋转功能
                .setEnableCamera(true)      //开启相机功能
                .setCropWidth(DensityUtils.dp2px(getApplicationContext(), 100))     //裁剪宽度
                .setCropHeight(DensityUtils.dp2px(getApplicationContext(), 100))    //裁剪高度
                .setCropSquare(false)       //裁剪正方形
                .setForceCrop(false)        //启动强制裁剪功能,一进入编辑页面就开启图片裁剪，不需要用户手动点击裁剪，此功能只针对单选操作
                .setForceCropEdit(false)    //在开启强制裁剪功能时是否可以对图片进行编辑（也就是是否显示旋转图标和拍照图标）
                .setRotateReplaceSource(false)   //配置选择图片时是否替换原始图片，默认不替换
                .setCropReplaceSource(false)     //配置裁剪图片时是否替换原始图片，默认不替换
                .setEnablePreview(true)     //是否开启预览功能
                .build();
    }

    @OnClick(R.id.btn_apply)
    public void submit(View view) {
        DialogHelper.showDimDialog(BrandApplyActivity.this, "正在上传...");
        uploadImage();
    }


    private void uploadImage() {
        String boundary = "xx--------------------------------------------------------------xx";

        MultipartBody.Builder builder = new MultipartBody.Builder(boundary).setType(MultipartBody.FORM)
                .addFormDataPart("version", URLConstants.API_VERSION_2)
                .addFormDataPart("shop_id", (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""))
                .addFormDataPart("brand_id", mBrandId)
                .addFormDataPart("img_id_arr", jsonArray.toJSONString());

        KLog.d("jsonArray = " + jsonArray.toJSONString());

        if (!TextUtils.isEmpty(mRestaurantId)) {
            builder = builder.addFormDataPart("restaurant_id", mRestaurantId);
        }

        if (!TextUtils.isEmpty(businessLicencePath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(businessLicencePath));
            builder = builder.addFormDataPart("file[]", "business_img", fileBody);
        }
        if (!TextUtils.isEmpty(foodLicencePath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(foodLicencePath));
            builder = builder.addFormDataPart("file[]", "food_img", fileBody);
        }
        if (!TextUtils.isEmpty(shopPhotoPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(shopPhotoPath));
            builder = builder.addFormDataPart("file[]", "rinfo", fileBody);
        }
        if (!TextUtils.isEmpty(idcardFrontPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardFrontPath));
            builder = builder.addFormDataPart("file[]", "IDcard_front", fileBody);
        }
        if (!TextUtils.isEmpty(idcardBackPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardBackPath));
            builder = builder.addFormDataPart("file[]", "IDcard_back", fileBody);
        }
        if (!TextUtils.isEmpty(idcardBodyPhotoPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardBodyPhotoPath));
            builder = builder.addFormDataPart("file[]", "IDcard_body_photo", fileBody);
        }
        if (!TextUtils.isEmpty(idcardBodyPhotoBackPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardBodyPhotoBackPath));
            builder = builder.addFormDataPart("file[]", "IDcard_body_photo_back", fileBody);
        }
        if (!TextUtils.isEmpty(idcardBlBodyPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(idcardBlBodyPath));
            builder = builder.addFormDataPart("file[]", "IDcard_bl_body", fileBody);
        }
        if (!TextUtils.isEmpty(bankCardPhotoPath)) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), new File(bankCardPhotoPath));
            builder = builder.addFormDataPart("file[]", "bank_card_photo", fileBody);
        }

        MultipartBody mBody = builder.build();

        Request request = new Request.Builder().url(URLConstants.URL_APPLY_BRAND).post(mBody).build();
        new OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, @NonNull final IOException e) {
                runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      DialogHelper.dismissDialog();
                                      ToastUtils.showMessage(getApplicationContext(), e.getMessage());
                                  }
                              }
                );
            }

            public void onResponse(Call call, @NonNull final Response response) throws IOException{

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DialogHelper.dismissDialog();

                        try {
                            String result = response.body().string();
                            if (response.code() != 200) {
                                JSONObject jsonObject = new JSONObject(result);
                                String error = jsonObject.getString("message");
                                ToastUtils.showMessage(getApplicationContext(), error);
                            } else {
                                EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_BRAND, "刷新!"));
                                ToastUtils.showMessage(getApplicationContext(), "信息提交成功");
                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
