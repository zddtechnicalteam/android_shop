package com.zhaodiandao.shopkeeper.me;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.BigImageActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.adapter.BrandImageAdapter;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.BrandDetailModel;
import com.zhaodiandao.shopkeeper.model.FeedbackDetailBean;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.net.ZddStringCallback;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.util.Util;
import com.zhaodiandao.shopkeeper.view.ScrollGridLayoutManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Request;

/**
 * Created by yanix on 2017/2/20.
 * 质量反馈详情
 */
public class FeedbackDetailActivity extends BaseActivity {
    public static final String KEY_FEEDBACK_ID = "feedback_id";

    @Nullable
    @BindView(R.id.tv_product)
    TextView tvProduct;
    @Nullable
    @BindView(R.id.tv_batch)
    TextView tvBatch;
    @Nullable
    @BindView(R.id.tv_problem)
    TextView tvProblem;
    @Nullable
    @BindView(R.id.tv_result)
    TextView tvResult;
    @Nullable
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.confirm)
    TextView confirm;
    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @NonNull
    private List<BrandDetailModel.ImageBean> datas = new ArrayList<>();
    private BrandImageAdapter adapter;
    // 指示当前图片放大动画是否打开，防止重复点击
    boolean isOpening;

    @Override
    protected void getView(Bundle savedInstanceState) {
        // 这里的作用是让状态栏从隐藏到显示的过程中,布局不受影响
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        );

        setContentView(R.layout.activity_feedback_detail);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*FullyGridLayoutManager manager = new FullyGridLayoutManager(this, 3);
        manager.setOrientation(GridLayoutManager.VERTICAL);
        manager.setSmoothScrollbarEnabled(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setNestedScrollingEnabled(false);*/

        ScrollGridLayoutManager recyclerViews = new ScrollGridLayoutManager(this, 3);
        recyclerViews.setScrollEnabled(false);
        mRecyclerView.setLayoutManager(recyclerViews);

        // mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new BrandImageAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);
        // 照片点击事件
        adapter.setOnItemClickListener(new BrandImageAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(@NonNull final View view, @NonNull final BrandDetailModel.ImageBean data) {
                open(((RelativeLayout) view).getChildAt(0), data.getImg_url());
            }
        });

        final String feedbackId = getIntent().getStringExtra(KEY_FEEDBACK_ID);
        getFeedbackDetail(feedbackId);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogHelper.showDimDialog(FeedbackDetailActivity.this, "正在提交...");
                mApiService.confirmFeedback(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), feedbackId,
                        new ZddStringCallback(getApplicationContext(), "message") {
                            @Override
                            public void success(String response) {
                                ToastUtils.showMessage(getApplicationContext(), "确认成功");
                                EventBus.getDefault().post(new MessageEvent(EventCode.CONFIRM_FEEDBACK, "确认成功"));
                                finish();
                            }

                            @Override
                            public void onAfter(int id) {
                                super.onAfter(id);
                                DialogHelper.dismissDialog();
                            }
                        });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpening = false;
    }

    private void getFeedbackDetail(String feedbackId) {
        mApiService.getFeedbackDetail(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), feedbackId,
                new GenericCallback<FeedbackDetailBean>(getApplicationContext(), FeedbackDetailBean.class) {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(FeedbackDetailActivity.this, "努力加载中...");
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }

                    @Override
                    public void success(@NonNull FeedbackDetailBean data) {
                        tvProduct.setText(data.getProduct());
                        tvBatch.setText(data.getBatch());
                        tvProblem.setText(data.getProblem());
                        tvResult.setText(data.getSolve());
                        tvStatus.setText(data.getStatus_label());

                        if (data.getStatus_id() == 3) {
                            confirm.setVisibility(View.VISIBLE);
                        } else {
                            confirm.setVisibility(View.GONE);
                        }

                        datas.clear();
                        for (String img : data.getImgs()) {
                            datas.add(new BrandDetailModel.ImageBean(img));
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
        );
    }

    private void open(@NonNull View view, String url) {
        if (!isOpening) {
            isOpening = true;
            Intent i = new Intent(getApplicationContext(), BigImageActivity.class);
            i.putExtra(KeyConstants.ORIG_X, Util.getXY(view)[0]);
            i.putExtra(KeyConstants.ORIG_Y, Util.getXY(view)[1]);
            i.putExtra(KeyConstants.ORIG_WIDTH, view.getWidth());
            i.putExtra(KeyConstants.ORIG_HEIGHT, view.getHeight());
            i.putExtra(KeyConstants.IMG_URL, url);

            // Start activity disable animations
            startActivity(i);
            FeedbackDetailActivity.this.overridePendingTransition(0, 0);
        }
    }
}
