/*
 * Copyright (c) 2017.
 * 早点到餐饮管理有限公司 版权所有
 */

package com.zhaodiandao.shopkeeper.me;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.ticket.MyStkTicketActivity;
import com.zhaodiandao.shopkeeper.ticket.MyTicketActivity;

/**
 * Created by yanix on 2017/8/21.
 */

public class MyBuyTicketActivity extends BaseActivity {

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_my_buy_ticket);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.tvTicket).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyBuyTicketActivity.this, MyTicketActivity.class));
            }
        });

        findViewById(R.id.tvStkTicket).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyBuyTicketActivity.this, MyStkTicketActivity.class));
            }
        });
    }
}
