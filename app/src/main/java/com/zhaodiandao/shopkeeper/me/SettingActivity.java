package com.zhaodiandao.shopkeeper.me;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.igexin.sdk.PushManager;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.umeng.analytics.MobclickAgent;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.BuildConfig;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.login.LoginActivity;
import com.zhaodiandao.shopkeeper.model.UpdateInfo;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.util.AppUtils;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.view.CustemConfirmDialog;
import com.zx.uploadlibrary.listener.impl.UIProgressListener;
import com.zx.uploadlibrary.utils.OKHttpUtils;

import java.io.File;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2016/11/25.
 * 业务员信息界面
 */
public class SettingActivity extends BaseActivity {
    CustemConfirmDialog dialog;

    @Nullable
    @BindView(R.id.tv_version)
    TextView tvVersion;

    private AppUtils mAppUtils;
    private CustemConfirmDialog mDialog;
    private ProgressDialog progressDialog;
    private static final String SAVE = "sdcard/shop.apk";

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        mAppUtils = AppUtils.getInstance();
        tvVersion.setText("v" + mAppUtils.getVersion(getApplicationContext()));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.tv_change_password)
    public void changePassword(View view) {
        startActivity(new Intent(this, ChangePasswordActivity.class));
    }

    @OnClick(R.id.ll_check_update)
    public void checkUpdate(View view) {
        checkUpdate();
    }

    @OnClick(R.id.tv_exit_login)
    public void exitLogin(View view) {
        showConfirmDialog();
    }

    private void showConfirmDialog() {
        dialog = new CustemConfirmDialog(SettingActivity.this, R.style.QQStyle, "确定退出吗？", "退出",
        new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                PushManager.getInstance().unBindAlias(getApplicationContext(), "shop_id_" + SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""), true);
                SPUtils.remove(getApplicationContext(), KeyConstants.SHOP_ID);
                SPUtils.remove(getApplicationContext(), KeyConstants.SHOP_NAME);
                SPUtils.remove(getApplicationContext(), KeyConstants.SHOP_ADDRESS);

                if (!BuildConfig.LOG_DEBUG) {
                    // 停止统计账户信息
                    MobclickAgent.onProfileSignOff();
                }

                DialogHelper.showDimDialog(SettingActivity.this, "正在退出聊天账号...");
                EMClient.getInstance().logout(true, new EMCallBack() {

                    @Override
                    public void onSuccess() {
                        DialogHelper.dismissDialog();
                        Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.reduce_in, R.anim.reduce_out);
                    }

                    @Override
                    public void onProgress(int progress, String status) {

                    }

                    @Override
                    public void onError(int code, String message) {
                        DialogHelper.dismissDialog();
                        ToastUtils.showMessage(getApplicationContext(), "退出失败");
                    }
                });


            }
        });
        dialog.show();
    }

    private void checkUpdate() {
        DialogHelper.showDimDialog(SettingActivity.this, "正在检查更新...");
        // 获取数据
        mApiService.getUpdateInfo(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                mAppUtils.getVersion(getApplicationContext()), mAppUtils.getVersionCode(getApplicationContext()),
                new GenericCallback<UpdateInfo>(getApplicationContext(), UpdateInfo.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        ToastUtils.showMessage(getApplicationContext(), "出错了");
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        ToastUtils.showMessage(getApplicationContext(), "出错了");
                    }

                    @Override
                    public void success(@NonNull final UpdateInfo data) {
                        if ("0".equals(data.getUpdate())) {  // 没有更新
                            ToastUtils.showMessage(getApplicationContext(), "你的版本已经是最新的了");
                        } else {        // 有更新
                            mDialog = new CustemConfirmDialog(SettingActivity.this, R.style.QQStyle, data.getUpdate_mark(), "下载",
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                            download(data.getDownloadUrl());
                                        }
                                    }
                            );
                            mDialog.setCancelable(true);
                            mDialog.setCanceledOnTouchOutside(true);
                            mDialog.show();
                        }

                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }
                });
    }

    protected void download2(String url) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            showProgress();
            HttpUtils httpUtils = new HttpUtils();
            httpUtils.download(url, SAVE, new RequestCallBack<File>() {
                @Override
                public void onSuccess(ResponseInfo<File> responseInfo) {
                    progressDialog.dismiss();
                    try {
                        install();
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(HttpException e, String s) {
                    progressDialog.dismiss();
                    ToastUtils.showMessage(getApplicationContext(), "新版本下载失败");
                }

                public void onLoading(long total, long current, boolean isUploding) {
                    try {
                        super.onLoading(total, current, isUploding);
                        progressDialog.setMax((int) total);
                        progressDialog.setProgress((int) current);
                        float all = total / 1024.0f / 1024.0f;
                        float percent = current / 1024.0f / 1024.0f;
                        progressDialog.setProgressNumberFormat(String.format(Locale.CHINA, "%.2fM/%.2fM", percent, all));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            ToastUtils.showMessage(getApplicationContext(), "没有发现可用的SD卡");
        }
    }

    private void download(String url) {
        //这个是ui线程回调，可直接操作UI
        final UIProgressListener uiProgressResponseListener = new UIProgressListener() {
            @Override
            public void onUIProgress(long bytesRead, long contentLength, boolean done) {
                Log.i("TAG", "bytesRead:" + bytesRead);
                Log.i("TAG", "contentLength:" + contentLength);
                Log.i("TAG", "done:" + done);
                if (contentLength != -1) {
                    //长度未知的情况下回返回-1
                    Log.i("TAG", (100 * bytesRead) / contentLength + "% done");
                }
                Log.i("TAG", "================================");
                progressDialog.setMax((int) contentLength);
                progressDialog.setProgress((int) bytesRead);
                // 格式化下载进度，以MB为单位
                float all = contentLength / 1024.0f / 1024.0f;
                float percent = bytesRead / 1024.0f / 1024.0f;
                progressDialog.setProgressNumberFormat(String.format(Locale.CHINA, "%.2fM/%.2fM", percent, all));
            }

            @Override
            public void onUIStart(long bytesRead, long contentLength, boolean done) {
                super.onUIStart(bytesRead, contentLength, done);
                ToastUtils.showMessage(getApplicationContext(), "开始下载");
            }

            @Override
            public void onUIFinish(long bytesRead, long contentLength, boolean done) {
                super.onUIFinish(bytesRead, contentLength, done);
                progressDialog.dismiss();
                if (!done) {
                    // 下载失败
                    ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_download_fail));
                } else {
                    // 下载完成
                    try {
                        // 安装新版本
                        install();
                        // 退出界面
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            // 显示进度框
            showProgress();
            //开启文件下载
            OKHttpUtils.downloadandSaveFile(url, SAVE, uiProgressResponseListener);
        } else {
            // 没有SDCard
            ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_no_sdcard));
        }
    }

    private void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("下载进度");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    protected void install() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        // 添加flag
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(Uri.fromFile(new File(SAVE)), "application/vnd.android.package-archive");
        startActivity(intent);
    }
}
