package com.zhaodiandao.shopkeeper.me;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.net.ZddStringCallback;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Request;

/**
 * Created by yanix on 2016/11/28.
 * 修改密码界面
 */
public class ChangePasswordActivity extends BaseActivity {
    @Nullable
    @BindView(R.id.et_new_password)
    TextInputEditText etNewPassword;
    @Nullable
    @BindView(R.id.et_new_password_repeat)
    TextInputEditText etNewPasswordRepeat;
    @Nullable
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_change_password);
        // bind view
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addTextChangeListener(etNewPassword);
        addTextChangeListener(etNewPasswordRepeat);
    }

    /**
     * 监听输入框变化，改变按钮状态
     * @param editText 要监听的输入框
     */
    private void addTextChangeListener(@NonNull final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            boolean isOtherEmpty = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (editText == etNewPassword) {
                    isOtherEmpty = TextUtils.isEmpty(etNewPasswordRepeat.getText().toString().trim());
                } else {
                    isOtherEmpty = TextUtils.isEmpty(etNewPassword.getText().toString().trim());
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(@NonNull Editable s) {
                // 如果账号密码都不为空，登录按钮状态设置为正常；否则，设置为禁用
                if (!TextUtils.isEmpty(s.toString().trim()) && !isOtherEmpty) {
                    btnSubmit.setEnabled(true);
                } else {
                    btnSubmit.setEnabled(false);
                }
            }
        });
    }

    @OnClick(R.id.btn_submit)
    public void submit(View view) {
        String newPsw = etNewPassword.getText().toString().trim();
        String newPswRepeat = etNewPasswordRepeat.getText().toString().trim();
        if (!newPsw.equals(newPswRepeat)) {
            ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_password_not_consistence));
            return;
        }

        mApiService.changePsw(requestTag, SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, "").toString(), newPsw, new ZddStringCallback(getApplicationContext(), "success") {

            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                DialogHelper.showDimDialog(ChangePasswordActivity.this, "正在修改...");
            }

            @Override
            public void onAfter(int id) {
                DialogHelper.dismissDialog();
                super.onAfter(id);
            }

            @Override
            public void success(String response) {
                // 修改成功
                ToastUtils.showMessage(getApplicationContext(), response);
                finish();
            }
        });
    }
}
