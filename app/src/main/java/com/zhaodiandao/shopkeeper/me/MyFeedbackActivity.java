package com.zhaodiandao.shopkeeper.me;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.adapter.MyFeedbackAdapter;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.FeedbackBean;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.ProductModel;
import com.zhaodiandao.shopkeeper.net.GenericArrayCallback;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.order.OrderListActivity;
import com.zhaodiandao.shopkeeper.order.ProductComplaintActivity;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by yanix on 2017/1/17.
 * 我的质量反馈
 */
public class MyFeedbackActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    @Nullable
    @BindView(R.id.pb_loading)
    View mProgressBar;
    @Nullable
    @BindView(R.id.ll_content)
    SwipeRefreshLayout mLayoutContent;
    @Nullable
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @Nullable
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private MyFeedbackAdapter mAdapter;
    @NonNull
    private List<FeedbackBean> datas = new ArrayList<>();

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_my_brand);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mLayoutContent.setColorSchemeResources(R.color.colorPrimary);
        mLayoutContent.setOnRefreshListener(this);
        mAdapter = new MyFeedbackAdapter(getApplicationContext(), datas);
        mAdapter.setOnItemClickListener(new MyFeedbackAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, @NonNull FeedbackBean data) {
                Intent intent = new Intent(MyFeedbackActivity.this, FeedbackDetailActivity.class);
                intent.putExtra(FeedbackDetailActivity.KEY_FEEDBACK_ID, data.getFd_id());
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        getData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.CONFIRM_FEEDBACK)
            getData();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public void getData() {
        // 获取数据
        mApiService.getMyFeedbackList(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericArrayCallback<FeedbackBean>(getApplicationContext(), FeedbackBean.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(@NonNull List<FeedbackBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    public void refresh() {
        // 获取数据
        mApiService.getMyFeedbackList(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericArrayCallback<FeedbackBean>(getApplicationContext(), FeedbackBean.class) {

                    @Override
                    public void success(@NonNull List<FeedbackBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_feedback, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.feedback:
                // toComplaint();
                Intent intent = new Intent(this, OrderListActivity.class);
                intent.putExtra(OrderListActivity.KEY_FROM_FEEDBACK, true);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void toComplaint() {
        mApiService.toFeedback(requestTag, (String) SPUtils.get(getApplicationContext(), KeyConstants.SHOP_ID, ""),
                new GenericCallback<ProductModel>(getApplicationContext(), ProductModel.class) {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(MyFeedbackActivity.this, "努力请求中...");
                    }

                    @Override
                    public void onAfter(int id) {
                        DialogHelper.dismissDialog();
                        super.onAfter(id);
                    }

                    @Override
                    public void success(ProductModel data) {
                        Intent intent = new Intent(MyFeedbackActivity.this, ProductComplaintActivity.class);
                        intent.putExtra(ProductComplaintActivity.KEY_PRODUCTS, data);
                        startActivity(intent);
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void onRefresh() {
        refresh();
    }
}
