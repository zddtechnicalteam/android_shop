package com.zhaodiandao.shopkeeper;

import android.os.Bundle;

import com.tencent.smtt.sdk.VideoActivity;

/**
 * Created by yanix on 2017/5/3.
 */
public class ZddVideoActivity extends VideoActivity {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }
}
