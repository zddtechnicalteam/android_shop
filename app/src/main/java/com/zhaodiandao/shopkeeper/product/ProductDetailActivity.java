package com.zhaodiandao.shopkeeper.product;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.fragment.OrderFragment;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.ProductClassBean;
import com.zhaodiandao.shopkeeper.order.DeliverInfoActivity;
import com.zhaodiandao.shopkeeper.util.AnimUtils;
import com.zhaodiandao.shopkeeper.util.DensityUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhaodiandao.shopkeeper.util.simpleadpter.CommonAdapter;
import com.zhaodiandao.shopkeeper.util.simpleadpter.ViewHolder;
import com.zhaodiandao.shopkeeper.view.CustemConfirmDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.zhaodiandao.shopkeeper.fragment.OrderFragment.realCart;

/**
 * Created by yangx on 2016/3/29.
 * 货品详情
 */
public class ProductDetailActivity extends BaseActivity implements View.OnClickListener {
    private static final int ACTION_ADD = 1;
    private static final int ACTION_REDUCE = 2;
    @Nullable
    @BindView(R.id.tv_name)
    TextView tvName;
    @Nullable
    @BindView(R.id.tv_format)
    TextView tvFormat;
    @Nullable
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @Nullable
    @BindView(R.id.tv_detail)
    TextView tvDetail;
    @Nullable
    @BindView(R.id.iv_product)
    ImageView ivProduct;
    @Nullable
    @BindView(R.id.iv_anim_reduce)
    ImageView ivAnimReduce;
    @Nullable
    @BindView(R.id.iv_add)
    ImageView ivAdd;
    @Nullable
    @BindView(R.id.iv_reduce)
    ImageView ivReduce;
    @Nullable
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @Nullable
    @BindView(R.id.tv_ok)
    TextView tvOk;
    @Nullable
    @BindView(R.id.tv_sum)
    TextView tvSum;
    @Nullable
    @BindView(R.id.tv_count)
    TextView tvCount;
    @Nullable
    @BindView(R.id.ll_cart)
    LinearLayout llCart;
    @Nullable
    @BindView(R.id.iv_cart)
    ImageView ivCart;
    // 辅助购物车显示的view
    @Nullable
    @BindView(R.id.black_view)
    View blackView;

    private ImageView imageView;
    @Nullable
    private AnimUtils animUtils;

    private int position = 0;

    private CustemConfirmDialog dialog;
    private boolean isShowCart = true;
    private PopupWindow popWindow;

    private CommonAdapter<ProductClassBean.ProductBean> cartAdapter;
    private ProductClassBean.ProductBean mproductInfo;

    @NonNull
    private Handler mHandler = new InternalHandler(this);

    private static class InternalHandler extends Handler {
        private WeakReference<Activity> weakRefActivity;

        /**
         * A constructor that gets a weak reference to the enclosing class. We
         * do this to avoid memory leaks during Java Garbage Collection.
         */
        public InternalHandler(Activity activity) {
            weakRefActivity = new WeakReference<Activity>(activity);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            Activity activity = weakRefActivity.get();
            if (activity != null) {
                switch (msg.what) {
                    case ACTION_ADD:
                        String pidAdd = msg.arg1 + "";
                        if (pidAdd.equals(((ProductDetailActivity) activity).mproductInfo.getPid())) {
                            ((ProductDetailActivity) activity).mproductInfo.setNumber(((ProductDetailActivity) activity).mproductInfo.getNumber() + 1);
                            ((ProductDetailActivity) activity).tvAmount.setText(((ProductDetailActivity) activity).mproductInfo.getNumber() + "");
                            if (((ProductDetailActivity) activity).mproductInfo.getNumber() == 1) {
                                ((ProductDetailActivity) activity).ivReduce.setEnabled(true);
                                ((ProductDetailActivity) activity).ivReduce.setVisibility(View.VISIBLE);
                                ((ProductDetailActivity) activity).tvAmount.setVisibility(View.VISIBLE);
                            }
                        }

                        ProductClassBean.ProductBean addItem = (ProductClassBean.ProductBean) msg.obj;
                        int addtotalCount = Integer.parseInt(((ProductDetailActivity) activity).tvCount.getText().toString());
                        float addsumMoney = Float.parseFloat(((ProductDetailActivity) activity).tvSum.getText().toString());

                        addtotalCount++;
                        addsumMoney += addItem.getPrice();
                        ((ProductDetailActivity) activity).tvCount.setTextSize(120);
                        if (addtotalCount > 0) {
                            if (addtotalCount > 999) {
                                ((ProductDetailActivity) activity).tvCount.setTextSize(6);
                            } else if (addtotalCount > 99) {
                                ((ProductDetailActivity) activity).tvCount.setTextSize(8);
                            } else if (addtotalCount > 9) {
                                ((ProductDetailActivity) activity).tvCount.setTextSize(10);
                            }
                            ((ProductDetailActivity) activity).tvOk.setEnabled(true);
                            ((ProductDetailActivity) activity).tvCount.setText(addtotalCount + "");
                            ((ProductDetailActivity) activity).tvCount.setVisibility(View.VISIBLE);
                            BigDecimal bigDecimal = new BigDecimal(addsumMoney);
                            ((ProductDetailActivity) activity).tvSum.setText("" + bigDecimal.setScale(KeyConstants.SCALE, BigDecimal.ROUND_HALF_UP).floatValue());
                        } else {
                            ((ProductDetailActivity) activity).tvOk.setEnabled(false);
                            ((ProductDetailActivity) activity).tvCount.setText("0");
                            ((ProductDetailActivity) activity).tvCount.setVisibility(View.GONE);
                            ((ProductDetailActivity) activity).tvSum.setText("0");
                        }
                        break;
                    case ACTION_REDUCE:
                        String pidReduce = msg.arg1 + "";
                        if (pidReduce.equals(((ProductDetailActivity) activity).mproductInfo.getPid())) {
                            ((ProductDetailActivity) activity).mproductInfo.setNumber(((ProductDetailActivity) activity).mproductInfo.getNumber() - 1);
                            ((ProductDetailActivity) activity).tvAmount.setText(((ProductDetailActivity) activity).mproductInfo.getNumber() + "");
                            if (((ProductDetailActivity) activity).mproductInfo.getNumber() <= 0) {
                                ((ProductDetailActivity) activity).ivReduce.setEnabled(false);
                                ((ProductDetailActivity) activity).ivReduce.setVisibility(View.INVISIBLE);
                                ((ProductDetailActivity) activity).tvAmount.setVisibility(View.INVISIBLE);
                            }
                        }

                        ProductClassBean.ProductBean recudeItem = (ProductClassBean.ProductBean) msg.obj;
                        int totalCount = Integer.parseInt(((ProductDetailActivity) activity).tvCount.getText().toString());
                        float sumMoney = Float.parseFloat(((ProductDetailActivity) activity).tvSum.getText().toString());

                        totalCount--;
                        sumMoney -= recudeItem.getPrice();
                        ((ProductDetailActivity) activity).tvCount.setTextSize(12);
                        if (totalCount > 0) {
                            if (totalCount > 999) {
                                ((ProductDetailActivity) activity).tvCount.setTextSize(6);
                            } else if (totalCount > 99) {
                                ((ProductDetailActivity) activity).tvCount.setTextSize(8);
                            } else if (totalCount > 9) {
                                ((ProductDetailActivity) activity).tvCount.setTextSize(10);
                            }
                            ((ProductDetailActivity) activity).tvOk.setEnabled(true);
                            ((ProductDetailActivity) activity).tvCount.setText(totalCount + "");
                            ((ProductDetailActivity) activity).tvCount.setVisibility(View.VISIBLE);
                            BigDecimal bigDecimal = new BigDecimal(sumMoney);
                            ((ProductDetailActivity) activity).tvSum.setText("" + bigDecimal.setScale(KeyConstants.SCALE, BigDecimal.ROUND_HALF_UP).floatValue());
                        } else {
                            ((ProductDetailActivity) activity).tvOk.setEnabled(false);
                            ((ProductDetailActivity) activity).tvCount.setText("0");
                            ((ProductDetailActivity) activity).tvCount.setVisibility(View.GONE);
                            ((ProductDetailActivity) activity).tvSum.setText("0");
                        }
                        break;
                }
            }
            super.handleMessage(msg);
        }
    }

    @Override
    protected void beforeContent(Bundle savedInstanceState) {
        super.beforeContent(savedInstanceState);
        // 设置全屏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        Intent intent = getIntent();
        mproductInfo = (ProductClassBean.ProductBean) intent.getSerializableExtra("product");

        position = intent.getIntExtra("pos", 0);

        if (!TextUtils.isEmpty(mproductInfo.getImage())) {
            Picasso.with(this).load(mproductInfo.getImage()).placeholder(R.drawable.default_src).into(ivProduct);
        }
        tvName.setText(mproductInfo.getName());
        tvDetail.setText(mproductInfo.getDetail());

        RelativeLayout rlAdded = (RelativeLayout) findViewById(R.id.rl_added);
        TextView tvReplenishment = (TextView) findViewById(R.id.tv_replenishment);
        TextView tvDayLeft = (TextView) findViewById(R.id.tv_day_left);

        if (mproductInfo.getDaysleft() == 0) {
            tvDayLeft.setVisibility(View.GONE);
            int status = mproductInfo.getStatus();
            if (status == 1) {
                if (mproductInfo.getBuy_amont() != 0) {
                    rlAdded.setVisibility(View.VISIBLE);
                    tvReplenishment.setVisibility(View.GONE);
                } else {
                    rlAdded.setVisibility(View.GONE);
                    tvReplenishment.setVisibility(View.VISIBLE);
                    tvReplenishment.setText("需使用预购券购买");
                }
            } else if (status == 3) {
                rlAdded.setVisibility(View.GONE);
                tvReplenishment.setVisibility(View.VISIBLE);
                tvReplenishment.setText("补货中");
            }
        } else {
            rlAdded.setVisibility(View.GONE);
            tvReplenishment.setVisibility(View.GONE);
            tvDayLeft.setText("还有" + mproductInfo.getDaysleft() + "天开抢");
            tvDayLeft.setVisibility(View.VISIBLE);
        }

        tvFormat.setText(mproductInfo.getFormat());
        tvPrice.setText("￥" + mproductInfo.getPrice() + "/" + mproductInfo.getUnit());
        tvOk.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        llCart.setOnClickListener(this);

        animUtils = new AnimUtils(this, ivCart);
        blackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
            }
        });

        int number = mproductInfo.getNumber();
        if (number > 0) {
            tvAmount.setVisibility(View.VISIBLE);
            ivReduce.setVisibility(View.VISIBLE);
            tvAmount.setText(number + "");
        }

        int totalCount = OrderFragment.getCartAmount();
        tvCount.setTextSize(12);
        if (totalCount > 0) {
            if (totalCount > 999) {
                tvCount.setTextSize(6);
            } else if (totalCount > 99) {
                tvCount.setTextSize(8);
            } else if (totalCount > 9) {
                tvCount.setTextSize(10);
            }
            tvOk.setEnabled(true);
            tvCount.setText(totalCount + "");
            tvCount.setVisibility(View.VISIBLE);
            BigDecimal bigDecimal = new BigDecimal(OrderFragment.getCartMoney());
            tvSum.setText("" + bigDecimal.setScale(KeyConstants.SCALE, BigDecimal.ROUND_HALF_UP).floatValue());
        } else {
            tvOk.setEnabled(false);
            tvCount.setText("0");
            tvCount.setVisibility(View.GONE);
            tvSum.setText("0");
        }

        ivReduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mproductInfo.setNumber(mproductInfo.getNumber() - 1);
                OrderFragment.productInfos.get(position).setNumber(mproductInfo.getNumber());
                tvAmount.setText(mproductInfo.getNumber() + "");
                OrderFragment.classes.get(mproductInfo.getPosition()).setNumber(OrderFragment.classes.get(mproductInfo.getPosition()).getNumber() - 1);
                OrderFragment.classAdapter.notifyDataSetChanged();

                if (mproductInfo.getNumber() <= 0) {
                    ivReduce.setEnabled(false);
                    tvAmount.setVisibility(View.INVISIBLE);
                    hide(ivReduce, ivAnimReduce);
                }

                Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                intent.putExtra("info", mproductInfo);
                intent.putExtra("type", KeyConstants.REDUCE);
                sendBroadcast(intent);

                int totalCount = Integer.parseInt(tvCount.getText().toString());
                float sumMoney = Float.parseFloat(tvSum.getText().toString());

                totalCount--;
                sumMoney -= mproductInfo.getPrice();
                tvCount.setTextSize(12);
                if (totalCount > 0) {
                    if (totalCount > 999) {
                        tvCount.setTextSize(6);
                    } else if (totalCount > 99) {
                        tvCount.setTextSize(8);
                    } else if (totalCount > 9) {
                        tvCount.setTextSize(10);
                    }
                    tvOk.setEnabled(true);
                    tvCount.setText(totalCount + "");
                    tvCount.setVisibility(View.VISIBLE);
                    BigDecimal bigDecimal = new BigDecimal(sumMoney);
                    tvSum.setText("" + bigDecimal.setScale(KeyConstants.SCALE, BigDecimal.ROUND_HALF_UP).floatValue());
                } else {
                    tvOk.setEnabled(false);
                    tvCount.setText("0");
                    tvCount.setVisibility(View.GONE);
                    tvSum.setText("0");
                }
            }
        });

        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@NonNull View v) {
                if (mproductInfo.getNumber() >= mproductInfo.getBuy_amont() && mproductInfo.getBuy_amont() != -1) {
                    ToastUtils.showMessage(getApplicationContext(), "已达到最大购买量");
                    return;
                }

                mproductInfo.setNumber(mproductInfo.getNumber() + 1);
                OrderFragment.productInfos.get(position).setNumber(mproductInfo.getNumber());
                tvAmount.setText(mproductInfo.getNumber() + "");
                tvAmount.setVisibility(View.VISIBLE);

                OrderFragment.classes.get(mproductInfo.getPosition()).setNumber(OrderFragment.classes.get(mproductInfo.getPosition()).getNumber() + 1);
                OrderFragment.classAdapter.notifyDataSetChanged();

                if (mproductInfo.getNumber() == 1) {
                    ivAdd.setEnabled(false);
                    show(ivAnimReduce, ivAdd, ivReduce, mproductInfo);
                } else {
                    Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                    intent.putExtra("info", mproductInfo);
                    intent.putExtra("type", KeyConstants.ADD);
                    sendBroadcast(intent);
                }

                int totalCount = Integer.parseInt(tvCount.getText().toString());
                float sumMoney = Float.parseFloat(tvSum.getText().toString());
                totalCount++;
                sumMoney += mproductInfo.getPrice();
                tvCount.setTextSize(12);

                if (totalCount > 0) {
                    if (totalCount > 999) {
                        tvCount.setTextSize(6);
                    } else if (totalCount > 99) {
                        tvCount.setTextSize(8);
                    } else if (totalCount > 9) {
                        tvCount.setTextSize(10);
                    }
                    tvOk.setEnabled(true);
                    tvCount.setText(totalCount + "");
                    tvCount.setVisibility(View.VISIBLE);
                    BigDecimal bigDecimal = new BigDecimal(sumMoney);
                    tvSum.setText("" + bigDecimal.setScale(KeyConstants.SCALE, BigDecimal.ROUND_HALF_UP).floatValue());
                } else {
                    tvOk.setEnabled(false);
                    tvCount.setText("0");
                    tvCount.setVisibility(View.GONE);
                    tvSum.setText("0");
                }

                int[] start_location = new int[2];// 一个整型数组，用来存储按钮的在屏幕的X、Y坐标
                v.getLocationInWindow(start_location);// 这是获取购买按钮的在屏幕的X、Y坐标（这也是动画开始的坐标）
                imageView = new ImageView(ProductDetailActivity.this);
                imageView.setImageBitmap(animUtils.getAddDrawBitMap());// 设置buyImg的图片
                animUtils.setAnim(imageView, start_location, null);// 开始执行动画
            }
        });
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void hide(@NonNull final View v, @NonNull View endView) {
        final int[] startPos = new int[2];
        v.getLocationOnScreen(startPos);

        final int[] endPos = new int[2];
        endView.getLocationOnScreen(endPos);

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setDuration(300);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, endPos[0] - startPos[0], 0, 0);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        RotateAnimation rotateAnimation = new RotateAnimation(0, 720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(300);
        //animationSet.addAnimation(rotateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        v.startAnimation(animationSet);

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setEnabled(true);
                v.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void show(@NonNull View v, @NonNull final View clickView, @NonNull final View endView, final ProductClassBean.ProductBean item) {
        int[] startPos = new int[2];
        v.getLocationOnScreen(startPos);

        final int[] endPos = new int[2];
        endView.getLocationOnScreen(endPos);

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setDuration(300);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, endPos[0] - startPos[0], 0, 0);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        RotateAnimation rotateAnimation = new RotateAnimation(0, -720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(300);
        //animationSet.addAnimation(rotateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        v.startAnimation(animationSet);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                endView.setVisibility(View.VISIBLE);

                Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                intent.putExtra("info", item);
                intent.putExtra("type", KeyConstants.ADD);
                sendBroadcast(intent);

                if (OrderFragment.cartAdapter != null) {
                    OrderFragment.cartAdapter.notifyDataSetChanged();
                }

                if (cartAdapter != null) {
                    cartAdapter.notifyDataSetChanged();
                }

                clickView.setEnabled(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.iv_cart:
            case R.id.ll_cart:
                OrderFragment.getCart();
                if (isShowCart) {
                    // 判断购物车是否为空
                    if (OrderFragment.isCartEmpty()) {
                        ToastUtils.showMessage(this, getString(R.string.cart_empty));
                    } else {
                        showPop(ivCart);
                        isShowCart = false;
                    }
                } else {
                    if (popWindow != null && popWindow.isShowing()) {
                        popWindow.dismiss();
                        blackView.setVisibility(View.GONE);
                        isShowCart = true;
                    }
                }
                break;
            case R.id.tv_ok:  // 选好了
                OrderFragment.getCart();
                Intent intent = new Intent(this, DeliverInfoActivity.class);
                intent.putExtra("infos", (Serializable) realCart);
                startActivity(intent);
                break;
        }
    }

    /**
     * 显示购物车的详情
     */
    private void showPop(@NonNull View temp) {
        View v = getLayoutInflater().inflate(R.layout.cart_popwindow, null);
        LinearLayout llCart = (LinearLayout) v.findViewById(R.id.ll_cart);
        int[] location = new int[2];
        temp.getLocationOnScreen(location);
        popWindow = new PopupWindow(v, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ListView menuList = (ListView) v.findViewById(R.id.card_food_list);
        cartAdapter = new CommonAdapter<ProductClassBean.ProductBean>(this, realCart, R.layout.listitem_cart) {

            @Override
            public void convert(@NonNull ViewHolder helper, @NonNull final ProductClassBean.ProductBean item) {
                helper.setText(R.id.tv_format, item.getFormat());
                helper.setText(R.id.tv_price, "￥" + item.getPrice() + "/" + item.getUnit());
                helper.setText(R.id.tv_name, item.getName());

                final TextView tvCount = helper.getView(R.id.tv_count);
                final ImageView ivAnimReduce = helper.getView(R.id.iv_anim_reduce);
                final ImageView ivAdd = helper.getView(R.id.iv_add);

                final ImageView ivReduce = helper.getView(R.id.iv_reduce);
                ivReduce.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (ProductClassBean productClassInfo : OrderFragment.productClassInfos) {
                            for (ProductClassBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                if (item.getPid().equals(productInfo.getPid())) {
                                    productInfo.setNumber(productInfo.getNumber() - 1);
                                }
                            }
                        }

                        item.setNumber(item.getNumber() - 1);

                        Message msg = mHandler.obtainMessage(ACTION_REDUCE);
                        msg.arg1 = Integer.parseInt(item.getPid());
                        msg.obj = item;
                        mHandler.sendMessage(msg);

                        OrderFragment.classes.get(item.getPosition()).setNumber(OrderFragment.classes.get(item.getPosition()).getNumber() - 1);
                        OrderFragment.classAdapter.notifyDataSetChanged();

                        if (item.getNumber() == 0) {
                            ivReduce.setEnabled(false);
                            hide(ivReduce, ivAnimReduce);
                        }

                        Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                        intent.putExtra("info", item);
                        intent.putExtra("type", KeyConstants.REDUCE);
                        sendBroadcast(intent);

                        cartAdapter.notifyDataSetChanged();
                    }
                });

                if (item.getNumber() > 0) {
                    tvCount.setText(item.getNumber() + "");
                    tvCount.setVisibility(View.VISIBLE);
                    ivReduce.setVisibility(View.VISIBLE);
                } else {
                    tvCount.setVisibility(View.INVISIBLE);
                    ivReduce.setVisibility(View.INVISIBLE);
                }

                ivAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (ProductClassBean productClassInfo : OrderFragment.productClassInfos) {
                            for (ProductClassBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                if (item.getPid().equals(productInfo.getPid())) {
                                    productInfo.setNumber(productInfo.getNumber() + 1);
                                }
                            }
                        }

                        item.setNumber(item.getNumber() + 1);

                        Message msg = mHandler.obtainMessage(ACTION_ADD);
                        msg.arg1 = Integer.parseInt(item.getPid());
                        msg.obj = item;
                        mHandler.sendMessage(msg);

                        OrderFragment.classes.get(item.getPosition()).setNumber(OrderFragment.classes.get(item.getPosition()).getNumber() + 1);
                        OrderFragment.classAdapter.notifyDataSetChanged();

                        if (item.getNumber() == 1) {
                            ivAdd.setEnabled(false);
                            show(ivAnimReduce, ivAdd, ivReduce, item);
                        } else {
                            Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                            intent.putExtra("info", item);
                            intent.putExtra("type", KeyConstants.ADD);
                            sendBroadcast(intent);
                            cartAdapter.notifyDataSetInvalidated();
                        }
                    }
                });
            }
        };
        menuList.setAdapter(cartAdapter);
        popWindow.showAtLocation(temp, Gravity.BOTTOM, 0, DensityUtils.dp2px(this, 40));
        blackView.setVisibility(View.VISIBLE);

        llCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
            }
        });

        v.findViewById(R.id.tv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog == null) {
                    dialog = new CustemConfirmDialog(ProductDetailActivity.this, R.style.QQStyle, "确定清空购物车吗？"
                            , "清空",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent();
                                    intent.setAction(KeyConstants.ACTION_CART_CLEAR);
                                    sendBroadcast(intent);
                                    dialog.dismiss();
                                    if (popWindow != null && popWindow.isShowing()) {
                                        popWindow.dismiss();
                                        blackView.setVisibility(View.GONE);
                                        isShowCart = true;
                                    }
                                    tvSum.setText("0");
                                    tvCount.setText("0");
                                    tvCount.setVisibility(View.GONE);
                                    tvOk.setEnabled(false);
                                    mproductInfo.setNumber(0);
                                    ivReduce.setEnabled(false);
                                    ivReduce.setVisibility(View.INVISIBLE);
                                    tvAmount.setVisibility(View.INVISIBLE);
                                }
                            });
                }
                dialog.show();
            }
        });
    }

    public void back(View v) {
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.PAY_SUCCESS)
            finish();
    }
}
