package com.zhaodiandao.shopkeeper.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.socks.library.KLog;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.account.PaySuccessActivity;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
    private IWXAPI api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_result);
        api = WXAPIFactory.createWXAPI(this, KeyConstants.WEIXIN_APP_ID);
        api.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {

    }

    @Override
    public void onResp(@NonNull BaseResp resp) {
        KLog.json(resp.errCode + "");
        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            /**
             * resp.errCode== 0 ：表示支付成功
             * resp.errCode== -1 ：表示支付失败
             * resp.errCode== -2 ：表示取消支付
             */
            if (resp.errCode == 0) {
                EventBus.getDefault().post(new MessageEvent(EventCode.PAY_SUCCESS, "支付成功!"));
                startActivity(new Intent(this, PaySuccessActivity.class));
                finish();
                overridePendingTransition(0, 0);
            } else if (resp.errCode == -1) {
                Toast.makeText(this, "支付失败", Toast.LENGTH_SHORT).show();
                finish();
            } else if (resp.errCode == -2) {
                Toast.makeText(this, "支付取消", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else {
            Toast.makeText(this, "未知错误", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}