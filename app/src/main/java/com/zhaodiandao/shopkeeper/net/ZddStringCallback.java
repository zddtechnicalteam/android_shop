package com.zhaodiandao.shopkeeper.net;

import android.content.Context;
import android.support.annotation.NonNull;

import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;

/**
 * Created by yanix on 2016/11/24.
 * 根据业务封装网络回调
 */
public abstract class ZddStringCallback extends StringCallback {
    Context mContext;
    String mKey;

    public ZddStringCallback(Context context, String key) {
        mContext = context;
        mKey = key;
    }

    @Override
    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
        if (!call.isCanceled())
            ToastUtils.showMessage(mContext, e.getMessage());
    }

    @Override
    public void onResponse(String response, int id) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String resp = jsonObject.getString(mKey);
            success(resp);
        } catch (JSONException e) {
            e.printStackTrace();
            ToastUtils.showMessage(mContext, mContext.getString(R.string.error_json));
            failure();
        }
    }

    public abstract void success(String response);

    public void failure() {
    }
}
