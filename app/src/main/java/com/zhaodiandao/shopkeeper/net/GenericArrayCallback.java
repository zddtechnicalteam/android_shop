package com.zhaodiandao.shopkeeper.net;

import android.content.Context;
import android.support.annotation.NonNull;

import com.alibaba.fastjson.JSON;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

import okhttp3.Call;

/**
 * Created by yanix on 2016/11/25.
 * 将json装换为单个实体类
 */
public abstract class GenericArrayCallback<T> extends StringCallback {
    private Context mContext;
    Class<T> mClass;

    public GenericArrayCallback(Context context, Class<T> cls) {
        mContext = context;
        mClass = cls;
    }

    @Override
    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
        if (!call.isCanceled())
            ToastUtils.showMessage(mContext, e.getMessage());
    }

    @Override
    public void onResponse(String response, int id) {
        try {
            success(JSON.parseArray(response, mClass));
        } catch (Exception e) {
            ToastUtils.showMessage(mContext, mContext.getString(R.string.error_json));
            failure();
        }
    }

    public abstract void success(List<T> data);

    public void failure() {

    }
}
