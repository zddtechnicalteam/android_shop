package com.zhaodiandao.shopkeeper.net;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.zhaodiandao.shopkeeper.config.URLConstants;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

/**
 * Created by yanix on 2016/11/24.
 * api接口服务类
 */
public class ApiService {
    @Nullable
    private static ApiService instance = null;

    private ApiService() {
    }

    @Nullable
    public static ApiService getInstance() {
        if (instance == null)
            instance = new ApiService();
        return instance;
    }

    /**
     * 业务员登录
     * @param tag           请求TAG
     * @param account       账号
     * @param password      密码
     * @param callback      回调接口
     */
    public void login(@NonNull String tag, String account, String password, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_LOGIN)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("account", account)
                .addParams("password", password)
                .build()
                .execute(callback);
    }

    /**
     * 登录
     * @param tag
     * @param shopId
     * @param callback
     */
    public void login(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_LOGIN_BY_SHOPID)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取城市数据
     * @param tag
     * @param callback
     */
    public void getCityData(@NonNull String tag, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_CITY_DATA)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .build()
                .execute(callback);
    }

    /**
     * 获取城市品牌信息
     * @param tag
     * @param callback
     */
    public void getCityBrandData(@NonNull String tag, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_CITY_BRAND_DATA)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .build()
                .execute(callback);
    }

    /**
     * 获取手机验证码
     * @param tag
     * @param mobile    手机号
     * @param callback
     */
    public void getMsgCode(@NonNull String tag, String mobile, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_MSG_CODE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("mobile", mobile)
                .build()
                .execute(callback);
    }

    /**
     * 查询开店进度
     * @param tag
     * @param mobile    手机号
     * @param msgCode   验证码
     * @param callback
     */
    public void queryProgress(@NonNull String tag, String mobile, String msgCode, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_QUERY_PROGRESS)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("mobile", mobile)
                .addParams("code", msgCode)
                .build()
                .execute(callback);
    }

    /**
     * 查询开店进度
     * @param tag
     * @param mobile
     * @param callback
     */
    public void queryProgress(@NonNull String tag, String mobile, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_QUERY_PROGRESS_BY_MOBILE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("mobile", mobile)
                .build()
                .execute(callback);
    }

    /**
     * 修改密码
     * @param tag
     * @param shopId    商户ID
     * @param newPsw    新密码
     * @param callback
     */
    public void changePsw(@NonNull String tag, String shopId, String newPsw, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_CHANGE_PASSWORD)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("password", newPsw)
                .build()
                .execute(callback);
    }

    /**
     * 获取产品列表
     * @param tag
     * @param shopId    商户ID
     * @param callback
     */
    public void getProductList(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_PRODUCT_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_3)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取文章列表
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getArticleList(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_ARTICLE_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取订单列表
     * @param tag
     * @param shopId
     * @param type      订单状态
     * @param callback
     */
    public void getOrderList(@NonNull String tag, String shopId, String type, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_ORDER_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("type", type)
                .build()
                .execute(callback);
    }

    /**
     * 获取未支付的订单数
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getNotPayOrderCount(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_NOT_PAY_ORDER_COUNT)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 检查更新
     * @param tag
     * @param shopId
     * @param appVersion
     * @param appVersionCode
     * @param callback
     */
    public void getUpdateInfo(@NonNull String tag, String shopId, String appVersion, String appVersionCode, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_APP_UPDATE_INFO)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("mobile_models", Build.BRAND + ": " + Build.MODEL)
                .addParams("mobile_version", Build.VERSION.SDK_INT + ": " + Build.VERSION.RELEASE)
                .addParams("app_version", appVersion)
                .addParams("app_version_code", appVersionCode)
                .build()
                .execute(callback);
    }

    /**
     * 获取账户流水
     * @param tag
     * @param shopId
     * @param type
     * @param callback
     */
    public void getAccountList(@NonNull String tag, String shopId, String type, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_ACCOUNT_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("type", type)
                .build()
                .execute(callback);
    }

    /**
     * 充值
     * @param tag
     * @param shopId
     * @param type
     * @param money
     * @param callback
     */
    public void recharge(@NonNull String tag, String shopId, String type, String money, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_RECHARGE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("type", type)
                .addParams("money", money)
                .build()
                .execute(callback);
    }

    /**
     * 获取配送信息
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getDeliverInfo(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_DELIVER_INFO)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 预览订单
     * @param tag
     * @param shopId
     * @param products
     * @param callback
     */
    public void getPreOrder(@NonNull String tag, String shopId, String linkman, String mobile, String address, String remark,
                            String delivertype, String delivertime, String products, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_PRE_ORDER)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("shop_id", shopId)
                .addParams("linkman", linkman)
                .addParams("mobile", mobile)
                .addParams("address", address)
                .addParams("remark", remark)
                .addParams("delivertype", delivertype)
                .addParams("delivertime", delivertime)
                .addParams("products", products)
                .build()
                .execute(callback);
    }

    /**
     * 生成订单
     * @param tag
     * @param shopId
     * @param linkman
     * @param mobile
     * @param address
     * @param delivertime
     * @param remark
     * @param products
     * @param callback
     */
    public void createOrder(@NonNull String tag, String shopId, String linkman, String mobile, String address,
                            String deliverType, String delivertime, String remark, String products, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_CREATE_ORDER)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("linkman", linkman)
                .addParams("mobile", mobile)
                .addParams("address", address)
                .addParams("delivertype", deliverType)
                .addParams("delivertime", delivertime)
                .addParams("remark", remark)
                .addParams("products", products)
                .build()
                .execute(callback);
    }

    /**
     * 提交支付
     * @param tag
     * @param shopId
     * @param ordernumber
     * @param paytype
     * @param callback
     */
    public void pay(@NonNull String tag, String shopId, String ordernumber, String paytype, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_PAY)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("ordernumber", ordernumber)
                .addParams("paytype", paytype)
                .build()
                .execute(callback);
    }

    /**
     * 获取订单详情
     * @param tag
     * @param shopId
     * @param orderId
     * @param callback
     */
    public void getOrderDetail(@NonNull String tag, String shopId, String orderId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_ORDER_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("order_id", orderId)
                .build()
                .execute(callback);
    }

    /**
     * 取消订单
     * @param tag
     * @param shopId
     * @param ordernumber
     * @param callback
     */
    public void cancelOrder(@NonNull String tag, String shopId, String ordernumber, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_CANCEL_ORDER)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("ordernumber", ordernumber)
                .build()
                .execute(callback);
    }

    /**
     * 跳转支付
     * @param tag
     * @param shopId
     * @param ordernumber
     * @param callback
     */
    public void toPay(@NonNull String tag, String shopId, String ordernumber, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_TO_PAY)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("ordernumber", ordernumber)
                .build()
                .execute(callback);
    }

    /**
     * 获取品牌列表
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getBrandList(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_BRAND_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取品牌介绍
     * @param tag
     * @param shopId
     * @param brandId
     */
    public void getBrandIntro(@NonNull String tag, String shopId, String brandId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_BRAND_INTRO)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("brand_id", brandId)
                .build()
                .execute(callback);
    }

    /**
     * 获取已加盟的品牌
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getMyBrandList(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_MY_BRAND)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取品牌详情
     * @param tag
     * @param shopId
     * @param restaurantId
     * @param callback
     */
    public void getBrandDetail(@NonNull String tag, String shopId, String restaurantId, String brandId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_BRAND_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("shop_id", shopId)
                .addParams("brand_id", brandId)
                .addParams("restaurant_id", restaurantId)
                .build()
                .execute(callback);
    }

    /**
     * 检查新产品
     * @param tag
     * @param shopId
     * @param callback
     */
    public void checkNewProduct(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_CHECK_NEW_PRODUCT)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 转到质量反馈
     * @param tag
     * @param shopId
     * @param orderId   订货单ID
     * @param callback
     */
    public void toFeedback(@NonNull String tag, String shopId, String orderId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_TO_FEEDBACK)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("order_id", orderId)
                .build()
                .execute(callback);
    }

    /**
     * 转到产品缺货
     * @param tag
     * @param orderId
     * @param callback
     */
    public void toAbsent(@NonNull String tag, String orderId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_TO_ABSENT)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("order_id", orderId)
                .build()
                .execute(callback);
    }

    /**
     * 获取最后一次要投诉的订货单
     * @param tag
     * @param shopId
     * @param callback
     */
    public void toFeedback(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_TO_FEEDBACK)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取我的质量反馈
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getMyFeedbackList(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_MY_FEEDBACK)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取质量反馈详情
     * @param tag
     * @param shopId
     * @param fdId
     * @param callback
     */
    public void getFeedbackDetail(@NonNull String tag, String shopId, String fdId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_FEEDBACK_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("fd_id", fdId)
                .build()
                .execute(callback);
    }

    /**
     * 提货券列表
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getTicketList(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_TICKET_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 购买预购劵
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getStkTicketList(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_STK_TICKET_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 我的提货券
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getMyTicket(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_MY_TICKET)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 我的预购券
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getMyStkTicket(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_MY_STK_TICKET)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 购买提货券
     * @param tag
     * @param shopId
     * @param callback
     */
    public void buyTicket(@NonNull String tag, String shopId, String paytype, String pfks, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_BUY_TICKET)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("paytype", paytype)
                .addParams("pfks", pfks)
                .build()
                .execute(callback);
    }

    /**
     * 购买预购券
     * @param tag
     * @param shopId
     * @param paytype
     * @param pfks
     * @param callback
     */
    public void buyStkTicket(@NonNull String tag, String shopId, String paytype, String pfks, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_BUY_STK_TICKET)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("paytype", paytype)
                .addParams("stks", pfks)
                .build()
                .execute(callback);
    }

    /**
     * 获取提货券详情
     * @param tag
     * @param shopId
     * @param pftId
     * @param type
     * @param callback
     */
    public void getTicketDetail(@NonNull String tag, String shopId, String pftId, String type, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_TICKET_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("pft_id", pftId)
                .addParams("tag", type)
                .build()
                .execute(callback);
    }

    /**
     * 预购券详情
     * @param tag
     * @param shopId
     * @param pftId
     * @param type
     * @param callback
     */
    public void getStkTicketDetail(@NonNull String tag, String shopId, String pftId, String type, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_STK_TICKET_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("stk_id", pftId)
                .addParams("type", type)
                .build()
                .execute(callback);
    }


    /**
     * 阅读文章统计
     * @param tag
     * @param id    文章ID
     * @param shopId
     * @param callback
     */
    public void readArticle(@NonNull String tag, String id, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_ARTICLE_READ)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("id", id)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取直播列表
     * @param tag
     * @param shopId
     * @param callback
     */
    public void getLiveList(@NonNull String tag, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_LIVE_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 上传直播状态
     * @param tag
     * @param shopId
     * @param liveId
     * @param status
     * @param callback
     */
    public void uploadLiveStatus(@NonNull String tag, String shopId, String liveId, String status, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_UPLOAD_LIVE_STATUS)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shopId)
                .addParams("live_id", liveId)
                .addParams("status", status)
                .build()
                .execute(callback);
    }

    /**
     * 缺货产品提交
     * @param tag
     * @param order_id
     * @param remark
     * @param outproducts
     * @param callback
     */
    public void absentSubmit(@NonNull String tag, String order_id, String remark, String outproducts, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_ABSENT_SUBMIT)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("order_id", order_id)
                .addParams("remark", remark)
                .addParams("outproducts", outproducts)
                .build()
                .execute(callback);
    }

    /**
     * 订单完成
     * @param tag
     * @param orderNumber
     * @param callback
     */
    public void orderFinish(@NonNull String tag, String orderNumber, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_ORDER_FINISH)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("ordernumber", orderNumber)
                .build()
                .execute(callback);
    }

    /**
     * 反馈确认
     * @param tag
     * @param shop_id
     * @param fd_id
     * @param callback
     */
    public void confirmFeedback(@NonNull String tag, String shop_id, String fd_id, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.CONFIRM_FEEDBACK)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("shop_id", shop_id)
                .addParams("fd_id", fd_id)
                .build()
                .execute(callback);
    }
}
