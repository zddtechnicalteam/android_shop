package com.zhaodiandao.shopkeeper.net;

import android.content.Context;
import android.support.annotation.NonNull;

import com.alibaba.fastjson.JSON;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.util.ToastUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by yanix on 2016/11/25.
 * 将json装换为单个实体类
 */
public abstract class GenericCallback<T> extends StringCallback {
    private Context mContext;
    Class<T> mClass;

    public GenericCallback(Context context, Class<T> cls) {
        mContext = context;
        mClass = cls;
    }

    @Override
    public void onError(@NonNull Call call, @NonNull Exception e, int id) {
        if (!call.isCanceled())
            ToastUtils.showMessage(mContext, e.getMessage());
    }

    @Override
    public void onResponse(String response, int id) {
        try {
            success(JSON.parseObject(response, mClass));
        } catch (Exception e) {
            ToastUtils.showMessage(mContext, mContext.getString(R.string.error_json));
            failure();
        }
    }

    public abstract void success(T data);

    public void failure() {

    }
}
