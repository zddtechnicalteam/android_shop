package com.zhaodiandao.shopkeeper;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

import com.tencent.smtt.sdk.WebChromeClient;
import com.zhaodiandao.shopkeeper.view.SlowlyProgressBar;
import com.zhaodiandao.shopkeeper.view.X5WebView;

import static com.zhaodiandao.shopkeeper.R.id.webView;

/**
 * Created by yanix on 16/7/27.
 */
public class ZDDWebViewActivity extends BaseActivity {
    public static final String TITLE = "title";
    public static final String URL = "url";
    private X5WebView mWebView;
    @Nullable
    private SlowlyProgressBar slowlyProgressBar;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_webview);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String title = intent.getStringExtra(TITLE);
        String url = intent.getStringExtra(URL);
        setTitle(title);

        mWebView = (X5WebView) findViewById(webView);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);

        mWebView.getView().setOverScrollMode(View.OVER_SCROLL_ALWAYS);

        slowlyProgressBar = new SlowlyProgressBar(findViewById(R.id.holder), getWindowManager().getDefaultDisplay().getWidth()).setViewHeight(3);

        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(com.tencent.smtt.sdk.WebView webView, int i) {
                super.onProgressChanged(webView, i);
                if (slowlyProgressBar == null)
                    return;
                slowlyProgressBar.setProgress(i);
            }

        });

        // 支持js
        mWebView.getSettings().setJavaScriptEnabled(true);

        mWebView.loadUrl(url);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        try {
            super.onConfigurationChanged(newConfig);
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        // mWebView.reload();
        super.onPause();
    }

    /**
     * 监听up键
     * id = android.R.id.home
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 监听back键
     * 在WebView中回退导航
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {  // 返回键的KEYCODE
            if (mWebView.canGoBack()) {
                mWebView.goBack();
                return true;  // 拦截
            } else {
                return super.onKeyDown(keyCode, event);   //  放行
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (slowlyProgressBar != null) {
            slowlyProgressBar.destroy();
            slowlyProgressBar = null;
        }
    }
}
