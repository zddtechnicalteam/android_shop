package com.zhaodiandao.shopkeeper.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.igexin.sdk.PushManager;
import com.umeng.analytics.MobclickAgent;
import com.zhaodiandao.shopkeeper.BaseActivity;
import com.zhaodiandao.shopkeeper.BuildConfig;
import com.zhaodiandao.shopkeeper.MainActivity;
import com.zhaodiandao.shopkeeper.R;
import com.zhaodiandao.shopkeeper.config.EventCode;
import com.zhaodiandao.shopkeeper.config.KeyConstants;
import com.zhaodiandao.shopkeeper.model.MessageEvent;
import com.zhaodiandao.shopkeeper.model.OpenShopModel;
import com.zhaodiandao.shopkeeper.model.ShopModel;
import com.zhaodiandao.shopkeeper.net.GenericCallback;
import com.zhaodiandao.shopkeeper.shop.OpenShopActivity;
import com.zhaodiandao.shopkeeper.shop.QueryShopProgressActivity;
import com.zhaodiandao.shopkeeper.shop.ShopProgressActivity;
import com.zhaodiandao.shopkeeper.util.DialogHelper;
import com.zhaodiandao.shopkeeper.util.SPUtils;
import com.zhaodiandao.shopkeeper.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Request;

/**
 * Created by yanix on 2016/11/21.
 * 运营登录界面
 */
public class LoginActivity extends BaseActivity {
    @Nullable
    @BindView(R.id.et_account)
    TextInputEditText etAccount;
    @Nullable
    @BindView(R.id.et_password)
    TextInputEditText etPassword;
    @Nullable
    @BindView(R.id.btn_login)
    Button btnLogin;

    @Override
    protected void beforeContent(Bundle savedInstanceState) {
        super.beforeContent(savedInstanceState);
        // 设置全屏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);

        OpenShopModel data = (OpenShopModel) getIntent().getSerializableExtra(ShopProgressActivity.KEY_MODEL);
        if (data != null) {
            Intent intent = new Intent(LoginActivity.this, ShopProgressActivity.class);
            intent.putExtra(ShopProgressActivity.KEY_MODEL, data);
            startActivity(intent);
        }

        // 监听输入框内容变化
        addTextChangeListener(etAccount);
        addTextChangeListener(etPassword);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @OnClick(R.id.btn_login)
    public void login(View view) {
        // 按钮可以点击，说明输入内容不为空，所以这里不用再判断内容是否为空
        String account = etAccount.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        // 去登录
        mApiService.login(requestTag, account, password, new GenericCallback<ShopModel>(getApplicationContext(), ShopModel.class) {

            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                DialogHelper.showDimDialog(LoginActivity.this, "登录中...");
            }

            @Override
            public void onAfter(int id) {
                DialogHelper.dismissDialog();
                super.onAfter(id);
            }

            @Override
            public void success(@NonNull ShopModel shopModel) {
                // 登录成功，保存业务员ID的值，下次就不用登录了
                SPUtils.put(getApplicationContext(), KeyConstants.SHOP_ID, shopModel.getShop_id());
                SPUtils.put(getApplicationContext(), KeyConstants.SHOP_NAME, shopModel.getName());
                SPUtils.put(getApplicationContext(), KeyConstants.SHOP_ADDRESS, shopModel.getAddress());
                SPUtils.remove(getApplicationContext(), KeyConstants.SHOP_MOBILE);
                // 绑定推送别名
                PushManager.getInstance().bindAlias(getApplicationContext(), "shop_id_" + shopModel.getShop_id());
                if (!BuildConfig.LOG_DEBUG) {
                    // 统计账户信息
                    MobclickAgent.onProfileSignIn(shopModel.getShop_id());
                }

                EMClient.getInstance().login("shop_" + shopModel.getShop_id(), "123456", new EMCallBack() {     //回调
                    @Override
                    public void onSuccess() {
                        // 进入首页
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        // 销毁界面
                        finish();
                    }

                    @Override
                    public void onProgress(int progress, String status) {

                    }

                    @Override
                    public void onError(int code, String message) {
                        ToastUtils.showMessage(getApplicationContext(), "登录聊天服务器失败！" + message);
                        // 进入首页
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        // 销毁界面
                        finish();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_open_shop)
    public void openShop(View v) {
        startActivity(new Intent(this, OpenShopActivity.class));
    }

    @OnClick(R.id.btn_query_process)
    public void queryProgress(View v) {
        startActivity(new Intent(this, QueryShopProgressActivity.class));
    }

    /**
     * 监听输入框变化，改变按钮状态
     *
     * @param editText 要监听的输入框
     */
    private void addTextChangeListener(@NonNull final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            boolean isOtherEmpty = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (editText == etAccount) {
                    isOtherEmpty = TextUtils.isEmpty(etPassword.getText().toString().trim());
                } else {
                    isOtherEmpty = TextUtils.isEmpty(etAccount.getText().toString().trim());
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(@NonNull Editable s) {
                // 如果账号密码都不为空，登录按钮状态设置为正常；否则，设置为禁用
                if (!TextUtils.isEmpty(s.toString().trim()) && !isOtherEmpty) {
                    btnLogin.setEnabled(true);
                } else {
                    btnLogin.setEnabled(false);
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull MessageEvent event) {
        if (event.code == EventCode.CLOSE)
            finish();
    }
}
