package com.zhaodiandao.shopkeeper;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.umeng.analytics.MobclickAgent;
import com.zhaodiandao.shopkeeper.net.ApiService;
import com.zhy.http.okhttp.OkHttpUtils;

/**
 * Created by yanix on 2016/11/23.
 * 作为Fragment的基类，封装一些通用的代码逻辑
 */
public abstract class BaseFragment extends Fragment {
    protected View root;
    protected boolean isFirstVisible = true;    // 是否第一次显示
    @Nullable
    protected ApiService mApiService;
    protected String requestTag = this.getClass().getSimpleName();

    /**
     * 返回视图，由子类实现
     */
    public abstract View getView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    /**
     * 获取数据，由子类实现
     * 如果获取失败，界面上需要显示重试按钮
     */
    public abstract void getData();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return getView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApiService = ApiService.getInstance();
    }

    @Override
    public void onResume() {
        super.onResume();
        // 友盟统计信息
        if (!BuildConfig.LOG_DEBUG)
            MobclickAgent.onPageStart("MainScreen");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!BuildConfig.LOG_DEBUG)
            MobclickAgent.onPageEnd("MainScreen");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isFirstVisible) {
            isFirstVisible = false;
            // 只在第一次可见时，获取数据并显示
            getData();
        }
    }

    @Override
    public void onStop() {
        // 取消网路请求
        OkHttpUtils.getInstance().cancelTag(requestTag);
        super.onStop();
    }
}
