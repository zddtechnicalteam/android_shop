package com.zhaodiandao.shopkeeper.config;

/**
 * Created by yanix on 2016/11/30.
 * Event Bus事件状态码
 */
public class EventCode {
    // 支付成功
    public static final int PAY_SUCCESS = 1;
    // 刷新界面
    public static final int REFRESH_PAY = 2;
    // 刷新订单界面
    public static final int REFRESH_ORDER = 3;
    // 刷新品牌
    public static final int REFRESH_BRAND = 4;
    // 销毁
    public static final int CLOSE = 5;
    // 反馈确认
    public static final int CONFIRM_FEEDBACK = 6;
}
