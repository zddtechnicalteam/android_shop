package com.zhaodiandao.shopkeeper.config;

/**
 * Created by yanix on 2016/11/24.
 */
public class KeyConstants {
    // 业务员登录ID
    public static final String SHOP_ID = "shop_id";
    public static final String SHOP_NAME = "shop_name";
    public static final String SHOP_ADDRESS = "shop_address";
    public static final String SHOP_MOBILE = "shop_mobile";
    public static final String SHOP_NICK = "shop_nick";

    public static final String ORIG_X = "orig_x";
    public static final String ORIG_Y = "orig_y";
    public static final String ORIG_WIDTH = "orig_width";
    public static final String ORIG_HEIGHT = "orig_height";
    public static final String IMG_URL = "img_url";

    public static final String ACTION_CHANGE = "action_change";             // 订货数量改变
    public static final String ACTION_CART_CLEAR = "action_cart_clear";     // 清空购物车
    public static final String ACTION_REFRESH = "action_refresh";     // 刷新菜品列表

    public static final String ACTION_CHANGE_TICKET = "action_change_ticket";             // 订货数量改变
    public static final String ACTION_CART_CLEAR_TICKET = "action_cart_clear_ticket";     // 清空购物车
    public static final String ACTION_REFRESH_TICKET = "action_refresh_ticket";     // 刷新菜品列表

    public static final String ACTION_CHANGE_STKTICKET = "action_change_stk_ticket";             // 订货数量改变
    public static final String ACTION_CART_CLEAR_STKTICKET = "action_cart_clear_stk_ticket";     // 清空购物车
    public static final String ACTION_REFRESH_STKTICKET = "action_refresh_stk_ticket";     // 刷新菜品列表
    // 金额保留小数点后的位数
    public static final int SCALE = 2;
    public static final int ADD = 1;
    public static final int REDUCE = 2;
    public static final int ANIM_DURATION = 400;

    // 微信支付
    // API密钥，在商户平台设置
    public static final String API_KEY = "b94051309fc47d345d62a7cf6cecbcc4";
    // 微信支付商户ID，在邮件中查看
    public static final String MCH_ID = "1305866201";
    // 应用ID，在开放平台或邮件中查看
    public static final String WEIXIN_APP_ID = "wx40905268fccdd376";
    // 微信支付结果回调地址
    // 1.充值
    // public static final String WEIXIN_NOTIFY_URL = "http://zdd.chrisbin.cn/mobile/wxpay/storeRechargeNotify";
    public static final String WEIXIN_NOTIFY_URL = "http://m.izaodiandao.com/wxpay/storeRechargeNotify";
    // 2.支付订货单
    public static final String WEIXIN_NOTIFY_URL2 = "http://m.izaodiandao.com/wxpay/storeNotify";
    // 3.支付提货券
    public static final String WEIXIN_NOTIFY_URL3 = "http://m.izaodiandao.com/wxpay/pfkOrderNotify";
    // 4.支付预购券
    public static final String WEIXIN_NOTIFY_URL4 = "http://m.izaodiandao.com/wxpay/stkOrderNotify";
    // public static final String WEIXIN_NOTIFY_URL4 = "http://w.zdd.dailingna.cn/mobile/wxpay/stkOrderNotify";

    //支付宝支付
    //商户PID
    public static final String PARTNER = "2088811346659463";
    //商户收款账号
    public static final String SELLER = "zaodiandao@126.com";
    //商户私钥，pkcs8格式
    public static final String RSA_PRIVATE = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAMvDFds4Ex+im5Jcu4ARjwc5d8tIfWWRr7A6NJK8SzwLx1U8SjdsVeOZs+wepoUUm67uyv34zRf+7iLRNjh02Beq5XJjmEMXvQof994PYZu9xSxujva0znEkTc3j5cgkDI4MkVBmXpVUMtBcGMe480/Z3Ce2yIoAAzHWr3iYf6kDAgMBAAECgYAe55PlD/g2kB8wb8gwgHlFCz5UIsaCJTqUa/+VUeHHVbICB4bqKRBN2P4lDfELtjh2q9k19AqHXS8ZMMocffxYNTnxarpsEGqFOnkudRs0yTT7vY6yfRzC2+JPJmqlYB+BUVJb8/4yo87BmuatPnZYE2YH2F7K+v1rJt9AyHaHgQJBAPHpTHxFFEs4AHCIrxAnv+ydRzrk+XkMuziWZFiqx7UF4piTP1g6LvbAGw++SYXFtuvZ7TCfKolyYTuKO912SfMCQQDXoQMQ8sn5Xnd6HKRg8oV6KbmpqPUz8lX+iec1D6LsydoZWEcURJOkL4EGf/ziAq0t7b9bCDfBWLfFbIjeSVixAkEAtrj/msYa5A3y1w2HHe7IWxMZYHTfsv1Y1Q9T0F1+508ArzpZmGyqWJsWVbfnlPGYj2FWdWlkGdmZc/2f5lGY3QJBAKjETsAPJuHUfDLJBCcU6j+JNNWDdhcoTwIgMSQojt5BxkAcBdXpLp1MyPGrbuP+MElIUYMQVYMwyvTsU9uXEKECQQCaSpVwSWk2UUqwoQL+A6Lr++8jJMGMQeYEb09bW3ojEdA1jhEjTEAEc9s6dncid1u0uc8V+0yRsGlJU36iynLq";
    //支付宝支付成功通知服务器地址
    // 1.充值
    // public static final String ALIPAY_NOTIFY_URL = "http://zdd.chrisbin.cn/mobile/alipay/storeRechargeApiNotify";
    public static final String ALIPAY_NOTIFY_URL = "http://m.izaodiandao.com/alipay/storeRechargeApiNotify";
    // 2.支付订货单
    public static final String ALIPAY_NOTIFY_URL2 = "http://m.izaodiandao.com/alipay/storeApiNotify";
    // 3.支付提货券
    public static final String ALIPAY_NOTIFY_URL3 = "http://m.izaodiandao.com/alipay/pfkOrderNotify";
    // 4.支付预购卷
    public static final String ALIPAY_NOTIFY_URL4 = "http://m.izaodiandao.com/alipay/stkOrderNotify";
    // public static final String ALIPAY_NOTIFY_URL4 = "http://w.zdd.dailingna.cn/mobile/alipay/stkOrderNotify";

    public static final String TAG = "yanix";
}
