package com.zhaodiandao.shopkeeper.config;

/**
 * Created by yanix on 2016/11/24.
 * API接口地址
 */
public class URLConstants {
    // public static final String API_BASE_URL = "http://shop.zdd.dailingna.cn/";
    public static final String API_BASE_URL = "http://shop.izaodiandao.com/";
    // API版本控制
    public static final String API_VERSION = "v1";
    public static final String API_VERSION_2 = "v2";
    public static final String API_VERSION_3 = "v3";
    public static final String API_VERSION_4 = "v4";
    // 业务员登录
    public static final String URL_LOGIN = API_BASE_URL + "v1/shopkeeper/login";
    public static final String URL_LOGIN_BY_SHOPID = API_BASE_URL + "v1/shopkeeper/idlogin";
    // 获取城市列表
    public static final String URL_CITY_DATA = API_BASE_URL + "v1/shopkeeper/city";
    public static final String URL_CITY_BRAND_DATA = API_BASE_URL + "v1/shopkeeper/preload";
    // 获取验证码
    public static final String URL_MSG_CODE = API_BASE_URL + "v1/shopkeeper/code";
    // 门店申请
    public static final String URL_OPEN_SHOP = API_BASE_URL + "v1/shopkeeper/register";
    // 门店进度查询
    public static final String URL_QUERY_PROGRESS = API_BASE_URL + "v1/shopkeeper/progress";
    public static final String URL_QUERY_PROGRESS_BY_MOBILE = API_BASE_URL + "v1/shopkeeper/mobile";
    // 修改密码
    public static final String URL_CHANGE_PASSWORD = API_BASE_URL + "v1/shopkeeper/newpwd";
    // 订货列表
    public static final String URL_PRODUCT_LIST = API_BASE_URL + "v1/product/list";
    // 文章列表
    public static final String URL_ARTICLE_LIST = API_BASE_URL + "v1/article/list";
    // 历史订单
    public static final String URL_ORDER_LIST = API_BASE_URL + "v1/order/list";
    // 未支付订单数量
    public static final String URL_NOT_PAY_ORDER_COUNT = API_BASE_URL + "v1/order/noPay";
    // app更新信息
    public static final String URL_APP_UPDATE_INFO = API_BASE_URL + "v1/shopkeeper/appupdate";
    // 获取账户流水
    public static final String URL_ACCOUNT_LIST = API_BASE_URL + "v1/account/list";
    // 提交充值
    public static final String URL_RECHARGE = API_BASE_URL + "v1/account/recharge";
    // 获取配送信息
    public static final String URL_DELIVER_INFO = API_BASE_URL + "v1/order/fpreorder";
    // 预览订单
    public static final String URL_PRE_ORDER = API_BASE_URL + "v1/order/preOrder";
    // 生成订单
    public static final String URL_CREATE_ORDER = API_BASE_URL + "v1/order/saveOrder";
    // 提交支付
    public static final String URL_PAY = API_BASE_URL + "v1/order/pay";
    // 订单详情
    public static final String URL_ORDER_DETAIL = API_BASE_URL + "v1/order/detail";
    // 取消订单
    public static final String URL_CANCEL_ORDER = API_BASE_URL + "v1/order/cancelorder";
    // 跳转支付
    public static final String URL_TO_PAY = API_BASE_URL + "v1/order/topay";
    // 品牌列表
    public static final String URL_BRAND_LIST = API_BASE_URL + "v1/brand/list";
    // 品牌介绍
    public static final String URL_BRAND_INTRO = API_BASE_URL + "v1/brand/detail";
    // 已加盟的品牌
    public static final String URL_MY_BRAND = API_BASE_URL + "v1/brand/addedlist";
    // 已加盟品牌详情
    public static final String URL_BRAND_DETAIL = API_BASE_URL + "v1/brand/getrestaurant";
    // 加盟品牌
    public static final String URL_APPLY_BRAND = API_BASE_URL + "v1/brand/league";
    // 检查新产品
    public static final String URL_CHECK_NEW_PRODUCT = API_BASE_URL + "v1/product/push";
    // 质量投诉
    public static final String URL_TO_FEEDBACK = API_BASE_URL + "v1/feedback/apply";
    public static final String URL_TO_ABSENT = API_BASE_URL + "v1/order/getProduct";
    // 提交投诉
    public static final String URL_SUBMIT_FEEDBACK = API_BASE_URL + "v1/feedback/save";
    // 质量反馈列表
    public static final String URL_MY_FEEDBACK = API_BASE_URL + "v1/feedback/list";
    // 质量反馈详情
    public static final String URL_FEEDBACK_DETAIL = API_BASE_URL + "v1/feedback/detail";
    // 常见问题
    public static final String URL_PROBLEM = "http://www.izaodiandao.com/mobile/question/index";
    // public static final String URL_PROBLEM = "http://zdd.chrisbin.cn/mobile/question/index";
    // 证照示例
    public static final String URL_EXAMPLE = "http://www.izaodiandao.com/mobile/example/index";
    // 提货券列表
    public static final String URL_TICKET_LIST = API_BASE_URL + "v1/deliveryticket/list";
    public static final String URL_STK_TICKET_LIST = API_BASE_URL + "v1/stk/sale";
    // 我的提货券
    public static final String URL_MY_TICKET = API_BASE_URL + "v1/deliveryticket/pftlist";
    public static final String URL_MY_STK_TICKET = API_BASE_URL + "v1/stk/list";
    // 购买提货券
    public static final String URL_BUY_TICKET = API_BASE_URL + "v1/deliveryticket/savepfkorder";
    public static final String URL_BUY_STK_TICKET = API_BASE_URL + "v1/stk/pay";
    // 提货券详情
    public static final String URL_TICKET_DETAIL = API_BASE_URL + "v1/deliveryticket/pfkdetail";
    public static final String URL_STK_TICKET_DETAIL = API_BASE_URL + "v1/stk/detail";
    // 提货券说明
    public static final String URL_TICKET_DESC = "http://www.izaodiandao.com/mobile/example/pickupticket";
    // 阅读次数统计
    public static final String URL_ARTICLE_READ = API_BASE_URL + "v1/article/clickarticle";
    // 视频直播列表
    public static final String URL_LIVE_LIST = API_BASE_URL + "v1/article/lives";
    // 上传直播状态
    public static final String URL_UPLOAD_LIVE_STATUS = API_BASE_URL + "v1/article/onlive";
    public static final String URL_ABSENT_SUBMIT = API_BASE_URL + "v1/order/outOrder";
    public static final String URL_ORDER_FINISH = API_BASE_URL + "v1/order/complete";
    public static final String CONFIRM_FEEDBACK = API_BASE_URL + "v1/feedback/fbconfirm";

}
